﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Context
{
    public class GovernancaCorporativaDbContext : DbContext
    {
        public GovernancaCorporativaDbContext(DbContextOptions<GovernancaCorporativaDbContext> options)
        : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public async Task<bool> CommitAsync()
        {
            var sucesso = true;
            try
            {
                sucesso = await base.SaveChangesAsync() > 0;
            }
            catch (System.Exception ex)
            {
                sucesso = false;
                throw new System.Exception($"error:{ex.Message} >>>> {ex.InnerException?.Message} <<<<>>>>>{ex.InnerException?.InnerException?.Message} <<<<<");
            }

            return sucesso;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(config.GetConnectionString("CSN")); //.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                         .SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(GovernancaCorporativaDbContext).Assembly);
        }

        public DbSet<TEntity> DbSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }
    }

    public class DesignTimeApplicationDbContext : IDesignTimeDbContextFactory<GovernancaCorporativaDbContext>
    {
        public GovernancaCorporativaDbContext CreateDbContext(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<GovernancaCorporativaDbContext>();
            // pass your design time connection string here
            optionsBuilder.UseSqlServer(config.GetConnectionString("CSN"));
            return new GovernancaCorporativaDbContext(optionsBuilder.Options);
        }
    }
}
