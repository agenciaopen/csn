﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Migrations
{
    public partial class _200520220855 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "GovernancaCorporativa",
                table: "ComposicaoAcionariaGraficoEstruturaSocietaria",
                newName: "Referencia");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "GovernancaCorporativa",
                table: "ComposicaoAcionariaGraficoEstruturaSocietaria",
                newName: "ReferenciaIdioma");
        }
    }
}
