﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Migrations
{
    public partial class GovernancaCorporativa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "GovernancaCorporativa");

            migrationBuilder.CreateTable(
                name: "ComposicaoAcionariaGraficoEstruturaSocietaria",
                schema: "GovernancaCorporativa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false),
                    VicunhaAcos = table.Column<double>(type: "float", nullable: false),
                    RioLaco = table.Column<double>(type: "float", nullable: false),
                    Nyse = table.Column<double>(type: "float", nullable: false),
                    AcoesEmTesouraria = table.Column<double>(type: "float", nullable: false),
                    Outros = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComposicaoAcionariaGraficoEstruturaSocietaria", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComposicaoAcionariaGraficoEstruturaSocietaria",
                schema: "GovernancaCorporativa");
        }
    }
}
