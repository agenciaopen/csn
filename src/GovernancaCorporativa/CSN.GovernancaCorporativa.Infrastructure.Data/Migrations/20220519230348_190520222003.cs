﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Migrations
{
    public partial class _190520222003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "GovernancaCorporativa",
                table: "ComposicaoAcionariaGraficoEstruturaSocietaria",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "GovernancaCorporativa",
                table: "ComposicaoAcionariaGraficoEstruturaSocietaria");
        }
    }
}
