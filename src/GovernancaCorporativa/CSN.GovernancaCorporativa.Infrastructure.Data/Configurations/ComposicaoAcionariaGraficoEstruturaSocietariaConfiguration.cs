﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Configurations
{
    public class ComposicaoAcionariaGraficoEstruturaSocietariaConfiguration : IEntityTypeConfiguration<EstruturaSocietaria>
    {
        public void Configure(EntityTypeBuilder<EstruturaSocietaria> builder)
        {
            builder.ToTable("ComposicaoAcionariaGraficoEstruturaSocietaria", "GovernancaCorporativa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.VicunhaAcos)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.RioLaco)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Nyse)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.AcoesEmTesouraria)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Outros)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
