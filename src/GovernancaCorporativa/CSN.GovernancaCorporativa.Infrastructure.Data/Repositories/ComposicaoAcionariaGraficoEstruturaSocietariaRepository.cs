﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;
using CSN.GovernancaCorporativa.Infrastructure.Data.Context;

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Repositories
{
    public class ComposicaoAcionariaGraficoEstruturaSocietariaRepository : BaseRepositoryAsync<EstruturaSocietaria>
    {
        protected readonly GovernancaCorporativaDbContext _context;

        public ComposicaoAcionariaGraficoEstruturaSocietariaRepository(GovernancaCorporativaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
