﻿using System.Linq.Expressions;
using CSN.Core;
using CSN.Core.Domain.Interfaces;
using CSN.GovernancaCorporativa.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.GovernancaCorporativa.Infrastructure.Data.Repositories
{
    public class BaseRepositoryAsync<TEntity> : ICsnRepositoryAsync<TEntity> where TEntity : class
    {
        protected readonly GovernancaCorporativaDbContext _context;

        public BaseRepositoryAsync(GovernancaCorporativaDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AdicionarAsync(TEntity t)
        {
            try
            {
                _context.Set<TEntity>().Add(t);
                return await _context.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> AtualizarAsync(TEntity? t, object? key)
        {
            try
            {
                if (t == null)
                    return false;

                TEntity? exist = _context.Set<TEntity>().Find(key);

                if (exist != null)
                    _context.Update(t);
                //_context.Entry(exist).CurrentValues.SetValues(t);

                return await _context.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> ExcluirAsync(object id)
        {
            TEntity? exist = _context.Set<TEntity>().Find(id);

            if (exist != null)
                _context.Set<TEntity>().Remove(exist);

            return await _context.CommitAsync();
        }

        public async Task<TEntity?> ObterAsync(Expression<Func<TEntity, bool>> predicate, Func<TEntity, DateTime> order)
        {
            IOrderedEnumerable<TEntity> result = _context.DbSet<TEntity>().Where(predicate).OrderByDescending(order);
            return result.FirstOrDefault();
        }

        public async Task<bool> ExisteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.DbSet<TEntity>().AnyAsync(predicate);
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
