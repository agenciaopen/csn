﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.GovernancaCorporativa.Application.Requests.ComposicaoAcionaria;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;

namespace CSN.GovernancaCorporativa.Application.Interfaces
{
    public interface IComposicaoAcionariaGraficoEstruturaSocietariaService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoEstruturaSocietariaRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoEstruturaSocietariaRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoEstruturaSocietariaRequest request);
        Task<EstruturaSocietaria?> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoEstruturaSocietariaRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEstruturaSocietariaRequest request);
    }
}
