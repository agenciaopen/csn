﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.GovernancaCorporativa.Application.Requests.ComposicaoAcionaria
{
    public class AdicionarGraficoEstruturaSocietariaRequest
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public double VicunhaAcos { get; set; }
        public double RioLaco { get; set; }
        public double Nyse { get; set; }
        public double AcoesEmTesouraria { get; set; }
        public double Outros { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoEstruturaSocietariaRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public double VicunhaAcos { get; set; }
        public double RioLaco { get; set; }
        public double Nyse { get; set; }
        public double AcoesEmTesouraria { get; set; }
        public double Outros { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoEstruturaSocietariaRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoEstruturaSocietariaRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoEstruturaSocietariaRequest
    {
        public Idioma Idioma { get; set; }
    }
}
