﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.GovernancaCorporativa.Application.Interfaces;
using CSN.GovernancaCorporativa.Application.Requests.ComposicaoAcionaria;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;

namespace CSN.GovernancaCorporativa.Application.Services
{
    public class ComposicaoAcionariaGraficoEstruturaSocietariaService : IComposicaoAcionariaGraficoEstruturaSocietariaService
    {
        private readonly ICsnRepositoryAsync<EstruturaSocietaria> _csnRepository;

        public ComposicaoAcionariaGraficoEstruturaSocietariaService(ICsnRepositoryAsync<EstruturaSocietaria> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoEstruturaSocietariaRequest request)
        {
            try
            {
                var entity = new EstruturaSocietaria(request.Nome, request.Sigla, request.VicunhaAcos, request.RioLaco, request.Nyse, request.AcoesEmTesouraria, request.Outros, request.Idioma, true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoEstruturaSocietariaRequest request)
        {
            try
            {
                var usoAgua = new EstruturaSocietaria
                {
                    Id = request.Id,
                    Idioma = request.Idioma,
                    Ativo = request.Ativo,
                    Cadastro = DateTime.Now,
                    Sigla = request.Sigla,
                    AcoesEmTesouraria = request.AcoesEmTesouraria,
                    Nome = request.Nome,
                    Nyse = request.Nyse,
                    Outros = request.Outros,
                    RioLaco = request.RioLaco,
                    VicunhaAcos = request.VicunhaAcos
                };

                var result = await _csnRepository.AtualizarAsync(usoAgua, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoEstruturaSocietariaRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<EstruturaSocietaria?> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoEstruturaSocietariaRequest request)
        {
            return await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEstruturaSocietariaRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
