﻿using CSN.Core;
using CSN.Core.Domain.Interfaces;
using CSN.GovernancaCorporativa.Application.Interfaces;
using CSN.GovernancaCorporativa.Application.Services;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;
using CSN.GovernancaCorporativa.Infrastructure.Data.Context;
using CSN.GovernancaCorporativa.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CSN.GovernancaCorporativa.Infrastructure.IoC
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection AddGovernancaCorporativaContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<GovernancaCorporativaDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("CSN")));

            return services;
        }

        public static IServiceCollection AddGovernancaCorporativaServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<GovernancaCorporativaDbContext>();

            services.AddScoped<ICsnRepositoryAsync<EstruturaSocietaria>, BaseRepositoryAsync<EstruturaSocietaria>>();

            services.AddScoped<IComposicaoAcionariaGraficoEstruturaSocietariaService, ComposicaoAcionariaGraficoEstruturaSocietariaService>();

            return services;
        }
    }
}