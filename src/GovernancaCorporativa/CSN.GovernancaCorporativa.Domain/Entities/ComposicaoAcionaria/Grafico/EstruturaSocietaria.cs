﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.GovernancaCorporativa.Domain.ValueObjects;

namespace CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico
{
    public class EstruturaSocietaria : Entity
    {
        public EstruturaSocietaria() { }

        public EstruturaSocietaria(string nome,
            string sigla,
            double vicunhaAcos,
            double rioLaco,
            double nyse,
            double acoesEmTesouraria,
            double outros,
            Idioma idioma,
            bool ativo)
        {
            Nome = nome;
            Sigla = sigla;
            VicunhaAcos = vicunhaAcos;
            RioLaco = rioLaco;
            Nyse = nyse;
            AcoesEmTesouraria = acoesEmTesouraria;
            Outros = outros;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Referencia = ReferenciaTipo.ComposicaoAcionariaGraficoEstruturaSocietaria.Valor();
        }

        public EstruturaSocietaria(Guid id,
            string nome,
            string sigla,
            double vicunhaAcos,
            double rioLaco,
            double nyse,
            double acoesEmTesouraria,
            double outros,
            Idioma idioma,
            bool ativo)
        {
            Nome = nome;
            Sigla = sigla;
            VicunhaAcos = vicunhaAcos;
            RioLaco = rioLaco;
            Nyse = nyse;
            AcoesEmTesouraria = acoesEmTesouraria;
            Outros = outros;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Referencia = ReferenciaTipo.ComposicaoAcionariaGraficoEstruturaSocietaria.Valor();
        }

        public string Nome { get; set; }
        public string Sigla { get; set; }
        public double VicunhaAcos { get; set; }
        public double RioLaco { get; set; }
        public double Nyse { get; set; }
        public double AcoesEmTesouraria { get; set; }
        public double Outros { get; set; }
    }
}
