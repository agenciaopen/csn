﻿namespace CSN.GovernancaCorporativa.Domain.ValueObjects
{
    public static class Referencia
    {
        public static Guid Valor(this ReferenciaTipo me)
        {
            switch (me)
            {
                case ReferenciaTipo.ComposicaoAcionariaGraficoEstruturaSocietaria:
                    return Guid.Parse("5002a336-3ae7-40ad-8038-b324728f1c19");
                default:
                    return Guid.Empty;
            }
        }
    }

    public enum ReferenciaTipo
    {
        ComposicaoAcionariaGraficoEstruturaSocietaria,
    }
}
