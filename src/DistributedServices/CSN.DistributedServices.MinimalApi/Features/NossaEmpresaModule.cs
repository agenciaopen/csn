﻿using Carter;
using Carter.OpenApi;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;

namespace CSN.DistributedServices.MinimalApi.Features
{
    public class NossaEmpresaModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Cimento

            #region Gráfico Uso da Água Parâmetros

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/cadastrar", async (AdicionarGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("CadastrarGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/atualizar", async (AtualizarGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("AtualizarGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/excluir", async (ExcluirGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("ExcluirGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/obter-por-id", async (ObterPorIdGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<UsoAguaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("ObterPorIdGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/obter-por-slug", async (ObterPorSlugGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
                {
                    var result = await service.ObterPorSlugAsync(request);
                    return Response(result);
                })
                .AllowAnonymous()
                .Produces<ResponseResult<UsoAguaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("ObterPorSlugGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/obter-lista", async (ObterListaGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
                {
                    var result = await service.ObterListaAsync(request);
                    return Response(result);
                })
                .AllowAnonymous()
                .Produces<ResponseResult<List<UsoAguaParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("ObterListaGraficoUsoAguaParametro")
                .IncludeInOpenApi();


            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/parametros/uso-da-agua/existe", async (ExisteGraficoUsoAguaParametroRequest request, ICimentoGraficoUsoAguaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Parâmetros")
                .WithName("ExisteGraficoUsoAguaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Uso da Água Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/exibicao/uso-da-agua/cadastrar", async (AdicionarGraficoUsoAguaExibicaoRequest request, ICimentoGraficoUsoAguaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Exibição")
                .WithName("CadastrarGraficoUsoAguaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/exibicao/uso-da-agua/atualizar", async (AtualizarGraficoUsoAguaExibicaoRequest request, ICimentoGraficoUsoAguaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Exibição")
                .WithName("AtualizarGraficoUsoAguaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/exibicao/uso-da-agua/excluir", async (ExcluirGraficoUsoAguaExibicaoRequest request, ICimentoGraficoUsoAguaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Exibição")
                .WithName("ExcluirGraficoUsoAguaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/exibicao/uso-da-agua/obter-dados", async (ObterDadosGraficoUsoAguaExibicaoRequest request, ICimentoGraficoUsoAguaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IEnumerable<UsoAguaExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Exibição")
                .WithName("ObteDadosGraficoUsoAguaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimentos/grafico/exibicao/uso-da-agua/existe", async (ExisteUltimoAtivoGraficoUsoAguaExibicaoRequest request, ICimentoGraficoUsoAguaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimentos - Gráfico - Uso da Água - Exibição")
                .WithName("ExisteAtivoGraficoUsoAguaExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/cadastrar", async (AdicionarCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("CadastrarCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/atualizar", async (AtualizarCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("AtualizarCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/excluir", async (ExcluirCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExcluirCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/obter-por-id", async (ObterPorIdCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<CimentoSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorIdCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/obter-por-slug", async (ObterPorSlugCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<CimentoSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorSlugCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/obter-lista", async (ObterListaCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<CimentoSaudeSegurancaParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterListaCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/parametros/existe", async (ExisteCimentoTabelaSaudeSegurancaParametroRequest request, ICimentoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExisteCimentoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Exibicao

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/exibicao/cadastrar", async (AdicionarCimentoTabelaSaudeSegurancaExibicaoRequest request, ICimentoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Exibição")
                .WithName("CadastrarCimentoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/exibicao/atualizar", async (AtualizarCimentoTabelaSaudeSegurancaExibicaoRequest request, ICimentoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Exibição")
                .WithName("AtualizarCimentoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/exibicao/excluir", async (ExcluirCimentoTabelaSaudeSegurancaExibicaoRequest request, ICimentoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExcluirCimentoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/exibicao/obter-dados", async (ObterDadosCimentoTabelaSaudeSegurancaExibicaoRequest request, ICimentoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
                {
                    var result = await service.ObterDadosAsync(request);
                    return Response(result);
                })
                .AllowAnonymous()
                .Produces<ResponseResult<List<CimentoSaudeSegurancaExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Exibição")
                .WithName("ObterDadosCimentoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/tabela/saude-e-seguranca/exibicao/existe-ativo", async (ExisteAtivoCimentoTabelaSaudeSegurancaExibicaoRequest request, ICimentoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExisteAtivoCimentoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Emissão Específica Intensidade Parâmetros

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/cadastrar", async (AdicionarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("CadastrarCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/atualizar", async (AtualizarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("AtualizarCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/excluir", async (ExcluirCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ExcluirCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/obter-por-id", async (ObterPorIdCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<CimentoEmissaoEspecificaIntensidadeParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterPorIdCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/obter-por-slug", async (ObterPorSlugCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<CimentoEmissaoEspecificaIntensidadeParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterPorSlugCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/obter-lista", async (ObterListaCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<CimentoEmissaoEspecificaIntensidadeParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterListaCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/parametros/existe-ativo", async (ExisteCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ExisteCimentoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Emissão Específica Intensidade Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/exibicao/cadastrar", async (AdicionarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("CadastrarCimentoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/exibicao/atualizar", async (AtualizarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("AtualizarCimentoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/exibicao/excluir", async (ExcluirCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ExcluirCimentoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/exibicao/obter-dados", async (ObterDadosCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IEnumerable<CimentoEmissaoEspecificaIntensidadeExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ObterDadosCimentoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/cimento/grafico/emissao-especifica-intensidade/exibicao/existe-ativo", async (ExisteUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Cimento - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ExisteUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Mineracao

            #region Gráfico Balanço Hídrico

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/balanco-hidrico/cadastrar", async (AdicionarMineracaoGraficoBalancoHidricoRequest request, IMineracaoGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Balanço Hídrico")
                .WithName("CadastrarMineracaoGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/balanco-hidrico/atualizar", async (AtualizarMineracaoGraficoBalancoHidricoRequest request, IMineracaoGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Balanço Hídrico")
                .WithName("AtualizarMineracaoGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/balanco-hidrico/excluir", async (ExcluirMineracaoGraficoBalancoHidricoRequest request, IMineracaoGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Balanço Hídrico")
                .WithName("ExcluirMineracaoGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/balanco-hidrico/obter-ultimo-ativo", async (ObterUltimoAtivoMineracaoGraficoBalancoHidricoRequest request, IMineracaoGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<MineracaoBalancoHidrico>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Balanço Hídrico")
                .WithName("ObterUltimoAtivoMineracaoGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/balanco-hidrico/existe-ativo", async (ExisteUltimoAtivoMineracaoGraficoBalancoHidricoRequest request, IMineracaoGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Balanço Hídrico")
                .WithName("ExisteUltimoAtivoMineracaoGraficoBalancoHidrico")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Emissão Específica Intensidade Parâmetros

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/cadastrar", async (AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("CadastrarMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/atualizar", async (AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/excluir", async (ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/obter-por-id", async (ObterPorIdMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterPorIdMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/obter-por-slug", async (ObterPorSlugMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterPorSlugMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/obter-lista", async (ObterListaMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<MineracaoEmissaoEspecificaIntensidadeParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ObterListaMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/parametros/existe-ativo", async (ExisteMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Parâmetros")
                .WithName("ExisteMineracaoGraficoEmissaoEspecificaIntensidadeParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Emissão Específica Intensidade Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/exibicao/cadastrar", async (AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("CadastrarMineracaoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/exibicao/atualizar", async (AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/exibicao/excluir", async (ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/exibicao/obter-dados", async (ObterDadosMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IEnumerable<MineracaoEmissaoEspecificaIntensidadeExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ObterDadosMineracaoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/emissao-especifica-intensidade/exibicao/existe-ativo", async (ExisteUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Emissão Específica Intensidade - Exibição")
                .WithName("ExisteUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Divisão das Áreas de Preservação

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/divisao-area-preservacao/cadastrar", async (AdicionarGraficoDivisaoAreaPreservacaoRequest request, IMineracaoGraficoDivisaoAreaPreservacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Divisão das Áreas de Preservação - Exibição")
                .WithName("CadastrarMineracaoGraficoDivisaoAreaPreservacao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/divisao-area-preservacao/atualizar", async (AtualizarGraficoDivisaoAreaPreservacaoRequest request, IMineracaoGraficoDivisaoAreaPreservacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Divisão das Áreas de Preservação - Exibição")
                .WithName("AtualizarMineracaoGraficoDivisaoAreaPreservacao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/divisao-area-preservacao/excluir", async (ExcluirGraficoDivisaoAreaPreservacaoRequest request, IMineracaoGraficoDivisaoAreaPreservacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Divisão das Áreas de Preservação - Exibição")
                .WithName("ExcluirMineracaoGraficoDivisaoAreaPreservacao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/divisao-area-preservacao/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request, IMineracaoGraficoDivisaoAreaPreservacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<DivisaoAreaPreservacao>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Divisão das Áreas de Preservação - Exibição")
                .WithName("ObterUltimoAtivoGraficoDivisaoAreaPreservacao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/mineracao/grafico/divisao-area-preservacao/existe-ativo", async (ExisteUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request, IMineracaoGraficoDivisaoAreaPreservacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineração - Gráfico - Divisão das Áreas de Preservação - Exibição")
                .WithName("ExisteUltimoAtivoMineracaoGraficoDivisaoAreaPreservacao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/cadastrar", async (AdicionarMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("CadastrarMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/atualizar", async (AtualizarMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("AtualizarMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/excluir", async (ExcluirMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExcluirMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/obter-por-id", async (ObterPorIdMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<MineracaoSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorIdMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/obter-por-slug", async (ObterPorSlugMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<MineracaoSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorSlugMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/obter-lista", async (ObterListaMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<MineracaoSaudeSegurancaParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterListaMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/parametros/existe", async (ExisteMineracaoTabelaSaudeSegurancaParametroRequest request, IMineracaoTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExisteMineracaoTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/cadastrar", async (AdicionarMineracaoTabelaSaudeSegurancaExibicaoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição")
                .WithName("CadastrarMineracaoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/atualizar", async (AtualizarMineracaoTabelaSaudeSegurancaExibicaoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição")
                .WithName("AtualizarMineracaoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/excluir", async (ExcluirMineracaoTabelaSaudeSegurancaExibicaoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExcluirMineracaoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/obter-dados", async (ObterDadosMineracaoTabelaSaudeSegurancaExibicaoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<MineracaoSaudeSegurancaExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição")
                .WithName("ObterDadosMineracaoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoMineracaoTabelaSaudeSegurancaExibicaoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExisteUltimoAtivoMineracaoTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Exibição Ano

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/cadastrar", async (AdicionarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("CadastrarMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/atualizar", async (AtualizarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("AtualizarMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/excluir", async (ExcluirMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("ExcluirMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/obter-dados-por-descricao", async (ObterPorDescricaoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<MineracaoSaudeSegurancaExibicaoAno>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("ObterDadosPorDescricaoMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/obter-ano-por-id", async (ObterPorIdMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
                {
                    var result = await service.ObterAnoPorIdAsync(request);
                    return Response(result);
                })
                .AllowAnonymous()
                .Produces<ResponseResult<MineracaoSaudeSegurancaExibicaoAno>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("ObterAnoPorIdMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/Mineracao/tabela/saude-e-seguranca/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request, IMineracaoTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Mineracao - Tabela - Saúde e Segurança - Exibição - Ano")
                .WithName("ExisteAnoAtivoMineracaoTabelaSaudeSegurancaExibicaoAno")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Siderurgia

            #region Gráfico Balanço Hídrico

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/balanco-hidrico/cadastrar", async (AdicionarSiderurgiaGraficoBalancoHidricoRequest request, ISiderurgiaGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Balanço Hídrico")
                .WithName("CadastrarSiderurgiaGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/balanco-hidrico/atualizar", async (AtualizarSiderurgiaGraficoBalancoHidricoRequest request, ISiderurgiaGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Balanço Hídrico")
                .WithName("AtualizarSiderurgiaGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/balanco-hidrico/excluir", async (ExcluirSiderurgiaGraficoBalancoHidricoRequest request, ISiderurgiaGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Balanço Hídrico")
                .WithName("ExcluirSiderurgiaGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/balanco-hidrico/obter-ultimo-ativo", async (ObterUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request, ISiderurgiaGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<SiderurgiaBalancoHidrico>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Balanço Hídrico")
                .WithName("ObterUltimoAtivoSiderurgiaGraficoBalancoHidrico")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/balanco-hidrico/existe-ativo", async (ExisteUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request, ISiderurgiaGraficoBalancoHidricoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Balanço Hídrico")
                .WithName("ExisteUltimoAtivoSiderurgiaGraficoBalancoHidrico")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Intensidade Emissão Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/exibicao/cadastrar", async (AdicionarGraficoIntensidadeEmissaoExibicaoRequest request, ISiderurgiaGraficoIntensidadeEmissaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Exibição")
                .WithName("CadastrarGraficoIntensidadeEmissaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/exibicao/atualizar", async (AtualizarGraficoIntensidadeEmissaoExibicaoRequest request, ISiderurgiaGraficoIntensidadeEmissaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Exibição")
                .WithName("AtualizarGraficoIntensidadeEmissaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/exibicao/excluir", async (ExcluirGraficoIntensidadeEmissaoExibicaoRequest request, ISiderurgiaGraficoIntensidadeEmissaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Exibição")
                .WithName("ExcluirGraficoIntensidadeEmissaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/exibicao/obter-dados", async (ObterDadosGraficoIntensidadeEmissaoExibicaoRequest request, ISiderurgiaGraficoIntensidadeEmissaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<PercentualMulherUpvParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Exibição")
                .WithName("ObterDadosGraficoIntensidadeEmissaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest request, ISiderurgiaGraficoIntensidadeEmissaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Exibição")
                .WithName("ExisteUltimoAtivoGraficoIntensidadeEmissaoExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Intensidade Emissão Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/cadastrar", async (AdicionarGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("CadastrarGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/atualizar", async (AtualizarGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("AtualizarGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/excluir", async (ExcluirGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("ExcluirGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/obter-por-id", async (ObterPorIdGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IntensidadeEmissaoParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("ObterPorIdGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/obter-por-slug", async (ObterPorSlugGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IntensidadeEmissaoParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("ObterPorSlugGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/obter-lista", async (ObterListaGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<IntensidadeEmissaoParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("ObterListaGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/intensidade-emissao/parametros/existe", async (ExisteGraficoIntensidadeEmissaoParametroRequest request, ISiderurgiaGraficoIntensidadeEmissaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Intensidade Emissão - Parâmetros")
                .WithName("ExisteGraficoIntensidadeEmissaoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/cadastrar", async (AdicionarSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("CadastrarSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/atualizar", async (AtualizarSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("AtualizarSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/excluir", async (ExcluirSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExcluirSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/obter-por-id", async (ObterPorIdSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<SiderurgiaSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorIdSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/obter-por-slug", async (ObterPorSlugSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<SiderurgiaSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorSlugSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/obter-lista", async (ObterListaSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<SiderurgiaSaudeSegurancaParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterListaSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/parametros/existe", async (ExisteSiderurgiaTabelaSaudeSegurancaParametroRequest request, ISiderurgiaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExisteSiderurgiaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Exibicao

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/exibicao/cadastrar", async (AdicionarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request, ISiderurgiaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Exibição")
                .WithName("CadastrarSiderurgiaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/exibicao/atualizar", async (AtualizarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request, ISiderurgiaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Exibição")
                .WithName("AtualizarSiderurgiaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/exibicao/excluir", async (ExcluirSiderurgiaTabelaSaudeSegurancaExibicaoRequest request, ISiderurgiaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExcluirSiderurgiaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/exibicao/obter-dados", async (ObterDadosSiderurgiaTabelaSaudeSegurancaExibicaoRequest request, ISiderurgiaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<SiderurgiaSaudeSegurancaExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Exibição")
                .WithName("ObterDadosSiderurgiaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/tabela/saude-e-seguranca/exibicao/existe-ativo", async (ExisteAtivoSiderurgiaTabelaSaudeSegurancaExibicaoRequest request, ISiderurgiaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExisteAtivoSiderurgiaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Percentual Mulheres na UPV Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/cadastrar", async (AdicionarGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("CadastrarGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/atualizar", async (AtualizarGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("AtualizarGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/excluir", async (ExcluirGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("ExcluirGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/obter-por-id", async (ObterPorIdGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<PercentualMulherUpvParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("ObterPorIdGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/obter-por-slug", async (ObterPorSlugGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<PercentualMulherUpvParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("ObterPorSlugGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/obter-lista", async (ObterListaGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<PercentualMulherUpvParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("ObterListaGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/parametros/existe", async (ExisteGraficoPercentualMulherUpvParametroRequest request, ISiderurgiaGraficoPercentualMulherUpvParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Parâmetros")
                .WithName("ExisteGraficoPercentualMulherUpvParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Percentual Mulheres na UPV Exibição

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/exibicao/cadastrar", async (AdicionarGraficoPercentualMulherUpvExibicaoRequest request, ISiderurgiaGraficoPercentualMulherUpvExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Exibição")
                .WithName("CadastrarGraficoPercentualMulherUpvExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/exibicao/atualizar", async (AtualizarGraficoPercentualMulherUpvExibicaoRequest request, ISiderurgiaGraficoPercentualMulherUpvExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Exibição")
                .WithName("AtualizarGraficoPercentualMulherUpvExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/exibicao/excluir", async (ExcluirGraficoPercentualMulherUpvExibicaoRequest request, ISiderurgiaGraficoPercentualMulherUpvExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Exibição")
                .WithName("ExcluirGraficoPercentualMulherUpvExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/exibicao/obter-dados", async (ObterDadosGraficoPercentualMulherUpvExibicaoRequest request, ISiderurgiaGraficoPercentualMulherUpvExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<PercentualMulherUpvParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Exibição")
                .WithName("ObterDadosGraficoPercentualMulherUpvExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/siderurgia/grafico/percentual-mulheres-na-upv/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest request, ISiderurgiaGraficoPercentualMulherUpvExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Siderurgia - Gráfico - Percentual de Mulheres na UPV - Exibição")
                .WithName("ExisteUltimoAtivoGraficoPercentualMulherUpvExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Logística

            #region Gráfico Emissões Atmosféricas

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/grafico/emissao-atmosferica/cadastrar", async (AdicionarGraficoEmissaoAtmosfericaRequest request, ILogisticaGraficoEmissaoAtmosfericaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Gráfico - Emissão Atmosférica - Exibição")
                .WithName("CadastrarLogisticaGraficoEmissaoAtmosferica")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/grafico/emissao-atmosferica/atualizar", async (AtualizarGraficoEmissaoAtmosfericaRequest request, ILogisticaGraficoEmissaoAtmosfericaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Gráfico - Emissão Atmosférica - Exibição")
                .WithName("AtualizarLogisticaGraficoEmissaoAtmosferica")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/grafico/emissao-atmosferica/excluir", async (ExcluirGraficoEmissaoAtmosfericaRequest request, ILogisticaGraficoEmissaoAtmosfericaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Gráfico - Emissão Atmosférica - Exibição")
                .WithName("ExcluirLogisticaGraficoEmissaoAtmosferica")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/grafico/emissao-atmosferica/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoEmissaoAtmosfericaRequest request, ILogisticaGraficoEmissaoAtmosfericaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosferica>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Gráfico - Emissão Atmosférica - Exibição")
                .WithName("ObteUltimoAtivoLogisticaGraficoEmissaoAtmosferica")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/grafico/emissao-atmosferica/existe", async (ExisteUltimoAtivoGraficoEmissaoAtmosfericaRequest request, ILogisticaGraficoEmissaoAtmosfericaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Gráfico - Emissão Atmosférica - Exibição")
                .WithName("ExisteAtivoLogisticaGraficoEmissaoAtmosferica")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Parâmetro

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/cadastrar", async (AdicionarLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("CadastrarLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/atualizar", async (AtualizarLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("AtualizarLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/excluir", async (ExcluirLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExcluirLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/obter-por-id", async (ObterPorIdLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<LogisticaSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorIdLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/obter-por-slug", async (ObterPorSlugLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<LogisticaSaudeSegurancaParametro>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterPorSlugLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/obter-lista", async (ObterListaLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<LogisticaSaudeSegurancaParametro>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ObterListaLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/parametros/existe", async (ExisteLogisticaTabelaSaudeSegurancaParametroRequest request, ILogisticaTabelaSaudeSegurancaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Parâmetros")
                .WithName("ExisteLogisticaTabelaSaudeSegurancaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Saúde e Segurança Exibicao

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/exibicao/cadastrar", async (AdicionarLogisticaTabelaSaudeSegurancaExibicaoRequest request, ILogisticaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Exibição")
                .WithName("CadastrarLogisticaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/exibicao/atualizar", async (AtualizarLogisticaTabelaSaudeSegurancaExibicaoRequest request, ILogisticaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Exibição")
                .WithName("AtualizarLogisticaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/exibicao/excluir", async (ExcluirLogisticaTabelaSaudeSegurancaExibicaoRequest request, ILogisticaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExcluirLogisticaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/exibicao/obter-dados", async (ObterDadosLogisticaTabelaSaudeSegurancaExibicaoRequest request, ILogisticaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<LogisticaSaudeSegurancaExibicao>>>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Exibição")
                .WithName("ObterDadosLogisticaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/nossa-empresa/nossos-negocios/logistica/tabela/saude-e-seguranca/exibicao/existe-ativo", async (ExisteAtivoLogisticaTabelaSaudeSegurancaExibicaoRequest request, ILogisticaTabelaSaudeSegurancaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Nossa Empresa - Nossos Negócios - Logística - Tabela - Saúde e Segurança - Exibição")
                .WithName("ExisteAtivoLogisticaTabelaSaudeSegurancaExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion
        }
    }
}
