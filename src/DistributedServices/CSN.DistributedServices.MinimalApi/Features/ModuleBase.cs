﻿using CSN.Core;
using CSN.Core.Communications;

namespace CSN.DistributedServices.MinimalApi.Features
{
    public abstract class ModuleBase
    {
        protected IResult Response(ResponseResult responseResult)
        {
            if (!responseResult.Success)
                return Results.BadRequest(responseResult);

            return Results.Ok(responseResult);
        }

        protected IResult Response<T>(ResponseResult<T> responseResult)
        {
            if (!responseResult.Success)
                return Results.BadRequest(responseResult);

            return Results.Ok(responseResult);
        }
    }
}
