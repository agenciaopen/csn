﻿using Carter;
using Carter.OpenApi;
using CSN.Core;
using CSN.Core.Communications;
using CSN.GovernancaCorporativa.Application.Interfaces;
using CSN.GovernancaCorporativa.Application.Requests.ComposicaoAcionaria;
using CSN.GovernancaCorporativa.Domain.Entities.ComposicaoAcionaria.Grafico;

namespace CSN.DistributedServices.MinimalApi.Features
{
    public class GovernancaCorporativaModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Composição Ácionaria

            #region Gráfico Estrutura Societária

            app.MapPost("/api/governanca/composicao-acionaria/grafico/estrutura-societaria/cadastrar", async (AdicionarGraficoEstruturaSocietariaRequest request, IComposicaoAcionariaGraficoEstruturaSocietariaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Governança Corporativa - Composição Ácionaria - Gráfico - Estrutura Societária")
                .WithName("CadastrarGraficoEstruturaSocietaria")
                .IncludeInOpenApi();

            app.MapPost("/api/governanca/composicao-acionaria/grafico/estrutura-societaria/atualizar", async (AtualizarGraficoEstruturaSocietariaRequest request, IComposicaoAcionariaGraficoEstruturaSocietariaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Governança Corporativa - Composição Ácionaria - Gráfico - Estrutura Societária")
                .WithName("AtualizarGraficoEstruturaSocietaria")
                .IncludeInOpenApi();

            app.MapPost("/api/governanca/composicao-acionaria/grafico/estrutura-societaria/excluir", async (ExcluirGraficoEstruturaSocietariaRequest request, IComposicaoAcionariaGraficoEstruturaSocietariaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Governança Corporativa - Composição Ácionaria - Gráfico - Estrutura Societária")
                .WithName("ExcluirGraficoEstruturaSocietaria")
                .IncludeInOpenApi();

            app.MapPost("/api/governanca/composicao-acionaria/grafico/estrutura-societaria/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoEstruturaSocietariaRequest request, IComposicaoAcionariaGraficoEstruturaSocietariaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<EstruturaSocietaria>(200)
                .WithTags("Governança Corporativa - Composição Ácionaria - Gráfico - Estrutura Societária")
                .WithName("ObterUltimoAtivoGraficoEstruturaSocietaria")
                .IncludeInOpenApi();

            app.MapPost("/api/governanca/composicao-acionaria/grafico/estrutura-societaria/existe-ativo", async (ExisteUltimoAtivoGraficoEstruturaSocietariaRequest request, IComposicaoAcionariaGraficoEstruturaSocietariaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Governança Corporativa - Composição Ácionaria - Gráfico - Estrutura Societária")
                .WithName("ExisteAtivoGraficoEstruturaSocietaria")
                .IncludeInOpenApi();

            #endregion

            #endregion
        }
    }
}
