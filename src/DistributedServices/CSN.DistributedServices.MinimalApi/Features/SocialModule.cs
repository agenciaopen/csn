﻿using Carter;
using Carter.OpenApi;
using CSN.Core.Communications;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.Diversidade;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.Diversidade.Grafico;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.DistributedServices.MinimalApi.Features
{
    public class GenteIndicadoresModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Gente Indicadores

            #region Tabela Colabores Próprios Parâmetros

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/cadastrar", async (AdicionarTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("CadastrarTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/atualizar", async (AtualizarTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("AtualizarTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/excluir", async (ExcluirTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("ExcluirTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/obter-por-id", async (ObterPorIdTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ColaboradorProprioParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("ObterPorIdTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/obter-por-slug", async (ObterPorSlugTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ColaboradorProprioParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("ObterPorSlugTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/obter-litsa", async (ObterListaTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ColaboradorProprioParametro>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("ObterListaTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/parametros/existe", async (ExisteTabelaColaboradorProprioParametroRequest request, IGenteIndicadoresTabelaColaboradorProprioParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Parâmetros")
                .WithName("ExisteTabelaColaboradorProprioParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Colabores Próprios Exibição

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/exibicao/cadastrar", async (AdicionarTabelaColaboradorProprioExibicaoRequest request, IGenteIndicadoresTabelaColaboradorProprioExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Exibição")
                .WithName("CadastrarTabelaColaboradorProprioExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/exibicao/atualizar", async (AtualizarTabelaColaboradorProprioExibicaoRequest request, IGenteIndicadoresTabelaColaboradorProprioExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Exibição")
                .WithName("AtualizarTabelaColaboradorProprioExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/exibicao/excluir", async (ExcluirTabelaColaboradorProprioExibicaoRequest request, IGenteIndicadoresTabelaColaboradorProprioExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Exibição")
                .WithName("ExcluirTabelaColaboradorProprioExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/exibicao/obter-dados", async (ObterDadosTabelaColaboradorProprioExibicaoRequest request, IGenteIndicadoresTabelaColaboradorProprioExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ColaboradorProprioExibicao>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Exibição")
                .WithName("ObterDadosTabelaColaboradorProprioExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-proprio/exibicao/existe", async (ExisteUltimoAtivoTabelaColaboradorProprioExibicaoRequest request, IGenteIndicadoresTabelaColaboradorProprioExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores Próprios - Exibição")
                .WithName("ExisteTabelaColaboradorProprioExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Divisão dos Colabores por Segmento Parâmetros

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/cadastrar", async (AdicionarTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("CadastrarTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/atualizar", async (AtualizarTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("AtualizarTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/excluir", async (ExcluirTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("ExcluirTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/obter-por-id", async (ObterPorIdTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<DivisaoColaboradorSegmentoParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("ObterPorIdTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/obter-por-slug", async (ObterPorSlugTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<DivisaoColaboradorSegmentoParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("ObterPorSlugTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/obter-litsa", async (ObterListaTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<DivisaoColaboradorSegmentoParametro>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("ObterListaTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/parametros/existe", async (ExisteTabelaDivisaoColaboradorSegmentoParametroRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Parâmetros")
                .WithName("ExisteTabelaDivisaoColaboradorSegmentoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Divisão dos Colabores por Segmento Exibição

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/exibicao/cadastrar", async (AdicionarTabelaDivisaoColaboradorSegmentoExibicaoRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Exibição")
                .WithName("CadastrarTabelaDivisaoColaboradorSegmentoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/exibicao/atualizar", async (AtualizarTabelaDivisaoColaboradorSegmentoExibicaoRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Exibição")
                .WithName("AtualizarTabelaDivisaoColaboradorSegmentoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/exibicao/excluir", async (ExcluirTabelaDivisaoColaboradorSegmentoExibicaoRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Exibição")
                .WithName("ExcluirTabelaDivisaoColaboradorSegmentoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/exibicao/obter-dados", async (ObterDadosTabelaDivisaoColaboradorSegmentoExibicaoRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<DivisaoColaboradorSegmentoExibicao>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Exibição")
                .WithName("ObterDadosTabelaDivisaoColaboradorSegmentoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/divisao-de-colaborador-por-segmento/exibicao/existe", async (ExisteUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest request, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Divisão de Colaboradores por Segmento - Exibição")
                .WithName("ExisteTabelaDivisaoColaboradorSegmentoExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Colabores por Tipo de Contrato e Gênero Parâmetros

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/cadastrar", async (AdicionarTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("CadastrarTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/atualizar", async (AtualizarTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("AtualizarTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/excluir", async (ExcluirTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("ExcluirTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/obter-por-id", async (ObterPorIdTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ColaboradorTipoContratoParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("ObterPorIdTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/obter-por-slug", async (ObterPorSlugTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ColaboradorTipoContratoParametro>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("ObterPorSlugTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/obter-litsa", async (ObterListaTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ColaboradorTipoContratoParametro>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("ObterListaTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/parametros/existe", async (ExisteTabelaColaboradorTipoContratoParametroRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Parâmetros")
                .WithName("ExisteTabelaColaboradorTipoContratoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Colabores por Tipo de Contrato e Gênero Exibição

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/exibicao/cadastrar", async (AdicionarTabelaColaboradorTipoContratoExibicaoRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Exibição")
                .WithName("CadastrarTabelaColaboradorTipoContratoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/exibicao/atualizar", async (AtualizarTabelaColaboradorTipoContratoExibicaoRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Exibição")
                .WithName("AtualizarTabelaColaboradorTipoContratoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/exibicao/excluir", async (ExcluirTabelaColaboradorTipoContratoExibicaoRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Exibição")
                .WithName("ExcluirTabelaColaboradorTipoContratoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/exibicao/obter-dados", async (ObterDadosTabelaColaboradorTipoContratoExibicaoRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ColaboradorTipoContratoExibicao>>>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Exibição")
                .WithName("ObterDadosTabelaColaboradorTipoContratoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-indicadores/tabela/colaborador-tipo-contrato/exibicao/existe", async (ExisteUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest request, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Indicadores - Tabela - Colaboradores por Tipo de Contrato e Gênero - Exibição")
                .WithName("ExisteTabelaColaboradorTipoContratoExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Gente Treinamentos

            #region Tabela Número Total de Horas de Treinamento Realizados Parâmetros

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/cadastrar", async (AdicionarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("CadastrarTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/atualizar", async (AtualizarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("AtualizarTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/excluir", async (ExcluirTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("ExcluirTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/obter-por-id", async (ObterPorIdTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("ObterPorIdTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/obter-por-slug", async (ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/obter-litsa", async (ObterListaTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<NumeroTotalTreinamentoRealizadoParametro>>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("ObterListaTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/parametros/existe", async (ExisteTabelaNumeroTotalTreinamentoRealizadoParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Parâmetros")
                .WithName("ExisteTabelaNumeroTotalTreinamentoRealizadoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Número Total de Horas de Treinamento Realizados Exibição

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/exibicao/cadastrar", async (AdicionarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Exibição")
                .WithName("CadastrarTabelaNumeroTotalTreinamentoRealizadoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/exibicao/atualizar", async (AtualizarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Exibição")
                .WithName("AtualizarTabelaNumeroTotalTreinamentoRealizadoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/exibicao/excluir", async (ExcluirTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Exibição")
                .WithName("ExcluirTabelaNumeroTotalTreinamentoRealizadoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/exibicao/obter-dados", async (ObterDadosTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<NumeroTotalTreinamentoRealizadoExibicao>>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Exibição")
                .WithName("ObterDadosTabelaNumeroTotalTreinamentoRealizadoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados/exibicao/existe", async (ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Número Total de Horas de Treinamento Realizados - Exibição")
                .WithName("ExisteTabelaNumeroTotalTreinamentoRealizadoExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria Parâmetros

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/cadastrar", async (AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("CadastrarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/atualizar", async (AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/excluir", async (ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/obter-por-id", async (ObterPorIdTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("ObterPorIdTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/obter-por-slug", async (ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/obter-litsa", async (ObterListaTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("ObterListaTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/parametros/existe", async (ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Parâmetros")
                .WithName("ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria Exibição

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/exibicao/cadastrar", async (AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Exibição")
                .WithName("CadastrarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/exibicao/atualizar", async (AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Exibição")
                .WithName("AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/exibicao/excluir", async (ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Exibição")
                .WithName("ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/exibicao/obter-dados", async (ObterDadosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>>>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Exibição")
                .WithName("ObterDadosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/gente-treinamentos/tabela/numero-total-treinamentos-realizados-por-genero-categoria/exibicao/existe", async (ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Gente Treinamentos - Tabela - Tabela Número Total de Horas de Treinamento Realizados por Gênero e Categoria - Exibição")
                .WithName("ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Saúde e Segurança - Desempenho

            #region Gráfico Taxa de Gravidade Parâmetros

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/cadastrar", async (AdicionarGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("CadastrarGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/atualizar", async (AtualizarGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("AtualizarGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/excluir", async (ExcluirGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("ExcluirGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/obter-por-id", async (ObterPorIdGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TaxaGravidadeParametro>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("ObterPorIdGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/obter-por-slug", async (ObterPorSlugGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TaxaGravidadeParametro>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("ObterPorSlugGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/obter-litsa", async (ObterListaGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TaxaGravidadeParametro>>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("ObterListaGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/parametros/existe", async (ExisteGraficoTaxaGravidadeParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Parâmetros")
                .WithName("ExisteGraficoTaxaGravidadeParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Taxa de Gravidade Exibição

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/exibicao/cadastrar", async (AdicionarGraficoTaxaGravidadeExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Exibição")
                .WithName("CadastrarGraficoTaxaGravidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/exibicao/atualizar", async (AtualizarGraficoTaxaGravidadeExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Exibição")
                .WithName("AtualizarGraficoTaxaGravidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/exibicao/excluir", async (ExcluirGraficoTaxaGravidadeExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Exibição")
                .WithName("ExcluirGraficoTaxaGravidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/exibicao/obter-dados", async (ObterDadosGraficoTaxaGravidadeExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TaxaGravidadeExibicao>>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Exibição")
                .WithName("ObterDadosGraficoTaxaGravidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-gravidade/exibicao/existe", async (ExisteUltimoAtivoGraficoTaxaGravidadeExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Gravidade - Exibição")
                .WithName("ExisteGraficoTaxaGravidadeExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Taxa de Frequência Parâmetros

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/cadastrar", async (AdicionarGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("CadastrarGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/atualizar", async (AtualizarGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("AtualizarGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/excluir", async (ExcluirGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("ExcluirGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/obter-por-id", async (ObterPorIdGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TaxaFrequenciaParametro>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("ObterPorIdGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/obter-por-slug", async (ObterPorSlugGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TaxaFrequenciaParametro>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("ObterPorSlugGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/obter-litsa", async (ObterListaGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TaxaFrequenciaParametro>>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("ObterListaGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/parametros/existe", async (ExisteGraficoTaxaFrequenciaParametroRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Parâmetros")
                .WithName("ExisteGraficoTaxaFrequenciaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Taxa de Gravidade Exibição

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/exibicao/cadastrar", async (AdicionarGraficoTaxaFrequenciaExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Exibição")
                .WithName("CadastrarGraficoTaxaFrequenciaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/exibicao/atualizar", async (AtualizarGraficoTaxaFrequenciaExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Exibição")
                .WithName("AtualizarGraficoTaxaFrequenciaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/exibicao/excluir", async (ExcluirGraficoTaxaFrequenciaExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Exibição")
                .WithName("ExcluirGraficoTaxaFrequenciaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/exibicao/obter-dados", async (ObterDadosGraficoTaxaFrequenciaExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TaxaFrequenciaExibicao>>>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Exibição")
                .WithName("ObterDadosGraficoTaxaFrequenciaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/saude-seguranca-desempenho/grafico/taxa-de-frequencia/exibicao/existe", async (ExisteUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest request, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Saúde e Segurança Desempenho - Gráfico - Taxa de Frequência - Exibição")
                .WithName("ExisteGraficoTaxaFrequenciaExibicao")
                .IncludeInOpenApi();

            #endregion

            #endregion

            #region Diversidade

            #region Gráfico Percentual Mulheres na Grupo Parâmetro

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/cadastrar", async (AdicionarGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("CadastrarGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/atualizar", async (AtualizarGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("AtualizarGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/excluir", async (ExcluirGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("ExcluirGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/obter-por-id", async (ObterPorIdGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<PercentualMulherGrupoParametro>>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("ObterPorIdGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/obter-por-slug", async (ObterPorSlugGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<PercentualMulherGrupoParametro>>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("ObterPorSlugGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/obter-lista", async (ObterListaGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<PercentualMulherGrupoParametro>>>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("ObterListaGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/parametros/existe", async (ExisteGraficoPercentualMulherGrupoParametroRequest request, IDiversidadeGraficoPercentualMulherGrupoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Parâmetros")
                .WithName("ExisteGraficoPercentualMulherGrupoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Percentual Mulheres na Grupo Exibição

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/exibicao/cadastrar", async (AdicionarGraficoPercentualMulherGrupoExibicaoRequest request, IDiversidadeGraficoPercentualMulherGrupoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Exibição")
                .WithName("CadastrarGraficoPercentualMulherGrupoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/exibicao/atualizar", async (AtualizarGraficoPercentualMulherGrupoExibicaoRequest request, IDiversidadeGraficoPercentualMulherGrupoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Exibição")
                .WithName("AtualizarGraficoPercentualMulherGrupoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/exibicao/excluir", async (ExcluirGraficoPercentualMulherGrupoExibicaoRequest request, IDiversidadeGraficoPercentualMulherGrupoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Exibição")
                .WithName("ExcluirGraficoPercentualMulherGrupoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/exibicao/obter-dados", async (ObterDadosGraficoPercentualMulherGrupoExibicaoRequest request, IDiversidadeGraficoPercentualMulherGrupoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<PercentualMulherGrupoParametro>>>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Exibição")
                .WithName("ObterDadosGraficoPercentualMulherGrupoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/social/diversidade/grafico/percentual-mulheres-no-grupo/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest request, IDiversidadeGraficoPercentualMulherGrupoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Social - Diversidade - Gráfico - Percentual de Mulheres no Grupo - Exibição")
                .WithName("ExisteUltimoAtivoGraficoPercentualMulherGrupoExibicao")
                .IncludeInOpenApi();

            #endregion
            
            #endregion
        }
    }
}
