﻿using Carter;
using Carter.OpenApi;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Application.Requests.Residuos;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;

namespace CSN.DistributedServices.MinimalApi.Features
{
    public class MudancasClimaticasModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Gráfico Escopo

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/grafico/escopo/cadastrar", async (AdicionarGraficoEscopoRequest request, IMudancasClimaticasGraficoEscopoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Gráfico - Escopo")
                .WithName("CadastrarGraficoEscopo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/grafico/escopo/atualizar", async (AtualizarGraficoEscopoRequest request, IMudancasClimaticasGraficoEscopoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);

            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Gráfico - Escopo")
                .WithName("AtualizarGraficoEscopo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/grafico/escopo/excluir", async (ExcluirGraficoEscopoRequest request, IMudancasClimaticasGraficoEscopoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Gráfico - Escopo")
                .WithName("ExcluirGraficoEscopo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/grafico/escopo/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoEscopoRequest request, IMudancasClimaticasGraficoEscopoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<GraficoEscopo>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Gráfico - Escopo")
                .WithName("ObterUltimoAtivoGraficoEscopo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/grafico/escopo/existe-ativo", async (ExisteUltimoAtivoGraficoEscopoRequest request, IMudancasClimaticasGraficoEscopoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Gráfico - Escopo")
                .WithName("ExisteUltimoAtivoGraficoEscopo")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Escopo Parâmetros

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/cadastrar", async (AdicionarTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("CadastrarTabelaEscopoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/atualizar", async (AtualizarTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("AtualizarTabelaEscopoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/excluir", async (ExcluirTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("ExcluirTabelaEscopoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/obter-por-id", async (ObterPorIdTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaEscopoParametro>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("ObterPorIdTabelaEscopoParametro")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/obter-por-slug", async (ObterPorSlugTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaEscopoParametro>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("ObterPorSlugTabelaEscopoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/parametros/obter-lista", async (ObterListaTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TabelaEscopoParametro>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("ObterListaTabelaEscopoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela-escopo/parametros/existe", async (ExisteTabelaEscopoParametroRequest request, IMudancasClimaticasTabelaEscopoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Parâmetros")
                .WithName("ExisteUltimoAtivoTabelaEscopoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Escopo Exibição

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/cadastrar", async (AdicionarTabelaEscopoExibicaoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição")
                .WithName("CadastrarTabelaEscopoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/atualizar", async (AtualizarTabelaEscopoExibicaoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição")
                .WithName("AtualizarTabelaEscopoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/excluir", async (ExcluirTabelaEscopoExibicaoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição")
                .WithName("ExcluirTabelaEscopoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/obter-dados", async (ObterDadosTabelaEscopoExibicaoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<AguaEfluenteExibicao>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição")
                .WithName("ObterDadosTabelaEscopoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoTabelaEscopoExibicaoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição")
                .WithName("ExisteUltimoAtivoTabelaEscopoExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Escopo Exibição Ano

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/cadastrar", async (AdicionarTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("CadastrarTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/atualizar", async (AtualizarTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("AtualizarTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/excluir", async (ExcluirTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("ExcluirTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/obter-dados-por-descricao", async (ObterPorDescricaoTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TabelaEscopoExibicaoAno>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("ObterDadosTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/obter-ano-por-id", async (ObterPorIdTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnoPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaEscopoExibicaoAno>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("ObterPorIdTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/escopo/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoTabelaEscopoExibicaoAnoRequest request, IMudancasClimaticasTabelaEscopoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Escopo - Exibição - Ano")
                .WithName("ExisteAnoAtivoTabelaEscopoExibicaoAno")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Intensidade Parâmetros

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/cadastrar", async (AdicionarTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("CadastrarTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/atualizar", async (AtualizarTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("AtualizarTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/excluir", async (ExcluirTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("ExcluirTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/obter-por-id", async (ObterPorIdTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaIntensidadeParametro>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("ObterPorIdTabelaIntensidadeParametro")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/obter-por-slug", async (ObterPorSlugTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaIntensidadeParametro>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("ObterPorSlugTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/parametros/obter-lista", async (ObterListaTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TabelaIntensidadeParametro>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("ObterListaTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela-Intensidade/parametros/existe", async (ExisteTabelaIntensidadeParametroRequest request, IMudancasClimaticasTabelaIntensidadeParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Parâmetros")
                .WithName("ExisteUltimoAtivoTabelaIntensidadeParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Intensidade Exibição

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/cadastrar", async (AdicionarTabelaIntensidadeExibicaoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição")
                .WithName("CadastrarTabelaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/atualizar", async (AtualizarTabelaIntensidadeExibicaoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Exibição")
                .WithName("AtualizarTabelaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/excluir", async (ExcluirTabelaIntensidadeExibicaoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Exibição")
                .WithName("ExcluirTabelaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/obter-dados", async (ObterDadosTabelaIntensidadeExibicaoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<AguaEfluenteExibicao>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Exibição")
                .WithName("ObterDadosTabelaIntensidadeExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoTabelaIntensidadeExibicaoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade - Exibição")
                .WithName("ExisteUltimoAtivoTabelaIntensidadeExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Intensidade Exibição Ano

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/cadastrar", async (AdicionarTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("CadastrarTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/atualizar", async (AtualizarTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("AtualizarTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/excluir", async (ExcluirTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("ExcluirTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/obter-dados-por-descricao", async (ObterPorDescricaoTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<TabelaIntensidadeExibicaoAno>>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("ObterDadosTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/obter-ano-por-id", async (ObterPorIdTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnoPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<TabelaIntensidadeExibicaoAno>>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("ObterPorIdTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/mudancas-climaticas/tabela/intensidade/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoTabelaIntensidadeExibicaoAnoRequest request, IMudancasClimaticasTabelaIntensidadeExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Mudanças Climáticas - Tabela - Intensidade- Exibição - Ano")
                .WithName("ExisteAnoAtivoTabelaIntensidadeExibicaoAno")
                .IncludeInOpenApi();

            #endregion
        }
    }

    public class AguaModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Gráfico Captação

            app.MapPost("/api/meio-ambiente/agua/grafico/captacao/cadastrar", async (AdicionarGraficoCaptacaoRequest request, IAguaGraficoCaptacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Captação")
                .WithName("CadastrarGraficoCaptacao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/captacao/atualizar", async (AtualizarGraficoCaptacaoRequest request, IAguaGraficoCaptacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Captação")
                .WithName("AtualizarGraficoCaptacao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/captacao/excluir", async (ExcluirGraficoCaptacaoRequest request, IAguaGraficoCaptacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Captação")
                .WithName("ExcluirGraficoCaptacao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/captacao/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoCaptacaoRequest request, IAguaGraficoCaptacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<Captacao>>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Captação")
                .WithName("ObterUltimoAtivoGraficoCaptacao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/captacao/existe-ativo", async (ExisteUltimoAtivoGraficoCaptacaoRequest request, IAguaGraficoCaptacaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Captação")
                .WithName("ExisteUltimoAtivoGraficoCaptacao")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Consumo

            app.MapPost("/api/meio-ambiente/agua/grafico/consumo/cadastrar", async (AdicionarGraficoConsumoRequest request, IAguaGraficoConsumoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Consumo")
                .WithName("CadastrarGraficoConsumo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/consumo/atualizar", async (AtualizarGraficoConsumoRequest request, IAguaGraficoConsumoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Consumo")
                .WithName("AtualizarGraficoConsumo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/consumo/excluir", async (ExcluirGraficoConsumoRequest request, IAguaGraficoConsumoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Consumo")
                .WithName("ExcluirGraficoConsumo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/consumo/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoConsumoRequest request, IAguaGraficoConsumoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<Consumo>>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Consumo")
                .WithName("ObterUltimoAtivoGraficoConsumo")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/consumo/existe-ativo", async (ExisteUltimoAtivoGraficoConsumoRequest request, IAguaGraficoConsumoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Consumo")
                .WithName("ExisteUltimoAtivoGraficoConsumo")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Descarte

            app.MapPost("/api/meio-ambiente/agua/grafico/descarte/cadastrar", async (AdicionarGraficoDescarteRequest request, IAguaGraficoDescarteService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Descarte")
                .WithName("CadastrarGraficoDescarte")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/descarte/atualizar", async (AtualizarGraficoDescarteRequest request, IAguaGraficoDescarteService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Descarte")
                .WithName("AtualizarGraficoDescarte")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/descarte/excluir", async (ExcluirGraficoDescarteRequest request, IAguaGraficoDescarteService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Descarte")
                .WithName("ExcluirGraficoDescarte")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/descarte/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoDescarteRequest request, IAguaGraficoDescarteService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<Descarte>>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Descarte")
                .WithName("ObterUltimoAtivoGraficoDescarte")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/grafico/descarte/existe-ativo", async (ExisteUltimoAtivoGraficoDescarteRequest request, IAguaGraficoDescarteService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Gráfico - Descarte")
                .WithName("ExisteUltimoAtivoGraficoDescarte")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Água e Efluentes Parâmetros

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/cadastrar", async (AdicionarTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("CadastrarTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/atualizar", async (AtualizarTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("AtualizarTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/excluir", async (ExcluirTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("ExcluirTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/obter-por-id", async (ObterPorIdTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<AguaEfluenteParametro>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("ObterPorIdTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/obter-por-slug", async (ObterPorSlugTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<AguaEfluenteParametro>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("ObterPorSlugTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/obter-litsa", async (ObterListaTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<AguaEfluenteParametro>>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("ObterListaTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/parametros/existe", async (ExisteTabelaAguaEfluenteParametroRequest request, IAguaTabelaAguaEfluenteParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Parâmetros")
                .WithName("ExisteTabelaAguaEfluenteParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Consumo Água e Efluentes Exibição

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/cadastrar", async (AdicionarTabelaAguaEfluenteExibicaoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarFonteCaptacaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição")
                .WithName("CadastrarTabelaAguaEfluenteExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/atualizar", async (AtualizarTabelaAguaEfluenteExibicaoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarFonteCaptacaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição")
                .WithName("AtualizarTabelaAguaEfluenteExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/excluir", async (ExcluirTabelaAguaEfluenteExibicaoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirFonteCaptacaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição")
                .WithName("ExcluirTabelaAguaEfluenteExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/obter-dados", async (ObterDadosTabelaAguaEfluenteExibicaoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosFonteCaptacaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<AguaEfluenteExibicao>>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição")
                .WithName("ObterDadosTabelaAguaEfluenteExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoTabelaAguaEfluenteExibicaoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoFonteCaptacaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição")
                .WithName("ExisteUltimoAtivoTabelaAguaEfluenteExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Consumo Água e Efluentes Exibição Ano

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/cadastrar", async (AdicionarTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("CadastrarTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/atualizar", async (AtualizarTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("AtualizarTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/excluir", async (ExcluirTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("ExcluirTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/obter-dados-por-descricao", async (ObterPorFonteCaptacaoTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorFonteCaptacaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<AguaEfluenteExibicaoAno>>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("ObterDadosTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/obter-ano-por-id", async (ObterPorIdTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnoPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<AguaEfluenteExibicaoAno>>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("ObterPorIdTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/agua/tabela/agua-e-efluentes/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoTabelaAguaEfluenteExibicaoAnoRequest request, IAguaTabelaAguaEfluenteExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Água - Tabela - Água e Efluentes - Exibição - Ano")
                .WithName("ExisteAnoAtivoTabelaAguaEfluenteExibicaoAno")
                .IncludeInOpenApi();

            #endregion
        }
    }

    public class EnergiaModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Tabela Intensidade Energética Parâmetro

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/cadastrar", async (AdicionarTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("CadastrarTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/atualizar", async (AtualizarTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("AtualizarTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/excluir", async (ExcluirTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("ExcluirTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/obter-por-id", async (ObterPorIdTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IntensidadeEnergeticaParametro>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("ObterPorIdTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/obter-por-slug", async (ObterPorSlugTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IntensidadeEnergeticaParametro>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("ObterPorSlugTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/obter-lista", async (ObterListaTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<IntensidadeEnergeticaParametro>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("ObterListaTabelaIntensidadeEnergeticaParametro")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/parametros/existe", async (ExisteTabelaIntensidadeEnergeticaParametroRequest request, IEnergiaTabelaIntensidadeEnergeticaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade Energética - Parâmetros")
                .WithName("ExisteTabelaIntensidadeEnergetica")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Intensidade Exibição

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/cadastrar", async (AdicionarTabelaIntensidadeEnergeticaExibicaoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição")
                .WithName("CadastrarTabelaIntensidadeEnergeticaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/atualizar", async (AtualizarTabelaIntensidadeEnergeticaExibicaoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição")
                .WithName("AtualizarTabelaIntensidadeEnergeticaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/excluir", async (ExcluirTabelaIntensidadeEnergeticaExibicaoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição")
                .WithName("ExcluirTabelaIntensidadeEnergeticaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/obter-dados", async (ObterDadosTabelaIntensidadeEnergeticaExibicaoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<IntensidadeEnergeticaExibicao>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição")
                .WithName("ObterDadosTabelaIntensidadeEnergeticaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoTabelaIntensidadeEnergeticaExibicaoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição")
                .WithName("ExisteUltimoAtivoTabelaIntensidadeEnergeticaExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Intensidade Exibição Ano

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/cadastrar", async (AdicionarTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("CadastrarTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/atualizar", async (AtualizarTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("AtualizarTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/excluir", async (ExcluirTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("ExcluirTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/obter-dados-por-descricao", async (ObterPorDescricaoTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<IntensidadeEnergeticaExibicaoAno>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("ObterDadosTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/obter-ano-por-id", async (ObterPorIdTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnoPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<IntensidadeEnergeticaExibicaoAno>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("ObterPorIdTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/energia/tabela/intensidade/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoTabelaIntensidadeEnergeticaExibicaoAnoRequest request, IEnergiaTabelaIntensidadeEnergeticaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Intensidade - Exibição - Ano")
                .WithName("ExisteAnoAtivoTabelaIntensidadeEnergeticaExibicaoAno")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Consumo Energia Organização Parâmetros

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/cadastrar", async (AdicionarTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("CadastrarTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/atualizar", async (AtualizarTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("AtualizarTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/excluir", async (ExcluirTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("ExcluirTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/obter-por-id", async (ObterPorIdTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ConsumoEnergiaOrganizacaoParametro>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("ObterPorIdTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/obter-por-slug", async (ObterPorSlugTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ConsumoEnergiaOrganizacaoParametro>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("ObterPorSlugTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/obter-litsa", async (ObterListaTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ConsumoEnergiaOrganizacaoParametro>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("ObterListaTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/parametros/existe", async (ExisteTabelaConsumoEnergiaOrganizacaoParametroRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Parâmetros")
                .WithName("ExisteTabelaConsumoEnergiaOrganizacaoParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Consumo Energia Organização Exibição

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/cadastrar", async (AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição")
                .WithName("CadastrarTabelaConsumoEnergiaOrganizacaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/atualizar", async (AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição")
                .WithName("AtualizarTabelaConsumoEnergiaOrganizacaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/excluir", async (ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirDescricaoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição")
                .WithName("ExcluirTabelaConsumoEnergiaOrganizacaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/obter-dados", async (ObterDadosTabelaConsumoEnergiaOrganizacaoExibicaoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterDadosDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ConsumoEnergiaOrganizacaoExibicao>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição")
                .WithName("ObterDadosTabelaConsumoEnergiaOrganizacaoExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/existe-ultimo-ativo", async (ExisteUltimoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição")
                .WithName("ExisteUltimoAtivoTabelaConsumoEnergiaOrganizacaoExibicao")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Consumo Energia Organização Exibição Ano

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/cadastrar", async (AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("CadastrarTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/atualizar", async (AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/excluir", async (ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAnoAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/obter-dados-por-descricao", async (ObterPorDescricaoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnosPorDescricaoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<ConsumoEnergiaOrganizacaoExibicaoAno>>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("ObterDadosTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/obter-ano-por-id", async (ObterPorIdTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterAnoPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ConsumoEnergiaOrganizacaoExibicaoAno>>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("ObterPorIdTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();


            app.MapPost("/api/meio-ambiente/energia/tabela/consumo-energia-organizacao/exibicao/ano/existe-ano-ativo", async (ExisteAnoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAnoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Energia - Tabela - Consumo Energia Organização - Exibição - Ano")
                .WithName("ExisteAnoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoAno")
                .IncludeInOpenApi();

            #endregion
        }
    }

    public class ResiduosModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Gráfico Resíduos Perigosos

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-perigosos/cadastrar", async (AdicionarGraficoResiduosPerigososRequest request, IResiduosGraficoResiduosPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Perigosos")
                .WithName("CadastrarGraficoResiduosPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-perigosos/atualizar", async (AtualizarGraficoResiduosPerigososRequest request, IResiduosGraficoResiduosPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Perigosos")
                .WithName("AtualizarGraficoResiduosPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-perigosos/excluir", async (ExcluirGraficoResiduosPerigososRequest request, IResiduosGraficoResiduosPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Perigosos")
                .WithName("ExcluirGraficoResiduosPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-perigosos/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoResiduosPerigososRequest request, IResiduosGraficoResiduosPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ResiduosPerigosos>>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Perigosos")
                .WithName("ObterUltimoAtivoGraficoResiduosPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-perigosos/existe-ativo", async (ExisteUltimoAtivoGraficoResiduosPerigososRequest request, IResiduosGraficoResiduosPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Perigosos")
                .WithName("ExisteUltimoAtivoGraficoResiduosPerigosos")
                .IncludeInOpenApi();

            #endregion

            #region Gráfico Resíduos Não Perigosos

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-nao-perigosos/cadastrar", async (AdicionarGraficoResiduosNaoPerigososRequest request, IResiduosGraficoResiduosNaoPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Não Perigosos")
                .WithName("CadastrarGraficoResiduosNaoPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-nao-perigosos/atualizar", async (AtualizarGraficoResiduosNaoPerigososRequest request, IResiduosGraficoResiduosNaoPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Não Perigosos")
                .WithName("AtualizarGraficoResiduosNaoPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-nao-perigosos/excluir", async (ExcluirGraficoResiduosNaoPerigososRequest request, IResiduosGraficoResiduosNaoPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Não Perigosos")
                .WithName("ExcluirGraficoResiduosNaoPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-nao-perigosos/obter-ultimo-ativo", async (ObterUltimoAtivoGraficoResiduosNaoPerigososRequest request, IResiduosGraficoResiduosNaoPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<ResiduosNaoPerigosos>>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Não Perigosos")
                .WithName("ObterUltimoAtivoGraficoResiduosNaoPerigosos")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/residuos/grafico/residuos-nao-perigosos/existe-ativo", async (ExisteUltimoAtivoGraficoResiduosNaoPerigososRequest request, IResiduosGraficoResiduosNaoPerigososService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Resíduos - Gráfico - Resíduos Não Perigosos")
                .WithName("ExisteUltimoAtivoGraficoResiduosNaoPerigosos")
                .IncludeInOpenApi();

            #endregion
        }
    }

    public class QualidadeArModule : ModuleBase, ICarterModule
    {
        public void AddRoutes(IEndpointRouteBuilder app)
        {
            #region Tabela Emissões Atmosféricas Parâmetros

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/cadastrar", async (AdicionarTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("CadastrarTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/atualizar", async (AtualizarTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("AtualizarTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/excluir", async (ExcluirTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("ExcluirTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/obter-por-id", async (ObterPorIdTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaParametro>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("ObterPorIdTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/obter-por-slug", async (ObterPorSlugTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorSlugAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaParametro>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("ObterPorSlugTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/obter-litsa", async (ObterListaTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterListaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaParametro>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("ObterListaTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/parametros/existe", async (ExisteTabelaEmissaoAtmosfericaParametroRequest request, IQualidadeArTabelaEmissaoAtmosfericaParametroService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Parâmetros")
                .WithName("ExisteTabelaEmissaoAtmosfericaParametro")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Emissões Atmosféricas Categorias

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/cadastrar", async (AdicionarTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("CadastrarTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/atualizar", async (AtualizarTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("AtualizarTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/excluir", async (ExcluirTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("ExcluirTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/obter-por-id", async (ObterPorIdTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaCategoria>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("ObterPorIdTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/obter-por-nome", async (ObterPorNomeTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorNomeAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaCategoria>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("ObterListaTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categorias/existe", async (ExisteTabelaEmissaoAtmosfericaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categorias")
                .WithName("ExisteTabelaEmissaoAtmosfericaCategoria")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Emissões Atmosféricas Categorias Anos

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categoria/anos/cadastrar", async (AdicionarTabelaEmissaoAtmosfericaCategoriaAnoRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categoria - Anos")
                .WithName("CadastrarTabelaEmissaoAtmosfericaCategoriaAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categoria/anos/atualizar", async (AtualizarTabelaEmissaoAtmosfericaCategoriaAnoRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categoria - Anos")
                .WithName("AtualizarTabelaEmissaoAtmosfericaCategoriaAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categoria/anos/excluir", async (ExcluirTabelaEmissaoAtmosfericaCategoriaAnoRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categoria - Anos")
                .WithName("ExcluirTabelaEmissaoAtmosfericaCategoriaAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categoria/anos/obter-por-categoria", async (ObterPorCategoriaTabelaEmissaoAtmosfericaCategoriaAnoRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorCategoriaAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaCategoria>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categoria - Anos")
                .WithName("ObterPorCategoriaTabelaEmissaoAtmosfericaCategoriaAno")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/categoria/anos/existe", async (ExisteTabelaEmissaoAtmosfericaCategoriaAnoRequest request, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Categoria - Anos")
                .WithName("ExisteTabelaEmissaoAtmosfericaCategoriaAno")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Emissões Atmosféricas Fontes Emissoras

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/cadastrar", async (AdicionarTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("CadastrarTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/atualizar", async (AtualizarTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("AtualizarTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/excluir", async (ExcluirTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("ExcluirTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/obter-por-id", async (ObterPorIdTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaFonteEmissora>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("ObterPorIdTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/obter-por-nome", async (ObterPorNomeTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorNomeAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("ObterPorNomeTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/obter-por-categoria", async (ObterPorCategoriaTabelaEmissaoAtmosfericaFontesEmissorasRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
                {
                    var result = await service.ObterPorCalegoriaAsync(request);
                    return result;
                })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("ObterPorCategoriaTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/fontes-emissoras/existe", async (ExisteTabelaEmissaoAtmosfericaFonteEmissoraRequest request, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Fontes Emissoras")
                .WithName("ExisteTabelaEmissaoAtmosfericaFonteEmissora")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Emissões Atmosféricas Unidades

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/cadastrar", async (AdicionarTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("CadastrarTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/atualizar", async (AtualizarTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AtualizarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("AtualizarTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/excluir", async (ExcluirTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExcluirAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("ExcluirTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/obter-por-id", async (ObterPorIdTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorIdAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaCategoria>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("ObterPorIdTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/obter-por-nome", async (ObterPorNomeTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterPorNomeAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<List<EmissaoAtmosfericaCategoria>>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("ObterListaTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/unidades/existe", async (ExisteTabelaEmissaoAtmosfericaUnidadeRequest request, IQualidadeArTabelaEmissaoAtmosfericaUnidadeService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ExisteAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<bool>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Unidades")
                .WithName("ExisteTabelaEmissaoAtmosfericaUnidade")
                .IncludeInOpenApi();

            #endregion

            #region Tabela Emissões Atmosféricas Exibição

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/exibicao/cadastrar-por-categoria", async (AdicionarTabelaEmissaoAtmosfericaExibicaoCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Exibição")
                .WithName("CadastrarPorCategoriaTabelaEmissaoAtmosfericaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/exibicao/cadastrar-por-categorias", async (AdicionarTabelaEmissaoAtmosfericaExibicaoListaCategoriaRequest request, IQualidadeArTabelaEmissaoAtmosfericaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.AdicionarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Exibição")
                .WithName("CadastrarPorCategoriasTabelaEmissaoAtmosfericaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/exibicao/desativar", async (DesativarTabelaEmissaoAtmosfericaExibicaoRequest request, IQualidadeArTabelaEmissaoAtmosfericaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.DesativarAsync(request);
                return Response(result);
            })
                .AllowAnonymous()
                .Produces<ResponseResult>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Exibição")
                .WithName("DesativarTabelaEmissaoAtmosfericaExibicao")
                .IncludeInOpenApi();

            app.MapPost("/api/meio-ambiente/qualidade-ar/tabela/emissoes-atmosfericas/exibicao/obter-ultimo-ativo", async (ObterUltimoAtivoTabelaEmissaoAtmosfericaExibicaoRequest request, IQualidadeArTabelaEmissaoAtmosfericaExibicaoService service, HttpContext ctx, HttpResponse res) =>
            {
                var result = await service.ObterUltimoAtivoAsync(request);
                return result;
            })
                .AllowAnonymous()
                .Produces<ResponseResult<EmissaoAtmosfericaCategoria>>(200)
                .WithTags("Meio Ambiente - Qualidade Ar - Tabela - Emissões Atmosféricas - Exibição")
                .WithName("ObterUltimoAtivoTabelaEmissaoAtmosfericaExibicao")
                .IncludeInOpenApi();

            #endregion
        }
    }
}
