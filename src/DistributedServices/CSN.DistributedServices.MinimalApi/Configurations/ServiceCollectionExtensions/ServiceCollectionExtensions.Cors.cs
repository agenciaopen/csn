﻿namespace CSN.DistributedServices.MinimalApi.Configurations.ServiceCollectionExtensions
{
    public static partial class ServiceCollectionExtensions
    {

        public static IServiceCollection AddCors(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("Total",
                    builder =>
                        builder
                            .AllowAnyOrigin()
                            .AllowAnyMethod()
                            .AllowAnyHeader());
            });

            return services;
        }
    }
}
