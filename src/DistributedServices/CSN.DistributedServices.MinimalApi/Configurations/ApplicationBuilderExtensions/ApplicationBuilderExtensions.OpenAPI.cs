﻿namespace CSN.DistributedServices.MinimalApi.Configurations.ApplicationBuilderExtensions
{
    public static partial class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSwaggerEndpoints(this IApplicationBuilder app, string routePrefix)
        {
            app.UseSwagger(x => x.SerializeAsV2 = true);

            app.UseSwaggerUI(c =>
            {
                c.DefaultModelsExpandDepth(-1);
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                c.RoutePrefix = routePrefix;
            });

            return app;
        }
    }
}
