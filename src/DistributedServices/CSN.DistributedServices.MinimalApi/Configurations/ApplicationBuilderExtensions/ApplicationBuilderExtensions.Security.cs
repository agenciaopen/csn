﻿namespace CSN.DistributedServices.MinimalApi.Configurations.ApplicationBuilderExtensions
{
    public static partial class ApplicationBuilderExtensions
    {
        /// <summary>
        /// Register CORS.
        /// </summary>
        public static IApplicationBuilder UseAppCors(this IApplicationBuilder app)
        {
            app.UseCors("Total");

            return app;
        }
    }
}
