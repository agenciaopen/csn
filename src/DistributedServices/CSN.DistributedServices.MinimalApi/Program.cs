using System.Text;
using Carter;
using CSN.DistributedServices.MinimalApi.Configurations.ApplicationBuilderExtensions;
using CSN.DistributedServices.MinimalApi.Configurations.ServiceCollectionExtensions;
using CSN.GovernancaCorporativa.Infrastructure.IoC;
using CSN.MeioAmbiente.Infrastructure.IoC;
using CSN.NossaEmpresa.Infrastructure.IoC;
using CSN.Social.Infrastructure.IoC;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration(configurationBuilder => configurationBuilder.Sources.Clear());

ConfigurationManager configuration = builder.Configuration;

IWebHostEnvironment env = builder.Environment;
configuration.SetBasePath(env.ContentRootPath);
configuration.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
configuration.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
configuration.AddEnvironmentVariables();

builder.Services.AddNossaEmpresaContext(configuration);
builder.Services.AddMeioAmbienteContext(configuration);
builder.Services.AddGovernancaCorporativaContext(configuration);
builder.Services.AddSocialContext(configuration);

builder.Services.AddCors(configuration);

builder.AddSwagger();

builder.Services.AddHttpContextAccessor();
builder.Services.AddNossaEmpresaServices(configuration);
builder.Services.AddMeioAmbienteServices(configuration);
builder.Services.AddGovernancaCorporativaServices(configuration);
builder.Services.AddSocialServices(configuration);

builder.Services.AddCarter();

Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

var app = builder.Build();
var environment = app.Environment;
app
    .UseHttpsRedirection()
    .UseStaticFiles()
    .UseSwaggerEndpoints(routePrefix: string.Empty)
    .UseAppCors();

app.MapCarter();

await app.RunAsync();