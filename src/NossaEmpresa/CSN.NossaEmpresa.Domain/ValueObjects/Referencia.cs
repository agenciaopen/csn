﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSN.NossaEmpresa.Domain.ValueObjects
{
    public static class Referencia
    {
        public static Guid Valor(this ReferenciaTipo me)
        {
            switch (me)
            {
                case ReferenciaTipo.CimentoGraficoUsoAguaExibicao:
                    return Guid.Parse("e0efaf77-6e2f-418f-9586-7da4c17be2e6");
                case ReferenciaTipo.CimentoGraficoUsoAguaParametro:
                    return Guid.Parse("40ee4a2a-0aeb-47aa-8fd4-857e5d0b13da");
                case ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeParametro:
                    return Guid.Parse("2f9864a7-e400-4e7e-b182-9993ac95ff2e");
                case ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicaoAno:
                    return Guid.Parse("d8b1895e-34ec-4970-98cc-16ad215c0f99");
                case ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicao:
                    return Guid.Parse("04836580-e1d9-46a7-b8e4-337b047a4a90");
                case ReferenciaTipo.MineracaoTabelaSaudeSegurancaParametro:
                    return Guid.Parse("9767ec7a-8757-40c5-ad43-8a690fe206af");
                case ReferenciaTipo.SiderurgiaGraficoBalancoHidrico:
                    return Guid.Parse("61a6621a-e031-4809-9142-b56c1238e0c9");
                case ReferenciaTipo.SiderurgiaGraficoIntensidadeEmissaoParametro:
                    return Guid.Parse("f6511bbb-e143-44aa-8d2e-03d983a94510");
                case ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaExibicao:
                    return Guid.Parse("3fd3db8b-6dc4-4e9e-8f19-2602f680bd6b");
                case ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaParametro:
                    return Guid.Parse("e25dc001-fc59-40c5-af3a-ebc487f4af98");
                case ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeExibicao:
                    return Guid.Parse("a7109a5d-0e23-430c-a21f-037c96de9425");
                case ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvParametro:
                    return Guid.Parse("e3189399-e34f-4e85-9386-e2a1b7484e27");
                case ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvExibicao:
                    return Guid.Parse("db01c63d-073d-431a-bebd-24f55672265b");
                case ReferenciaTipo.CimentoTabelaSaudeSegurancaParametro:
                    return Guid.Parse("0594cbad-1e58-42d8-8134-7906665a8eb7");
                case ReferenciaTipo.CimentoTabelaSaudeSegurancaExibicao:
                    return Guid.Parse("e0729833-39e3-4ab7-8c6f-769efc529f1c");
                case ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeParametro:
                    return Guid.Parse("125dc14a-a957-4726-b75a-9f024e444ba2");
                case ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeExibicao:
                    return Guid.Parse("47f3131a-d6cf-40d3-801b-fe812c966327");
                case ReferenciaTipo.MineracaoGraficoDivisaoAreaPreservacao:
                    return Guid.Parse("c26babc6-d3db-46f5-acee-e7dc4192c2fa");
                case ReferenciaTipo.MineracaoGraficoBalancoHidrico:
                    return Guid.Parse("c7b04ecb-5c60-4af2-94fa-9d3970ec0339");
                case ReferenciaTipo.LogisticaGraficoEmissaoAtmosferica:
                    return Guid.Parse("f8c2fbe8-ddd7-4415-ae18-e9f341f86a6f");
                case ReferenciaTipo.LogisticaTabelaSaudeSegurancaParametro:
                    return Guid.Parse("74d790e6-034e-47c2-92df-8fbfc67e0747");
                case ReferenciaTipo.LogisticaTabelaSaudeSegurancaExibicao:
                    return Guid.Parse("8263e2ca-bf44-4929-8618-b9bfe3fae0cb");
                default:
                    return Guid.Empty;
            }
        }
    }

    public enum ReferenciaTipo
    {
        CimentoGraficoUsoAguaExibicao,
        CimentoGraficoUsoAguaParametro,
        CimentoTabelaSaudeSegurancaExibicao,
        CimentoTabelaSaudeSegurancaParametro,
        CimentoGraficoEmissaoEspecificaIntensidadeParametro,
        CimentoGraficoEmissaoEspecificaIntensidadeExibicao,
        MineracaoGraficoEmissaoEspecificaIntensidadeExibicao,
        MineracaoGraficoEmissaoEspecificaIntensidadeParametro,
        MineracaoGraficoDivisaoAreaPreservacao,
        MineracaoGraficoBalancoHidrico,
        MineracaoTabelaSaudeSegurancaExibicaoAno,
        MineracaoTabelaSaudeSegurancaExibicao,
        MineracaoTabelaSaudeSegurancaParametro,
        SiderurgiaGraficoBalancoHidrico,
        SiderurgiaGraficoIntensidadeEmissaoParametro,
        SiderurgiaGraficoIntensidadeEmissaoExibicao,
        SiderurgiaTabelaSaudeSegurancaExibicao,
        SiderurgiaTabelaSaudeSegurancaParametro,
        SiderurgiaGraficoPercentualMulherUpvParametro,
        SiderurgiaGraficoPercentualMulherUpvExibicao,
        LogisticaGraficoEmissaoAtmosferica,
        LogisticaTabelaSaudeSegurancaParametro,
        LogisticaTabelaSaudeSegurancaExibicao,
    }
}
