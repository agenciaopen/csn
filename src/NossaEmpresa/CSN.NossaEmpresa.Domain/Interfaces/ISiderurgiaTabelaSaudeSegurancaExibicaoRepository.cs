﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;

namespace CSN.NossaEmpresa.Domain.Interfaces
{
    public interface ISiderurgiaTabelaSaudeSegurancaExibicaoRepository
    {
        Task<IEnumerable<SiderurgiaSaudeSegurancaExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
