﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;

namespace CSN.NossaEmpresa.Domain.Interfaces
{
    public interface ILogisticaTabelaSaudeSegurancaExibicaoRepository
    {
        Task<IEnumerable<LogisticaSaudeSegurancaExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
