﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Domain.Interfaces
{
    public interface ISiderurgiaGraficoPercentualMulherUpvExibicaoRepository
    {
        Task<IEnumerable<PercentualMulherUpvExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
