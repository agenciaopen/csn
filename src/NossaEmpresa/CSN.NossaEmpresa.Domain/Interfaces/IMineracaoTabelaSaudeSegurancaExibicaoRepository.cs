﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;

namespace CSN.NossaEmpresa.Domain.Interfaces
{
    public interface IMineracaoTabelaSaudeSegurancaExibicaoRepository
    {
        Task<IEnumerable<MineracaoSaudeSegurancaExibicao>> ObterDadosAsync(Idioma idioma);
        Task<IEnumerable<MineracaoSaudeSegurancaExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo);
    }
}
