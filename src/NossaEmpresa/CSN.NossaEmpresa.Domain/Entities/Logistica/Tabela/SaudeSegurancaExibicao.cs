﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela
{
    public class LogisticaSaudeSegurancaExibicao : Entity
    {
        public LogisticaSaudeSegurancaExibicao() { }

        public LogisticaSaudeSegurancaExibicao(string unidade,
            string grupo,
            int ano,
            double taxa,
            Idioma idioma,
            bool ativo = true)
        {
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.LogisticaTabelaSaudeSegurancaExibicao.Valor();
        }

        public LogisticaSaudeSegurancaExibicao(Guid id,
            string unidade,
            string grupo,
            int ano,
            double taxa,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.LogisticaTabelaSaudeSegurancaExibicao.Valor();
        }

        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }

        [NotMapped]
        public LogisticaSaudeSegurancaParametro Parametro { get; set; }
    }
}
