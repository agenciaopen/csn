﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela
{
    public class LogisticaSaudeSegurancaParametro : Entity
    {
        public LogisticaSaudeSegurancaParametro() { }

        public LogisticaSaudeSegurancaParametro(string titulo,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaParametro.Valor();
        }

        public LogisticaSaudeSegurancaParametro(Guid id,
            string titulo,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Id = id;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Slug { get; set; }
        public int QuantidadeAnos { get; set; }
    }
}
