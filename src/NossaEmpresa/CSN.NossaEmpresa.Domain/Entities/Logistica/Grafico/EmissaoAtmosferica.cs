﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico
{
    public class EmissaoAtmosferica : Entity
    {
        public EmissaoAtmosferica(){}

        public EmissaoAtmosferica(string nome, 
            double escopoUm, 
            double escopoDois, 
            double escopoTres, 
            double fugitivas, 
            double residuos, 
            double combustaoMovel, 
            double combustaoEstacionario, 
            double processoIndustrial,
            Idioma idioma,
            bool ativo = true)
        {
            Nome = nome;
            EscopoUm = escopoUm;
            EscopoDois = escopoDois;
            EscopoTres = escopoTres;
            Fugitivas = fugitivas;
            Residuos = residuos;
            CombustaoMovel = combustaoMovel;
            CombustaoEstacionario = combustaoEstacionario;
            ProcessoIndustrial = processoIndustrial;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.LogisticaGraficoEmissaoAtmosferica.Valor();
        }

        public EmissaoAtmosferica(Guid id,
            string nome,
            double escopoUm,
            double escopoDois,
            double escopoTres,
            double fugitivas,
            double residuos,
            double combustaoMovel,
            double combustaoEstacionario,
            double processoIndustrial,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Nome = nome;
            EscopoUm = escopoUm;
            EscopoDois = escopoDois;
            EscopoTres = escopoTres;
            Fugitivas = fugitivas;
            Residuos = residuos;
            CombustaoMovel = combustaoMovel;
            CombustaoEstacionario = combustaoEstacionario;
            ProcessoIndustrial = processoIndustrial;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.LogisticaGraficoEmissaoAtmosferica.Valor();
        }


        public string Nome { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double EscopoTres { get; set; }
        public double Fugitivas { get; set; }
        public double Residuos { get; set; }
        public double CombustaoMovel { get; set; }
        public double CombustaoEstacionario { get; set; }
        public double ProcessoIndustrial { get; set; }
    }
}
