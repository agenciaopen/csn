﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela
{
    public class CimentoSaudeSegurancaExibicao : Entity
    {
        public CimentoSaudeSegurancaExibicao() { }

        public CimentoSaudeSegurancaExibicao(string unidade,
            string grupo,
            int ano,
            double taxa,
            Idioma idioma,
            bool ativo = true)
        {
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoTabelaSaudeSegurancaExibicao.Valor();
        }

        public CimentoSaudeSegurancaExibicao(Guid id,
            string unidade,
            string grupo,
            int ano,
            double taxa,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoTabelaSaudeSegurancaExibicao.Valor();
        }

        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }

        [NotMapped]
        public CimentoSaudeSegurancaParametro Parametro { get; set; }
    }
}
