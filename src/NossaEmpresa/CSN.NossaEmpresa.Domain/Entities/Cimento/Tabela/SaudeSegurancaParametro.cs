﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela
{
    public class CimentoSaudeSegurancaParametro : Entity
    {
        public CimentoSaudeSegurancaParametro() { }

        public CimentoSaudeSegurancaParametro(string nome,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = nome.ToSlug();
            Referencia = ReferenciaTipo.CimentoTabelaSaudeSegurancaParametro.Valor();
        }

        public CimentoSaudeSegurancaParametro(Guid id,
            string nome,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Id = id;
            Slug = nome.ToSlug();
            Referencia = ReferenciaTipo.CimentoTabelaSaudeSegurancaParametro.Valor();
        }

        public string Nome { get; set; }
        public string Slug { get; set; }
        public int QuantidadeAnos { get; set; }
    }
}
