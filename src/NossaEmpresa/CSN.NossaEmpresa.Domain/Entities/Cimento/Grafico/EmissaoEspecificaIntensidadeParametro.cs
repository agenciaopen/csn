﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico
{
    public class CimentoEmissaoEspecificaIntensidadeParametro : Entity
    {
        public CimentoEmissaoEspecificaIntensidadeParametro() { }

        public CimentoEmissaoEspecificaIntensidadeParametro(string nome,
            string sigla,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Nome = nome;
            Sigla = sigla;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
            Slug = nome.ToSlug();
        }

        public CimentoEmissaoEspecificaIntensidadeParametro(Guid id,
            string nome,
            string sigla,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Nome = nome;
            Sigla = sigla;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
            Slug = nome.ToSlug();
        }

        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }

    }
}
