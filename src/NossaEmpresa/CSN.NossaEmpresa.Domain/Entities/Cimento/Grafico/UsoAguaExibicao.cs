﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico
{
    public class UsoAguaExibicao : Entity
    {
        public UsoAguaExibicao()
        {
        }

        public UsoAguaExibicao(int ano, 
            double captacaoAgua, 
            double totalAguaDescartada, 
            double consumoAgua,
            Idioma idioma,
            bool ativo = true)
        {
            Ano = ano;
            CaptacaoAgua = captacaoAgua;
            TotalAguaDescartada = totalAguaDescartada;
            ConsumoAgua = consumoAgua;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoUsoAguaExibicao.Valor();
        }

        public UsoAguaExibicao(Guid id,
            int ano,
            double captacaoAgua,
            double totalAguaDescartada,
            double consumoAgua,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Ano = ano;
            CaptacaoAgua = captacaoAgua;
            TotalAguaDescartada = totalAguaDescartada;
            ConsumoAgua = consumoAgua;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoUsoAguaExibicao.Valor();
        }

        public int Ano { get; set; }
        public double CaptacaoAgua { get; set; }
        public double TotalAguaDescartada { get; set; }
        public double ConsumoAgua { get; set; }

        [NotMapped]
        public UsoAguaParametro Parametro { get; set; }
    }
}
