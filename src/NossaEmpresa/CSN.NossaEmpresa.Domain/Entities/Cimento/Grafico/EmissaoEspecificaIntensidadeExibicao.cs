﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico
{
    public class CimentoEmissaoEspecificaIntensidadeExibicao : Entity
    {
        public CimentoEmissaoEspecificaIntensidadeExibicao() { }

        public CimentoEmissaoEspecificaIntensidadeExibicao(int ano,
            double valor,
            Idioma idioma,
            bool ativo = true)
        {
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeExibicao.Valor();
        }

        public CimentoEmissaoEspecificaIntensidadeExibicao(Guid id,
            int ano,
            double valor,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.CimentoGraficoEmissaoEspecificaIntensidadeExibicao.Valor();
        }

        public int Ano { get; set; }
        public double Valor { get; set; }

        [NotMapped]
        public CimentoEmissaoEspecificaIntensidadeParametro Parametro { get; set; }
    }
}
