﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico
{
    public class UsoAguaParametro : Entity
    {
        public UsoAguaParametro(){}

        public UsoAguaParametro(string titulo, 
            string sigla, 
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.CimentoGraficoUsoAguaParametro.Valor();
        }

        public UsoAguaParametro(Guid id,
            string titulo,
            string sigla,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.CimentoGraficoUsoAguaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string Slug { get; set; }
        public int QuantidadeAnos { get; set; }
    }
}
