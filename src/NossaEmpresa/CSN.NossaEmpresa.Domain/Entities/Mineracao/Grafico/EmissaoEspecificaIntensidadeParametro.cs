﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico
{
    public class MineracaoEmissaoEspecificaIntensidadeParametro : Entity
    {
        public MineracaoEmissaoEspecificaIntensidadeParametro() { }

        public MineracaoEmissaoEspecificaIntensidadeParametro(string titulo, 
            string sigla,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
        }

        public MineracaoEmissaoEspecificaIntensidadeParametro(Guid id, 
            string titulo,
            string sigla,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
