﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico
{
    public class MineracaoEmissaoEspecificaIntensidadeExibicao : Entity
    {
        public MineracaoEmissaoEspecificaIntensidadeExibicao(){}

        public MineracaoEmissaoEspecificaIntensidadeExibicao(int ano,
            double escopoUm,
            double escopoDois,
            double total,
            Idioma idioma,
            bool ativo = true)
        {
            Ano = ano;
            EscopoUm = escopoUm;
            EscopoDois = escopoDois;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
        }

        public MineracaoEmissaoEspecificaIntensidadeExibicao(Guid id,
            int ano,
            double escopoUm,
            double escopoDois,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Ano = ano;
            EscopoUm = escopoUm;
            EscopoDois = escopoDois;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoEmissaoEspecificaIntensidadeParametro.Valor();
        }

        public int Ano { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double Total { get; set; }

        [NotMapped]
        public MineracaoEmissaoEspecificaIntensidadeParametro Parametro { get; set; }
    }
}
