﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico
{
    public class DivisaoAreaPreservacao : Entity
    {
        public DivisaoAreaPreservacao(){}

        public DivisaoAreaPreservacao(string nome, 
            double areaReabilitacao, 
            double areaIntervinda, 
            double areaConservacao,
            Idioma idioma,
            bool ativo = true)
        {
            Nome = nome;
            AreaReabilitacao = areaReabilitacao;
            AreaIntervinda = areaIntervinda;
            AreaConservacao = areaConservacao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoDivisaoAreaPreservacao.Valor();
        }

        public DivisaoAreaPreservacao(Guid id,
            string nome,
            double areaReabilitacao,
            double areaIntervinda,
            double areaConservacao,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Nome = nome;
            AreaReabilitacao = areaReabilitacao;
            AreaIntervinda = areaIntervinda;
            AreaConservacao = areaConservacao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoDivisaoAreaPreservacao.Valor();
        }

        public string Nome { get; set; }
        public double AreaReabilitacao { get; set; }
        public double AreaIntervinda { get; set; }
        public double AreaConservacao { get; set; }

    }
}
