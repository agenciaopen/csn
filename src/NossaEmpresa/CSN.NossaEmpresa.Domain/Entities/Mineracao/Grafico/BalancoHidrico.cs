﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico
{
    public class MineracaoBalancoHidrico : Entity
    {
        public MineracaoBalancoHidrico() { }

        public MineracaoBalancoHidrico(string titulo,
            string sigla,
            double captacao,
            double recirculacao,
            double retorno,
            double consumo,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            Captacao = captacao;
            Recirculacao = recirculacao;
            Retorno = retorno;
            Consumo = consumo;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoBalancoHidrico.Valor();
        }

        public MineracaoBalancoHidrico(Guid id,
            string titulo,
            string sigla,
            double captacao,
            double recirculacao,
            double retorno,
            double consumo,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Sigla = sigla;
            Captacao = captacao;
            Recirculacao = recirculacao;
            Retorno = retorno;
            Consumo = consumo;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoGraficoBalancoHidrico.Valor();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
    }
}
