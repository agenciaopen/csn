﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela
{
    public class MineracaoSaudeSegurancaExibicaoAno : Entity
    {
        public MineracaoSaudeSegurancaExibicaoAno(){}

        public MineracaoSaudeSegurancaExibicaoAno(Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo = true)
        {
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicaoAno.Valor();
        }

        public MineracaoSaudeSegurancaExibicaoAno(Guid id, Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo)
        {
            Id = id;
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicaoAno.Valor();
        }

        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public virtual MineracaoSaudeSegurancaExibicao Descricao { get; set; }
    }
}
