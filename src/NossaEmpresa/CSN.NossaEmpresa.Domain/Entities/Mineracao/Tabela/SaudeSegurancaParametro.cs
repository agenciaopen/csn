﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela
{
    public class MineracaoSaudeSegurancaParametro : Entity
    {
        public MineracaoSaudeSegurancaParametro() {}

        public MineracaoSaudeSegurancaParametro(string titulo, 
            string areaNegocio, 
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaParametro.Valor();
        }

        public MineracaoSaudeSegurancaParametro(Guid id,
            string titulo,
            string areaNegocio,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Id = id;
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
