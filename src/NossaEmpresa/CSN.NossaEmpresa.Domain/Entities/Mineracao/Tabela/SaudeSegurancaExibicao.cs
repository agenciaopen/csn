﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela
{
    public class MineracaoSaudeSegurancaExibicao : Entity
    {
        public MineracaoSaudeSegurancaExibicao(){}

        public MineracaoSaudeSegurancaExibicao(string descricao, Idioma idioma, bool ativo = true)
        {
            Descricao = descricao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicao.Valor();
        }

        public MineracaoSaudeSegurancaExibicao(Guid id, string descricao, Idioma idioma, bool ativo)
        {
            Id = id;
            Descricao = descricao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MineracaoTabelaSaudeSegurancaExibicao.Valor();
        }

        public string Descricao { get; set; }

        public virtual ICollection<MineracaoSaudeSegurancaExibicaoAno> Anos { get; set; }

        [NotMapped]
        public MineracaoSaudeSegurancaParametro Parametro { get; set; }
    }
}
