﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico
{
    public class IntensidadeEmissaoParametro : Entity
    {
        public IntensidadeEmissaoParametro(){}

        public IntensidadeEmissaoParametro(string titulo, 
            string sigla,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoIntensidadeEmissaoParametro.Valor();
            Slug = titulo.ToSlug();
        }

        public IntensidadeEmissaoParametro(Guid id,
            string titulo,
            string sigla,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Sigla = sigla;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoIntensidadeEmissaoParametro.Valor();
            Slug = titulo.ToSlug();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
