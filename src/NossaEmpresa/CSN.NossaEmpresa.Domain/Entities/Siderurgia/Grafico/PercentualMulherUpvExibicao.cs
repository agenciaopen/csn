﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico
{ 
    public class PercentualMulherUpvExibicao : Entity
    {
        public PercentualMulherUpvExibicao() { }

        public PercentualMulherUpvExibicao(int ano, double valor, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvExibicao.Valor();
        }

        public PercentualMulherUpvExibicao(Guid id, int ano, double valor, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvExibicao.Valor();
        }

        public int Ano { get; set; }
        public double Valor { get; set; }

        [NotMapped]
        public PercentualMulherUpvParametro Parametro { get; set; }
    }

}
