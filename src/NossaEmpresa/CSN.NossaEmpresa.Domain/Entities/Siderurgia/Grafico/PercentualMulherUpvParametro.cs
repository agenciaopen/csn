﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico
{
    public class PercentualMulherUpvParametro : Entity
    {
        public PercentualMulherUpvParametro(string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            Slug = nome.ToSlug();
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvParametro.Valor();
        }

        public PercentualMulherUpvParametro(Guid id, string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Nome = nome;
            Slug = nome.ToSlug();
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoPercentualMulherUpvParametro.Valor();
        }

        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
