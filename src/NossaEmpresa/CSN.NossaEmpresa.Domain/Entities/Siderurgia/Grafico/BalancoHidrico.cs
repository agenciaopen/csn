﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico
{
    public class SiderurgiaBalancoHidrico : Entity
    {
        public SiderurgiaBalancoHidrico(){}

        public SiderurgiaBalancoHidrico(string titulo, 
            string sigla, 
            double captacao, 
            double recirculacao, 
            double retorno, 
            double consumo, 
            double total,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            Captacao = captacao;
            Recirculacao = recirculacao;
            Retorno = retorno;
            Consumo = consumo;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoBalancoHidrico.Valor();
        }

        public SiderurgiaBalancoHidrico(Guid id, 
            string titulo,
            string sigla,
            double captacao,
            double recirculacao,
            double retorno,
            double consumo,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Sigla = sigla;
            Captacao = captacao;
            Recirculacao = recirculacao;
            Retorno = retorno;
            Consumo = consumo;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaGraficoBalancoHidrico.Valor();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
    }
}
