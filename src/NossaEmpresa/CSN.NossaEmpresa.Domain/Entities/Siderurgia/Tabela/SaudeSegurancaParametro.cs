﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela
{
    public class SiderurgiaSaudeSegurancaParametro : Entity
    {
        public SiderurgiaSaudeSegurancaParametro() { }

        public SiderurgiaSaudeSegurancaParametro(string titulo,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaParametro.Valor();
        }

        public SiderurgiaSaudeSegurancaParametro(Guid id,
            string titulo,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Id = id;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Slug { get; set; }
        public int QuantidadeAnos { get; set; }
    }
}
