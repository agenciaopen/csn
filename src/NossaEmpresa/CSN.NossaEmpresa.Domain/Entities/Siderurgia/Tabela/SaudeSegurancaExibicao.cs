﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela
{
    public class SiderurgiaSaudeSegurancaExibicao : Entity
    {
        public SiderurgiaSaudeSegurancaExibicao(){}

        public SiderurgiaSaudeSegurancaExibicao(string unidade, 
            string grupo, 
            int ano, 
            double taxa, 
            Idioma idioma,
            bool ativo = true)
        {
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaExibicao.Valor();
        }

        public SiderurgiaSaudeSegurancaExibicao(Guid id,
            string unidade,
            string grupo,
            int ano,
            double taxa,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Unidade = unidade;
            Grupo = grupo;
            Ano = ano;
            Taxa = taxa;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SiderurgiaTabelaSaudeSegurancaExibicao.Valor();
        }

        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }

        [NotMapped]
        public SiderurgiaSaudeSegurancaParametro Parametro { get; set; }
    }
}
