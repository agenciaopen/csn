﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class SiderurgiaTabelaSaudeSegurancaParametroRepository : BaseRepositoryParametersAsync<SiderurgiaSaudeSegurancaParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public SiderurgiaTabelaSaudeSegurancaParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
