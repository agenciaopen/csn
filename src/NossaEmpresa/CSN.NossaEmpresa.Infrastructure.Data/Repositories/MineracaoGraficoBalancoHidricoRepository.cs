﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoGraficoBalancoHidricoRepository : BaseRepositoryAsync<MineracaoBalancoHidrico>
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoGraficoBalancoHidricoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
