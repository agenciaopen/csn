﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoGraficoDivisaoAreaPreservacaoRepository : BaseRepositoryAsync<DivisaoAreaPreservacao>
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoGraficoDivisaoAreaPreservacaoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
