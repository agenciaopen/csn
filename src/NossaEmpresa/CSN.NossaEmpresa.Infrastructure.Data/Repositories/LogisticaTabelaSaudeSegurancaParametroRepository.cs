﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class LogisticaTabelaSaudeSegurancaParametroRepository : BaseRepositoryParametersAsync<LogisticaSaudeSegurancaParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public LogisticaTabelaSaudeSegurancaParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
