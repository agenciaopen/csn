﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class SiderurgiaGraficoPercentualMulherUpvParametroRepository : BaseRepositoryParametersAsync<PercentualMulherUpvParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public SiderurgiaGraficoPercentualMulherUpvParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
