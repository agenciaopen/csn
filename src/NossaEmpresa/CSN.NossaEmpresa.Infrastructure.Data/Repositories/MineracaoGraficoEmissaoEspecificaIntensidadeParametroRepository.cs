﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoGraficoEmissaoEspecificaIntensidadeParametroRepository : BaseRepositoryParametersAsync<MineracaoEmissaoEspecificaIntensidadeParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoGraficoEmissaoEspecificaIntensidadeParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
