﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoTabelaSaudeSegurancaParametroRepository : BaseRepositoryParametersAsync<MineracaoSaudeSegurancaParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoTabelaSaudeSegurancaParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
