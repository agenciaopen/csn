﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;
using CSN.NossaEmpresa.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class SiderurgiaGraficoPercentualMulherUpvExibicaoRepository : BaseRepositoryAsync<PercentualMulherUpvExibicao>, ISiderurgiaGraficoPercentualMulherUpvExibicaoRepository
    {
        protected readonly NossaEmpresaDbContext _context;

        public SiderurgiaGraficoPercentualMulherUpvExibicaoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<PercentualMulherUpvExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<PercentualMulherUpvParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<PercentualMulherUpvExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .Take(parametro.QuantidadeAnos)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }
    }
}
