﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoTabelaSaudeSegurancaExibicaoAnoRepository : BaseRepositoryAsync<MineracaoSaudeSegurancaExibicaoAno>
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoTabelaSaudeSegurancaExibicaoAnoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
