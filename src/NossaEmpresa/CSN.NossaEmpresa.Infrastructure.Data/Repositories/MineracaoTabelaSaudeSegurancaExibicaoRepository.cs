﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;
using CSN.NossaEmpresa.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class MineracaoTabelaSaudeSegurancaExibicaoRepository : BaseRepositoryAsync<MineracaoSaudeSegurancaExibicao>, IMineracaoTabelaSaudeSegurancaExibicaoRepository
    {
        protected readonly NossaEmpresaDbContext _context;

        public MineracaoTabelaSaudeSegurancaExibicaoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<MineracaoSaudeSegurancaExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<MineracaoSaudeSegurancaParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<MineracaoSaudeSegurancaExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .Include(x=>x.Anos.Where(x=>x.Ativo)
                    .OrderByDescending(x=>x.Ano).Take(parametro.QuantidadeAnos))
                .OrderByDescending(x => x.Cadastro)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }

        public async Task<IEnumerable<MineracaoSaudeSegurancaExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo)
        {
            var predicate = PredicateBuilder.True<MineracaoSaudeSegurancaExibicaoAno>();
            predicate = predicate.And(x => x.DescricaoId == descricaoId);
            predicate = predicate.And(x => x.Idioma == idioma);

            if(ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<MineracaoSaudeSegurancaExibicaoAno>().Where(predicate).ToListAsync();
        }
    }
}
