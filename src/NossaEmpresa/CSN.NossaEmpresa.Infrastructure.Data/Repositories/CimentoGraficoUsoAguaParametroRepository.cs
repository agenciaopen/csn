﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class CimentoGraficoUsoAguaParametroRepository : BaseRepositoryParametersAsync<UsoAguaParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public CimentoGraficoUsoAguaParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
