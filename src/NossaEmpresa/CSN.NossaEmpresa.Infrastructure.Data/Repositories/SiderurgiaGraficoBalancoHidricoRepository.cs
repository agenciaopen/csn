﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{ 
    public class SiderurgiaGraficoBalancoHidricoRepository : BaseRepositoryAsync<SiderurgiaBalancoHidrico>
    {
        protected readonly NossaEmpresaDbContext _context;

        public SiderurgiaGraficoBalancoHidricoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }

}
