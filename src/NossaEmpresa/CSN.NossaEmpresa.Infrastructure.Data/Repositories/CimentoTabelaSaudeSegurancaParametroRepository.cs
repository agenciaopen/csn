﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class CimentoTabelaSaudeSegurancaParametroRepository : BaseRepositoryParametersAsync<CimentoSaudeSegurancaParametro>
    {
        protected readonly NossaEmpresaDbContext _context;

        public CimentoTabelaSaudeSegurancaParametroRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
