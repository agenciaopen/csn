﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;
using CSN.NossaEmpresa.Infrastructure.Data.Context;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class LogisticaGraficoEmissaoAtmosfericaRepository : BaseRepositoryAsync<EmissaoAtmosferica>
    {
        protected readonly NossaEmpresaDbContext _context;

        public LogisticaGraficoEmissaoAtmosfericaRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
