﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;
using CSN.NossaEmpresa.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.NossaEmpresa.Infrastructure.Data.Repositories
{
    public class LogisticaTabelaSaudeSegurancaExibicaoRepository : BaseRepositoryAsync<LogisticaSaudeSegurancaExibicao>, ILogisticaTabelaSaudeSegurancaExibicaoRepository
    {
        protected readonly NossaEmpresaDbContext _context;

        public LogisticaTabelaSaudeSegurancaExibicaoRepository(NossaEmpresaDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<LogisticaSaudeSegurancaExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<LogisticaSaudeSegurancaParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<LogisticaSaudeSegurancaExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }
    }
}
