﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CSN.NossaEmpresa.Infrastructure.Data.Context
{
    public class NossaEmpresaDbContext : DbContext
    {
        public NossaEmpresaDbContext(DbContextOptions<NossaEmpresaDbContext> options)
        : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public async Task<bool> CommitAsync()
        {
            var sucesso = true;
            try
            {
                sucesso = await base.SaveChangesAsync() > 0;
            }
            catch (System.Exception ex)
            {
                sucesso = false;
                throw new System.Exception($"error:{ex.Message} >>>> {ex.InnerException?.Message} <<<<>>>>>{ex.InnerException?.InnerException?.Message} <<<<<");
            }

            return sucesso;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(config.GetConnectionString("CSN")); //.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                         .SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(NossaEmpresaDbContext).Assembly);
        }

        public DbSet<TEntity> DbSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }
    }

    public class DesignTimeApplicationDbContext : IDesignTimeDbContextFactory<NossaEmpresaDbContext>
    {
        public NossaEmpresaDbContext CreateDbContext(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<NossaEmpresaDbContext>();
            // pass your design time connection string here
            optionsBuilder.UseSqlServer(config.GetConnectionString("CSN"));
            return new NossaEmpresaDbContext(optionsBuilder.Options);
        }
    }
}
