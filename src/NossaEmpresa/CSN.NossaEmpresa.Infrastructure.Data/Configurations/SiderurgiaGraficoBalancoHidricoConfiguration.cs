﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class SiderurgiaGraficoBalancoHidricoConfiguration : IEntityTypeConfiguration<SiderurgiaBalancoHidrico>
    {
        public void Configure(EntityTypeBuilder<SiderurgiaBalancoHidrico> builder)
        {
            builder.ToTable("SiderurgiaGraficoBalancoHidrico", "NossaEmpresa");


            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(10)");

            builder.Property(c => c.Captacao)
                .IsRequired();

            builder.Property(c => c.Recirculacao)
                .IsRequired();

            builder.Property(c => c.Retorno)
                .IsRequired();

            builder.Property(c => c.Consumo)
                .IsRequired();

            builder.Property(c => c.Total)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
