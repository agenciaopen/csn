﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class SiderurgiaTabelaSaudeSegurancaParametroConfiguration : IEntityTypeConfiguration<SiderurgiaSaudeSegurancaParametro>
    {
        public void Configure(EntityTypeBuilder<SiderurgiaSaudeSegurancaParametro> builder)
        {
            builder.ToTable("SiderurgiaTabelaSaudeSeguranca", "Parametros");


            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
