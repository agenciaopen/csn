﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class MineracaoGraficoDivisaoAreaPreservacaoConfiguration : IEntityTypeConfiguration<DivisaoAreaPreservacao>
    {
        public void Configure(EntityTypeBuilder<DivisaoAreaPreservacao> builder)
        {
            builder.ToTable("MineracaoGraficoDivisaoAreaPreservacao", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(60)");

            builder.Property(c => c.AreaConservacao)
                .IsRequired();

            builder.Property(c => c.AreaIntervinda)
                .IsRequired();

            builder.Property(c => c.AreaReabilitacao)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
