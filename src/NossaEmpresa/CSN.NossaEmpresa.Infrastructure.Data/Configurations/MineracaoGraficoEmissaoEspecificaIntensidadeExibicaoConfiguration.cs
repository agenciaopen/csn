﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class MineracaoGraficoEmissaoEspecificaIntensidadeExibicaoConfiguration : IEntityTypeConfiguration<MineracaoEmissaoEspecificaIntensidadeExibicao>
    {
        public void Configure(EntityTypeBuilder<MineracaoEmissaoEspecificaIntensidadeExibicao> builder)
        {
            builder.ToTable("MineracaoGraficoEmissaoEspecificaIntensidade", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.EscopoUm)
                .IsRequired();

            builder.Property(c => c.EscopoDois)
                .IsRequired();

            builder.Property(c => c.Total)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
