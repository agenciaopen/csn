﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class SiderurgiaTabelaSaudeSegurancaExibicaoConfiguration : IEntityTypeConfiguration<SiderurgiaSaudeSegurancaExibicao>
    {
        public void Configure(EntityTypeBuilder<SiderurgiaSaudeSegurancaExibicao> builder)
        {
            builder.ToTable("SiderurgiaTabelaSaudeSeguranca", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Unidade)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Grupo)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.Taxa)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
