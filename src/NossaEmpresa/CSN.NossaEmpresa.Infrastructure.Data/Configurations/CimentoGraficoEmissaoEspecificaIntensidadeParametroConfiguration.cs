﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class CimentoGraficoEmissaoEspecificaIntensidadeParametroConfiguration : IEntityTypeConfiguration<CimentoEmissaoEspecificaIntensidadeParametro>
    {
        public void Configure(EntityTypeBuilder<CimentoEmissaoEspecificaIntensidadeParametro> builder)
        {
            builder.ToTable("CimentoGraficoEmissaoEspecificaIntensidade", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(10)");

            builder.Property(c => c.Nota)
                .IsRequired()
                .HasColumnType("varchar(150)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }

}
