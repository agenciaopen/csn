﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class LogisticaGraficoEmissaoAtmosfericaConfiguration : IEntityTypeConfiguration<EmissaoAtmosferica>
    {
        public void Configure(EntityTypeBuilder<EmissaoAtmosferica> builder)
        {
            builder.ToTable("LogisticaGraficoEmissaoAtmosferica", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(25)");

            builder.Property(c => c.EscopoUm)
                .IsRequired();

            builder.Property(c => c.EscopoDois)
                .IsRequired();

            builder.Property(c => c.EscopoTres)
                .IsRequired();

            builder.Property(c => c.Fugitivas)
                .IsRequired();

            builder.Property(c => c.Residuos)
                .IsRequired();

            builder.Property(c => c.CombustaoMovel)
                .IsRequired();

            builder.Property(c => c.CombustaoEstacionario)
                .IsRequired();

            builder.Property(c => c.ProcessoIndustrial)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
