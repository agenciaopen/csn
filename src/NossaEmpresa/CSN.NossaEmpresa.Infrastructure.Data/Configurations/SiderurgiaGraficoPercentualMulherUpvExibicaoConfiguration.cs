﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class SiderurgiaGraficoPercentualMulherUpvExibicaoConfiguration : IEntityTypeConfiguration<PercentualMulherUpvExibicao>
    {
        public void Configure(EntityTypeBuilder<PercentualMulherUpvExibicao> builder)
        {
            builder.ToTable("SiderurgiaGraficoPercentualMulherUpv", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.Valor)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
