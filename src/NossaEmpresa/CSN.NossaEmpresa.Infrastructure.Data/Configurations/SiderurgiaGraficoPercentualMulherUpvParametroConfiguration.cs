﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class SiderurgiaGraficoPercentualMulherUpvParametroConfiguration : IEntityTypeConfiguration<PercentualMulherUpvParametro>
    {
        public void Configure(EntityTypeBuilder<PercentualMulherUpvParametro> builder)
        {
            builder.ToTable("SiderurgiaGraficoPercentualMulherUpv", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
