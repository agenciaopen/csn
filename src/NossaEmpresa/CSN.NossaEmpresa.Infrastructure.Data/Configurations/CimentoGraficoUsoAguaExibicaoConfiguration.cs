﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class CimentoGraficoUsoAguaExibicaoConfiguration : IEntityTypeConfiguration<UsoAguaExibicao>
    {
        public void Configure(EntityTypeBuilder<UsoAguaExibicao> builder)
        {
            builder.ToTable("CimentoGraficoUsoAgua", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.CaptacaoAgua)
                .IsRequired();

            builder.Property(c => c.TotalAguaDescartada)
                .IsRequired();

            builder.Property(c => c.ConsumoAgua)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
