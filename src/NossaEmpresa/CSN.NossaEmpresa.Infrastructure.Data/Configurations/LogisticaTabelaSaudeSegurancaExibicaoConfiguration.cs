﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class LogisticaTabelaSaudeSegurancaExibicaoConfiguration : IEntityTypeConfiguration<LogisticaSaudeSegurancaExibicao>
    {
        public void Configure(EntityTypeBuilder<LogisticaSaudeSegurancaExibicao> builder)
        {
            builder.ToTable("LogisticaTabelaSaudeSeguranca", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Unidade)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Grupo)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.Taxa)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
