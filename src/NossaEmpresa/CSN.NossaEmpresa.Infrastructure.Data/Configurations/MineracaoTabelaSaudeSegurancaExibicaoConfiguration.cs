﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.NossaEmpresa.Infrastructure.Data.Configurations
{
    public class MineracaoTabelaSaudeSegurancaExibicaoConfiguration : IEntityTypeConfiguration<MineracaoSaudeSegurancaExibicao>
    {
        public void Configure(EntityTypeBuilder<MineracaoSaudeSegurancaExibicao> builder)
        {
            builder.ToTable("MineracaoTabelaSaudeSeguranca", "NossaEmpresa");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Descricao)
                .IsRequired()
                .HasColumnType("varchar(60)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasMany(x => x.Anos).WithOne(x => x.Descricao);
        }
    }
}
