﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _180520221937UsoAguaParametro : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Parametros");

            migrationBuilder.RenameTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                newName: "SiderurgiaTabelaSaudeSeguranca",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                newName: "MineracaoTabelaSaudeSeguranca",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "CimentoGraficoUsoAgua",
                schema: "NossaEmpresa",
                newName: "CimentoGraficoUsoAgua",
                newSchema: "Parametros");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.RenameTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "Parametros",
                newName: "SiderurgiaTabelaSaudeSeguranca",
                newSchema: "NossaEmpresa");

            migrationBuilder.RenameTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "Parametros",
                newName: "MineracaoTabelaSaudeSeguranca",
                newSchema: "NossaEmpresa");

            migrationBuilder.RenameTable(
                name: "CimentoGraficoUsoAgua",
                schema: "Parametros",
                newName: "CimentoGraficoUsoAgua",
                newSchema: "NossaEmpresa");
        }
    }
}
