﻿// <auto-generated />
using System;
using CSN.NossaEmpresa.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    [DbContext(typeof(NossaEmpresaDbContext))]
    [Migration("20220524111201_240520220811")]
    partial class _240520220811
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico.UsoAguaExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("CaptacaoAgua")
                        .HasColumnType("float");

                    b.Property<double>("ConsumoAgua")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("TotalAguaDescartada")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("CimentoGraficoUsoAgua", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico.UsoAguaParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("CimentoGraficoUsoAgua", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico.EmissaoEspecificaIntensidadeExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("EscopoDois")
                        .HasColumnType("float");

                    b.Property<double>("EscopoUm")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Total")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("MineracaoGraficoEmissaoEspecificaIntensidade", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico.EmissaoEspecificaIntensidadeParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("MineracaoGraficoEmissaoEspecificaIntensidade", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<string>("Descricao")
                        .IsRequired()
                        .HasColumnType("varchar(60)");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("Id");

                    b.ToTable("MineracaoTabelaSaudeSeguranca", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaExibicaoAno", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<Guid>("DescricaoId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Valor")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.HasIndex("DescricaoId");

                    b.ToTable("MineracaoTabelaSaudeSegurancaAno", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AreaNegocio")
                        .IsRequired()
                        .HasColumnType("varchar(1000)");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(70)");

                    b.HasKey("Id");

                    b.ToTable("MineracaoTabelaSaudeSeguranca", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico.BalancoHidrico", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("Captacao")
                        .HasColumnType("float");

                    b.Property<double>("Consumo")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<double>("Recirculacao")
                        .HasColumnType("float");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Retorno")
                        .HasColumnType("float");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<double>("Total")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("SiderurgiaGraficoBalancoHidrico", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico.IntensidadeEmissaoExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Valor")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("SiderurgiaGraficoIntensidadeEmissao", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico.IntensidadeEmissaoParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("Nota")
                        .IsRequired()
                        .HasColumnType("varchar(150)");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(10)");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("SiderurgiaGraficoIntensidadeEmissao", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico.PercentualMulherUpvParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(30)");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(30)");

                    b.HasKey("Id");

                    b.ToTable("SiderurgiaGraficoPercentualMulherUpvParametro", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela.SiderurgiaSaudeSegurancaExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<string>("Grupo")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("ParametroId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("TabelaParametroId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Taxa")
                        .HasColumnType("float");

                    b.Property<string>("Unidade")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.HasIndex("ParametroId");

                    b.ToTable("SiderurgiaTabelaSaudeSeguranca", "NossaEmpresa");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela.SiderurgiaSaudeSegurancaParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("SiderurgiaTabelaSaudeSeguranca", "Parametros");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaExibicaoAno", b =>
                {
                    b.HasOne("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaExibicao", "Descricao")
                        .WithMany("Anos")
                        .HasForeignKey("DescricaoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Descricao");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela.SiderurgiaSaudeSegurancaExibicao", b =>
                {
                    b.HasOne("CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela.SiderurgiaSaudeSegurancaParametro", "Parametro")
                        .WithMany()
                        .HasForeignKey("ParametroId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Parametro");
                });

            modelBuilder.Entity("CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela.MineracaoSaudeSegurancaExibicao", b =>
                {
                    b.Navigation("Anos");
                });
#pragma warning restore 612, 618
        }
    }
}
