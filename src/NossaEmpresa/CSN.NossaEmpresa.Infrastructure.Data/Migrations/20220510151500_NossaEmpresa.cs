﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class NossaEmpresa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "NossaEmpresa");

            migrationBuilder.CreateTable(
                name: "CimentoGraficoUsoAgua",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CimentoGraficoUsoAgua", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MineracaoGraficoEmissaoEspecificaIntensidade",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MineracaoGraficoEmissaoEspecificaIntensidade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(70)", nullable: false),
                    AreaNegocio = table.Column<string>(type: "varchar(1000)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MineracaoTabelaSaudeSeguranca", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SiderurgiaGraficoBalancoHidrico",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Captacao = table.Column<double>(type: "float", nullable: false),
                    Recirculacao = table.Column<double>(type: "float", nullable: false),
                    Retorno = table.Column<double>(type: "float", nullable: false),
                    Consumo = table.Column<double>(type: "float", nullable: false),
                    Total = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaGraficoBalancoHidrico", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SiderurgiaGraficoIntensidadeEmissao",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Nota = table.Column<string>(type: "varchar(150)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaGraficoIntensidadeEmissao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaTabelaSaudeSeguranca", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CimentoGraficoUsoAgua",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "MineracaoGraficoEmissaoEspecificaIntensidade",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "SiderurgiaGraficoBalancoHidrico",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "SiderurgiaGraficoIntensidadeEmissao",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "NossaEmpresa");
        }
    }
}
