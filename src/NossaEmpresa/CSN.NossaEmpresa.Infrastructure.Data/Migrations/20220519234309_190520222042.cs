﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _190520222042 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId1",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "Id", "Idioma" });

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "ParametroId1", "ParametroIdioma" });

            migrationBuilder.AddForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "ParametroId1", "ParametroIdioma" },
                principalSchema: "Parametros",
                principalTable: "CimentoGraficoUsoAgua",
                principalColumns: new[] { "Id", "Idioma" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroId1",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "CimentoGraficoUsoAgua",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
