﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _240520220811 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropForeignKey(
                name: "FK_MineracaoGraficoEmissaoEspecificaIntensidade_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropForeignKey(
                name: "FK_SiderurgiaGraficoIntensidadeEmissao_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropIndex(
                name: "IX_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropIndex(
                name: "IX_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.CreateTable(
                name: "SiderurgiaGraficoPercentualMulherUpvParametro",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(30)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(30)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaGraficoPercentualMulherUpvParametro", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SiderurgiaGraficoPercentualMulherUpvParametro",
                schema: "Parametros");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "CimentoGraficoUsoAgua",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MineracaoGraficoEmissaoEspecificaIntensidade_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "MineracaoGraficoEmissaoEspecificaIntensidade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SiderurgiaGraficoIntensidadeEmissao_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "SiderurgiaGraficoIntensidadeEmissao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
