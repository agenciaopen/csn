﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _270520220840 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogisticaTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Unidade = table.Column<string>(type: "varchar(50)", nullable: false),
                    Grupo = table.Column<string>(type: "varchar(20)", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Taxa = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogisticaTabelaSaudeSeguranca", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogisticaTabelaSaudeSeguranca",
                schema: "NossaEmpresa");
        }
    }
}
