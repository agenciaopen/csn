﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _190520221343 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CimentoGraficoUsoAgua",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    CaptacaoAgua = table.Column<double>(type: "float", nullable: false),
                    TotalAguaDescartada = table.Column<double>(type: "float", nullable: false),
                    ConsumoAgua = table.Column<double>(type: "float", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CimentoGraficoUsoAgua", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "CimentoGraficoUsoAgua",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Unidade = table.Column<string>(type: "varchar(50)", nullable: false),
                    Grupo = table.Column<string>(type: "varchar(20)", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Taxa = table.Column<double>(type: "float", nullable: false),
                    TabelaParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaTabelaSaudeSeguranca", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SiderurgiaTabelaSaudeSeguranca_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "SiderurgiaTabelaSaudeSeguranca",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                column: "ParametroId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CimentoGraficoUsoAgua",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "SiderurgiaTabelaSaudeSeguranca",
                schema: "NossaEmpresa");
        }
    }
}
