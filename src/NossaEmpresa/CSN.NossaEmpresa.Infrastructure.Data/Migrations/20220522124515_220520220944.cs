﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _220520220944 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nota",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "Sigla",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "Titulo",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.AddColumn<int>(
                name: "Ano",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<double>(
                name: "Valor",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "QuantidadeAnos",
                schema: "Parametros",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "SiderurgiaGraficoIntensidadeEmissao",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Nota = table.Column<string>(type: "varchar(150)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SiderurgiaGraficoIntensidadeEmissao", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "CimentoGraficoUsoAgua",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SiderurgiaGraficoIntensidadeEmissao_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "SiderurgiaGraficoIntensidadeEmissao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropForeignKey(
                name: "FK_SiderurgiaGraficoIntensidadeEmissao_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropTable(
                name: "SiderurgiaGraficoIntensidadeEmissao",
                schema: "Parametros");

            migrationBuilder.DropIndex(
                name: "IX_SiderurgiaGraficoIntensidadeEmissao_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "Ano",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "Valor",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "QuantidadeAnos",
                schema: "Parametros",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.AddColumn<string>(
                name: "Nota",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "varchar(150)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Sigla",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "varchar(10)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Titulo",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");
        }
    }
}
