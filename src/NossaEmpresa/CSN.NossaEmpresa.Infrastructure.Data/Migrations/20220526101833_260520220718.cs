﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _260520220718 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Nota = table.Column<string>(type: "varchar(150)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CimentoGrafitoEmissaoEspecificaIntensidadeParametro", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                schema: "Parametros");
        }
    }
}
