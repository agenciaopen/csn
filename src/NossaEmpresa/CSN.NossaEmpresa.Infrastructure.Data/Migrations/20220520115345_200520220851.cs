﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _200520220851 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroId1",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoBalancoHidrico",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSegurancaAno",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                newName: "Referencia");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoBalancoHidrico",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSegurancaAno",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                newName: "ReferenciaIdioma");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId1",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "ParametroId1", "ParametroIdioma" });

            migrationBuilder.AddForeignKey(
                name: "FK_CimentoGraficoUsoAgua_CimentoGraficoUsoAgua_ParametroId1_ParametroIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "ParametroId1", "ParametroIdioma" },
                principalSchema: "Parametros",
                principalTable: "CimentoGraficoUsoAgua",
                principalColumns: new[] { "Id", "Idioma" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
