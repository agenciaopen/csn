﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _260520220824 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SiderurgiaGraficoPercentualMulherUpvParametro",
                schema: "Parametros",
                table: "SiderurgiaGraficoPercentualMulherUpvParametro");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                schema: "Parametros",
                table: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro");

            migrationBuilder.RenameTable(
                name: "SiderurgiaGraficoPercentualMulherUpvParametro",
                schema: "Parametros",
                newName: "SiderurgiaGraficoPercentualMulherUpv",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                schema: "Parametros",
                newName: "CimentoGrafitoEmissaoEspecificaIntensidade",
                newSchema: "Parametros");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SiderurgiaGraficoPercentualMulherUpv",
                schema: "Parametros",
                table: "SiderurgiaGraficoPercentualMulherUpv",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGrafitoEmissaoEspecificaIntensidade",
                schema: "Parametros",
                table: "CimentoGrafitoEmissaoEspecificaIntensidade",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidade",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CimentoGrafitoEmissaoEspecificaIntensidade", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidade",
                schema: "NossaEmpresa");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SiderurgiaGraficoPercentualMulherUpv",
                schema: "Parametros",
                table: "SiderurgiaGraficoPercentualMulherUpv");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGrafitoEmissaoEspecificaIntensidade",
                schema: "Parametros",
                table: "CimentoGrafitoEmissaoEspecificaIntensidade");

            migrationBuilder.RenameTable(
                name: "SiderurgiaGraficoPercentualMulherUpv",
                schema: "Parametros",
                newName: "SiderurgiaGraficoPercentualMulherUpvParametro",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "CimentoGrafitoEmissaoEspecificaIntensidade",
                schema: "Parametros",
                newName: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                newSchema: "Parametros");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SiderurgiaGraficoPercentualMulherUpvParametro",
                schema: "Parametros",
                table: "SiderurgiaGraficoPercentualMulherUpvParametro",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                schema: "Parametros",
                table: "CimentoGrafitoEmissaoEspecificaIntensidadeParametro",
                column: "Id");
        }
    }
}
