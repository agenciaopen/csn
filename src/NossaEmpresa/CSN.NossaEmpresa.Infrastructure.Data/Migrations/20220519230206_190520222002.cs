﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _190520222002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoBalancoHidrico",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Descricao = table.Column<string>(type: "varchar(60)", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    ReferenciaIdioma = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MineracaoTabelaSaudeSeguranca", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MineracaoTabelaSaudeSeguranca_MineracaoTabelaSaudeSeguranca_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "MineracaoTabelaSaudeSeguranca",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MineracaoTabelaSaudeSegurancaAno",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DescricaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    ReferenciaIdioma = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MineracaoTabelaSaudeSegurancaAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MineracaoTabelaSaudeSegurancaAno_MineracaoTabelaSaudeSeguranca_DescricaoId",
                        column: x => x.DescricaoId,
                        principalSchema: "NossaEmpresa",
                        principalTable: "MineracaoTabelaSaudeSeguranca",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MineracaoTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_MineracaoTabelaSaudeSegurancaAno_DescricaoId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSegurancaAno",
                column: "DescricaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MineracaoTabelaSaudeSegurancaAno",
                schema: "NossaEmpresa");

            migrationBuilder.DropTable(
                name: "MineracaoTabelaSaudeSeguranca",
                schema: "NossaEmpresa");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoIntensidadeEmissao");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "SiderurgiaGraficoBalancoHidrico");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MineracaoTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "NossaEmpresa",
                table: "CimentoGraficoUsoAgua");
        }
    }
}
