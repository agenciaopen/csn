﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _250520220821 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SiderurgiaTabelaSaudeSeguranca_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropIndex(
                name: "IX_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "TabelaParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca");

            migrationBuilder.CreateTable(
                name: "CimentoTabelaSaudeSeguranca",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Unidade = table.Column<string>(type: "varchar(50)", nullable: false),
                    Grupo = table.Column<string>(type: "varchar(20)", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Taxa = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CimentoTabelaSaudeSeguranca", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CimentoTabelaSaudeSeguranca",
                schema: "NossaEmpresa");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "TabelaParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_SiderurgiaTabelaSaudeSeguranca_SiderurgiaTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "SiderurgiaTabelaSaudeSeguranca",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "SiderurgiaTabelaSaudeSeguranca",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
