﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _210520221201 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MineracaoTabelaSaudeSeguranca_MineracaoTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca");

            migrationBuilder.DropIndex(
                name: "IX_MineracaoTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca");

            migrationBuilder.DropColumn(
                name: "Sigla",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "Titulo",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.AddColumn<int>(
                name: "Ano",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "EscopoDois",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "EscopoUm",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<double>(
                name: "Total",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "MineracaoGraficoEmissaoEspecificaIntensidade",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MineracaoGraficoEmissaoEspecificaIntensidade", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_MineracaoGraficoEmissaoEspecificaIntensidade_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "MineracaoGraficoEmissaoEspecificaIntensidade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MineracaoGraficoEmissaoEspecificaIntensidade_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropTable(
                name: "MineracaoGraficoEmissaoEspecificaIntensidade",
                schema: "Parametros");

            migrationBuilder.DropIndex(
                name: "IX_MineracaoGraficoEmissaoEspecificaIntensidade_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "Ano",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "EscopoDois",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "EscopoUm",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.DropColumn(
                name: "Total",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "Sigla",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "varchar(10)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Titulo",
                schema: "NossaEmpresa",
                table: "MineracaoGraficoEmissaoEspecificaIntensidade",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_MineracaoTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_MineracaoTabelaSaudeSeguranca_MineracaoTabelaSaudeSeguranca_ParametroId",
                schema: "NossaEmpresa",
                table: "MineracaoTabelaSaudeSeguranca",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "MineracaoTabelaSaudeSeguranca",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
