﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _270520220742 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LogisticaGraficoEmissaoAtmosferica",
                schema: "NossaEmpresa",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(25)", nullable: false),
                    EscopoUm = table.Column<double>(type: "float", nullable: false),
                    EscopoDois = table.Column<double>(type: "float", nullable: false),
                    EscopoTres = table.Column<double>(type: "float", nullable: false),
                    Fugitivas = table.Column<double>(type: "float", nullable: false),
                    Residuos = table.Column<double>(type: "float", nullable: false),
                    CombustaoMovel = table.Column<double>(type: "float", nullable: false),
                    CombustaoEstacionario = table.Column<double>(type: "float", nullable: false),
                    ProcessoIndustrial = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LogisticaGraficoEmissaoAtmosferica", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LogisticaGraficoEmissaoAtmosferica",
                schema: "NossaEmpresa");
        }
    }
}
