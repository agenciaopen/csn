﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.NossaEmpresa.Infrastructure.Data.Migrations
{
    public partial class _200520220904 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CimentoGraficoUsoAgua",
                schema: "Parametros",
                table: "CimentoGraficoUsoAgua",
                columns: new[] { "Id", "Idioma" });
        }
    }
}
