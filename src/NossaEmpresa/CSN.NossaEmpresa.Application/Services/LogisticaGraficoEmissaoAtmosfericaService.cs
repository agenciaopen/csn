﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class LogisticaGraficoEmissaoAtmosfericaService : ILogisticaGraficoEmissaoAtmosfericaService
    {
        private readonly ICsnRepositoryAsync<EmissaoAtmosferica> _repository;

        public LogisticaGraficoEmissaoAtmosfericaService(ICsnRepositoryAsync<EmissaoAtmosferica> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoEmissaoAtmosfericaRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosferica(request.Nome, request.EscopoUm, request.EscopoDois, request.EscopoTres, request.Fugitivas, request.Residuos, request.CombustaoMovel, request.CombustaoEstacionario, request.ProcessoIndustrial, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoEmissaoAtmosfericaRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosferica(request.Id, request.Nome, request.EscopoUm, request.EscopoDois, request.EscopoTres, request.Fugitivas, request.Residuos, request.CombustaoMovel, request.CombustaoEstacionario, request.ProcessoIndustrial, request.Idioma);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoEmissaoAtmosfericaRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<EmissaoAtmosferica>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoEmissaoAtmosfericaRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<EmissaoAtmosferica>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEmissaoAtmosfericaRequest request)
        {
            return await _repository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
