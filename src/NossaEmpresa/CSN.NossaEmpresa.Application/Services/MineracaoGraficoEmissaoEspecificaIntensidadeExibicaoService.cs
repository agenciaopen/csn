﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService : IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService
    {
        private readonly ICsnRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeExibicao> _csnRepository;
        private readonly IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRepository _repository;

        public MineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService(ICsnRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeExibicao> csnRepository, IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new MineracaoEmissaoEspecificaIntensidadeExibicao(request.Ano, request.EscopoUm, request.EscopoDois, request.Total, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new MineracaoEmissaoEspecificaIntensidadeExibicao(request.Id, request.Ano, request.EscopoUm, request.EscopoDois, request.Total, request.Idioma, request.Ativo);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<MineracaoEmissaoEspecificaIntensidadeExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<MineracaoEmissaoEspecificaIntensidadeExibicao>>> ObterDadosAsync(ObterDadosMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<MineracaoEmissaoEspecificaIntensidadeExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
