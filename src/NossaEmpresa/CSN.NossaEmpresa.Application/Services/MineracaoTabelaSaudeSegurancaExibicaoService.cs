﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoTabelaSaudeSegurancaExibicaoService : IMineracaoTabelaSaudeSegurancaExibicaoService
    {
        private readonly ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicao> _csnDescricaoRepository;
        private readonly IMineracaoTabelaSaudeSegurancaExibicaoRepository _descricaoRepository;
        private readonly ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicaoAno> _csnAnoRepository;

        public MineracaoTabelaSaudeSegurancaExibicaoService(ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicao> csnDescricaoRepository, IMineracaoTabelaSaudeSegurancaExibicaoRepository descricaoRepository, ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicaoAno> csnAnoRepository)
        {
            _csnDescricaoRepository = csnDescricaoRepository;
            _descricaoRepository = descricaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarDescricaoAsync(AdicionarMineracaoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaExibicao(request.Descricao, request.Idioma);
                var result = await _csnDescricaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarDescricaoAsync(AtualizarMineracaoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaExibicao(request.Id, request.Descricao, request.Idioma, request.Ativo);
                var result = await _csnDescricaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirDescricaoAsync(ExcluirMineracaoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var result = await _csnDescricaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicao>>> ObterDadosDescricaoAsync(ObterDadosMineracaoTabelaSaudeSegurancaExibicaoRequest request)
        {
            var entity = await _descricaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoMineracaoTabelaSaudeSegurancaExibicaoRequest request)
        {
            return await _csnDescricaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaExibicaoAno(request.DescricaoId, request.Ano, request.Valor, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaExibicaoAno(request.Id, request.DescricaoId, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            var entity = await _descricaoRepository.ObterAnosPorDescricaoAsync(request.DescricaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<MineracaoSaudeSegurancaExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x=>x.DescricaoId == request.DescricaoId, o=>o.Cadastro);
            return new ResponseResult<MineracaoSaudeSegurancaExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
