﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoGraficoBalancoHidricoService : IMineracaoGraficoBalancoHidricoService
    {
        private readonly ICsnRepositoryAsync<MineracaoBalancoHidrico> _csnRepository;

        public MineracaoGraficoBalancoHidricoService(ICsnRepositoryAsync<MineracaoBalancoHidrico> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoBalancoHidricoRequest request)
        {
            try
            {
                var entity = new MineracaoBalancoHidrico(request.Titulo, request.Sigla, request.Captacao, request.Recirculacao, request.Retorno, request.Consumo, request.Total, request.Idioma, true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoBalancoHidricoRequest request)
        {
            try
            {
                var entity = new MineracaoBalancoHidrico(request.Id, request.Titulo, request.Sigla, request.Captacao, request.Recirculacao, request.Retorno, request.Consumo, request.Total, request.Idioma, request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoBalancoHidricoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<MineracaoBalancoHidrico?> ObterUltimoAtivoAsync(ObterUltimoAtivoMineracaoGraficoBalancoHidricoRequest request)
        {
            return await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoMineracaoGraficoBalancoHidricoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
