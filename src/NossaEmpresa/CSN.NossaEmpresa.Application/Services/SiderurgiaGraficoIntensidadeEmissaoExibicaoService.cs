﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class SiderurgiaGraficoIntensidadeEmissaoExibicaoService : ISiderurgiaGraficoIntensidadeEmissaoExibicaoService
    {
        private readonly ICsnRepositoryAsync<IntensidadeEmissaoExibicao> _csnRepository;
        private readonly ISiderurgiaGraficoIntensidadeEmissaoExibicaoRepository _repository;

        public SiderurgiaGraficoIntensidadeEmissaoExibicaoService(ICsnRepositoryAsync<IntensidadeEmissaoExibicao> csnRepository, ISiderurgiaGraficoIntensidadeEmissaoExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            try
            {
                var entity = new IntensidadeEmissaoExibicao(request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            try
            {
                var entity = new IntensidadeEmissaoExibicao(request.Id, request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IntensidadeEmissaoExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<IntensidadeEmissaoExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<IntensidadeEmissaoExibicao>>> ObterDadosAsync(ObterDadosGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<IntensidadeEmissaoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
