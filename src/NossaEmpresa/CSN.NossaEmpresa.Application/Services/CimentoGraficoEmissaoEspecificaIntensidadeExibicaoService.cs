﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class CimentoGraficoEmissaoEspecificaIntensidadeExibicaoService : ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService
    {
        private readonly ICsnRepositoryAsync<CimentoEmissaoEspecificaIntensidadeExibicao> _csnRepository;
        private readonly ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoRepository _repository;

        public CimentoGraficoEmissaoEspecificaIntensidadeExibicaoService(ICsnRepositoryAsync<CimentoEmissaoEspecificaIntensidadeExibicao> csnRepository, ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new CimentoEmissaoEspecificaIntensidadeExibicao(request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new CimentoEmissaoEspecificaIntensidadeExibicao(request.Id, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<CimentoEmissaoEspecificaIntensidadeExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<CimentoEmissaoEspecificaIntensidadeExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<CimentoEmissaoEspecificaIntensidadeExibicao>>> ObterDadosAsync(ObterDadosCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<CimentoEmissaoEspecificaIntensidadeExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
