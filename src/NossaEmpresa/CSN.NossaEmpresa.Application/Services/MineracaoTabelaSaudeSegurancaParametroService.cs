﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoTabelaSaudeSegurancaParametroService : IMineracaoTabelaSaudeSegurancaParametroService
    {
        private readonly IRepositoryAsync<MineracaoSaudeSegurancaParametro> _repository;

        public MineracaoTabelaSaudeSegurancaParametroService(IRepositoryAsync<MineracaoSaudeSegurancaParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaParametro(request.Titulo, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new MineracaoSaudeSegurancaParametro(request.Id, request.Titulo, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<MineracaoSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<MineracaoSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<MineracaoSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<MineracaoSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<MineracaoSaudeSegurancaParametro>>> ObterListaAsync(ObterListaMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<MineracaoSaudeSegurancaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<MineracaoSaudeSegurancaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteMineracaoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
