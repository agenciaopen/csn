﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class CimentoTabelaSaudeSegurancaExibicaoService : ICimentoTabelaSaudeSegurancaExibicaoService
    {
        private readonly ICsnRepositoryAsync<CimentoSaudeSegurancaExibicao> _csnRepository;
        private readonly ICimentoTabelaSaudeSegurancaExibicaoRepository _repository;

        public CimentoTabelaSaudeSegurancaExibicaoService(ICsnRepositoryAsync<CimentoSaudeSegurancaExibicao> csnRepository, ICimentoTabelaSaudeSegurancaExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarCimentoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new CimentoSaudeSegurancaExibicao(request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarCimentoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new CimentoSaudeSegurancaExibicao(request.Id, request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma, request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirCimentoTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<CimentoSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosCimentoTabelaSaudeSegurancaExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<CimentoSaudeSegurancaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoCimentoTabelaSaudeSegurancaExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
