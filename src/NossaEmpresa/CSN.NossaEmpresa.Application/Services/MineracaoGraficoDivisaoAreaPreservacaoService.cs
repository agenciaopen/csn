﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoGraficoDivisaoAreaPreservacaoService : IMineracaoGraficoDivisaoAreaPreservacaoService
    {
        private readonly ICsnRepositoryAsync<DivisaoAreaPreservacao> _repository;

        public MineracaoGraficoDivisaoAreaPreservacaoService(ICsnRepositoryAsync<DivisaoAreaPreservacao> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoDivisaoAreaPreservacaoRequest request)
        {
            try
            {
                var entity = new DivisaoAreaPreservacao(request.Nome, request.AreaReabilitacao, request.AreaIntervinda, request.AreaConservacao, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoDivisaoAreaPreservacaoRequest request)
        {
            try
            {
                var entity = new DivisaoAreaPreservacao(request.Id, request.Nome, request.AreaReabilitacao, request.AreaIntervinda, request.AreaConservacao, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoDivisaoAreaPreservacaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<DivisaoAreaPreservacao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<DivisaoAreaPreservacao>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request)
        {
            return await _repository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
