﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class LogisticaTabelaSaudeSegurancaExibicaoService : ILogisticaTabelaSaudeSegurancaExibicaoService
    {
        private readonly ICsnRepositoryAsync<LogisticaSaudeSegurancaExibicao> _csnRepository;
        private readonly ILogisticaTabelaSaudeSegurancaExibicaoRepository _repository;

        public LogisticaTabelaSaudeSegurancaExibicaoService(ICsnRepositoryAsync<LogisticaSaudeSegurancaExibicao> csnRepository, ILogisticaTabelaSaudeSegurancaExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarLogisticaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new LogisticaSaudeSegurancaExibicao(request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarLogisticaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new LogisticaSaudeSegurancaExibicao(request.Id, request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma, request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirLogisticaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<LogisticaSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosLogisticaTabelaSaudeSegurancaExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<LogisticaSaudeSegurancaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoLogisticaTabelaSaudeSegurancaExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
