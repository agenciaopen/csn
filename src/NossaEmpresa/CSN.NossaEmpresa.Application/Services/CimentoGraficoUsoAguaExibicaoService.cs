﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class CimentoGraficoUsoAguaExibicaoService : ICimentoGraficoUsoAguaExibicaoService
    {
        private readonly ICsnRepositoryAsync<UsoAguaExibicao> _csnRepository;
        private readonly ICimentoGraficoUsoAguaExibicaoRepository _usoAguaExibicaoRepository;
        public CimentoGraficoUsoAguaExibicaoService(ICsnRepositoryAsync<UsoAguaExibicao> csnRepository, ICimentoGraficoUsoAguaExibicaoRepository usoAguaExibicaoRepository)
        {
            _csnRepository = csnRepository;
            _usoAguaExibicaoRepository = usoAguaExibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoUsoAguaExibicaoRequest request)
        {
            try
            {
                var entity = new UsoAguaExibicao(request.Ano, request.CaptacaoAgua, request.TotalAguaDescartada, request.ConsumoAgua, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoUsoAguaExibicaoRequest request)
        {
            try
            {
                var entity = new UsoAguaExibicao(request.Id, request.Ano, request.CaptacaoAgua, request.TotalAguaDescartada, request.ConsumoAgua, request.Idioma, request.Ativo);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoUsoAguaExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<UsoAguaExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoUsoAguaExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<UsoAguaExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<UsoAguaExibicao>>> ObterDadosAsync(ObterDadosGraficoUsoAguaExibicaoRequest request)
        {
            var entity = await _usoAguaExibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<UsoAguaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoUsoAguaExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
