﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class SiderurgiaGraficoPercentualMulherUpvExibicaoService : ISiderurgiaGraficoPercentualMulherUpvExibicaoService
    {
        private readonly ICsnRepositoryAsync<PercentualMulherUpvExibicao> _csnRepository;
        private readonly ISiderurgiaGraficoPercentualMulherUpvExibicaoRepository _repository;

        public SiderurgiaGraficoPercentualMulherUpvExibicaoService(ICsnRepositoryAsync<PercentualMulherUpvExibicao> csnRepository, ISiderurgiaGraficoPercentualMulherUpvExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherUpvExibicaoRequest request)
        {
            try
            {
                var entity = new PercentualMulherUpvExibicao(request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherUpvExibicaoRequest request)
        {
            try
            {
                var entity = new PercentualMulherUpvExibicao(request.Id, request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherUpvExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<PercentualMulherUpvExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<PercentualMulherUpvExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<PercentualMulherUpvExibicao>>> ObterDadosAsync(ObterDadosGraficoPercentualMulherUpvExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<PercentualMulherUpvExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
