﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;

namespace CSN.NossaEmpresa.Application.Services
{
    public class LogisticaTabelaSaudeSegurancaParametroService : ILogisticaTabelaSaudeSegurancaParametroService
    {
        private readonly IRepositoryAsync<LogisticaSaudeSegurancaParametro> _repository;

        public LogisticaTabelaSaudeSegurancaParametroService(IRepositoryAsync<LogisticaSaudeSegurancaParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new LogisticaSaudeSegurancaParametro(request.Titulo, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new LogisticaSaudeSegurancaParametro(request.Id, request.Titulo, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<LogisticaSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<LogisticaSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<LogisticaSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<LogisticaSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<LogisticaSaudeSegurancaParametro>>> ObterListaAsync(ObterListaLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<LogisticaSaudeSegurancaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<LogisticaSaudeSegurancaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteLogisticaTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
