﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class SiderurgiaGraficoPercentualMulherUpvParametroService : ISiderurgiaGraficoPercentualMulherUpvParametroService
    {
        private readonly IRepositoryAsync<PercentualMulherUpvParametro> _repository;

        public SiderurgiaGraficoPercentualMulherUpvParametroService(IRepositoryAsync<PercentualMulherUpvParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherUpvParametroRequest request)
        {
            try
            {
                var entity = new PercentualMulherUpvParametro(request.Nome, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherUpvParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherUpvParametroRequest request)
        {
            try
            {
                var entity = new PercentualMulherUpvParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<PercentualMulherUpvParametro>> ObterPorIdAsync(ObterPorIdGraficoPercentualMulherUpvParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<PercentualMulherUpvParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<PercentualMulherUpvParametro>> ObterPorSlugAsync(ObterPorSlugGraficoPercentualMulherUpvParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<PercentualMulherUpvParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<PercentualMulherUpvParametro>>> ObterListaAsync(ObterListaGraficoPercentualMulherUpvParametroRequest request)
        {
            var predicate = PredicateBuilder.True<PercentualMulherUpvParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<PercentualMulherUpvParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteGraficoPercentualMulherUpvParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }

}
