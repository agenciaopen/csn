﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class MineracaoGraficoEmissaoEspecificaIntensidadeParametroService : IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService
    {
        private readonly IRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeParametro> _repository;

        public MineracaoGraficoEmissaoEspecificaIntensidadeParametroService(IRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            try
            {
                var entity = new MineracaoEmissaoEspecificaIntensidadeParametro(request.Titulo, request.Sigla, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            try
            {
                var entity = new MineracaoEmissaoEspecificaIntensidadeParametro(request.Id, request.Titulo, request.Sigla, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>> ObterPorIdAsync(ObterPorIdMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>> ObterPorSlugAsync(ObterPorSlugMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<MineracaoEmissaoEspecificaIntensidadeParametro>>> ObterListaAsync(ObterListaMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            var predicate = PredicateBuilder.True<MineracaoEmissaoEspecificaIntensidadeParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<MineracaoEmissaoEspecificaIntensidadeParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
