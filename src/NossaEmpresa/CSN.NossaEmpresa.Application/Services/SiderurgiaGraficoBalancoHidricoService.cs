﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Services
{
    public class SiderurgiaGraficoBalancoHidricoService : ISiderurgiaGraficoBalancoHidricoService
    {
        private readonly ICsnRepositoryAsync<SiderurgiaBalancoHidrico> _csnRepository;

        public SiderurgiaGraficoBalancoHidricoService(ICsnRepositoryAsync<SiderurgiaBalancoHidrico> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarSiderurgiaGraficoBalancoHidricoRequest request)
        {
            try
            {
                var entity = new SiderurgiaBalancoHidrico(request.Titulo, request.Sigla, request.Captacao, request.Recirculacao, request.Retorno, request.Consumo, request.Total, request.Idioma, true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarSiderurgiaGraficoBalancoHidricoRequest request)
        {
            try
            {
                var entity = new SiderurgiaBalancoHidrico(request.Id, request.Titulo, request.Sigla, request.Captacao, request.Recirculacao, request.Retorno, request.Consumo, request.Total, request.Idioma, request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirSiderurgiaGraficoBalancoHidricoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<SiderurgiaBalancoHidrico?> ObterUltimoAtivoAsync(ObterUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request)
        {
            return await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
