﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;

namespace CSN.NossaEmpresa.Application.Services
{
    public class SiderurgiaTabelaSaudeSegurancaExibicaoService : ISiderurgiaTabelaSaudeSegurancaExibicaoService
    {
        private readonly ICsnRepositoryAsync<SiderurgiaSaudeSegurancaExibicao> _csnRepository;
        private readonly ISiderurgiaTabelaSaudeSegurancaExibicaoRepository _repository;

        public SiderurgiaTabelaSaudeSegurancaExibicaoService(ICsnRepositoryAsync<SiderurgiaSaudeSegurancaExibicao> csnRepository, ISiderurgiaTabelaSaudeSegurancaExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new SiderurgiaSaudeSegurancaExibicao(request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var entity = new SiderurgiaSaudeSegurancaExibicao(request.Id, request.Unidade, request.Grupo, request.Ano, request.Taxa, request.Idioma, request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirSiderurgiaTabelaSaudeSegurancaExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<SiderurgiaSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosSiderurgiaTabelaSaudeSegurancaExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<SiderurgiaSaudeSegurancaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoSiderurgiaTabelaSaudeSegurancaExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
