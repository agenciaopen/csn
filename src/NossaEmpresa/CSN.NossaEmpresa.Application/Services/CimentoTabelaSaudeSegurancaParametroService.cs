﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;

namespace CSN.NossaEmpresa.Application.Services
{
    public class CimentoTabelaSaudeSegurancaParametroService : ICimentoTabelaSaudeSegurancaParametroService
    {
        private readonly IRepositoryAsync<CimentoSaudeSegurancaParametro> _repository;

        public CimentoTabelaSaudeSegurancaParametroService(IRepositoryAsync<CimentoSaudeSegurancaParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new CimentoSaudeSegurancaParametro(request.Nome, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            try
            {
                var entity = new CimentoSaudeSegurancaParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<CimentoSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<CimentoSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<CimentoSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<CimentoSaudeSegurancaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<CimentoSaudeSegurancaParametro>>> ObterListaAsync(ObterListaCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<CimentoSaudeSegurancaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<CimentoSaudeSegurancaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteCimentoTabelaSaudeSegurancaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
