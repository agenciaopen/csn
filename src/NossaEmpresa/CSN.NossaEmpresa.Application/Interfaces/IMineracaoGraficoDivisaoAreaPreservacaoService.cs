﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoGraficoDivisaoAreaPreservacaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoDivisaoAreaPreservacaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoDivisaoAreaPreservacaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoDivisaoAreaPreservacaoRequest request);
        Task<ResponseResult<DivisaoAreaPreservacao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoDivisaoAreaPreservacaoRequest request);
    }
}
