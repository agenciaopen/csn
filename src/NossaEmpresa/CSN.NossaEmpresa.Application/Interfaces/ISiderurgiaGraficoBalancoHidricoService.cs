﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaGraficoBalancoHidricoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarSiderurgiaGraficoBalancoHidricoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarSiderurgiaGraficoBalancoHidricoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirSiderurgiaGraficoBalancoHidricoRequest request);
        Task<SiderurgiaBalancoHidrico?> ObterUltimoAtivoAsync(ObterUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest request);
    }
}
