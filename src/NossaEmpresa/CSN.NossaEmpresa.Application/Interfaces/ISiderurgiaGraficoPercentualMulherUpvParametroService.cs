﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaGraficoPercentualMulherUpvParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherUpvParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherUpvParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherUpvParametroRequest request);
        Task<ResponseResult<PercentualMulherUpvParametro>> ObterPorIdAsync(ObterPorIdGraficoPercentualMulherUpvParametroRequest request);
        Task<ResponseResult<PercentualMulherUpvParametro>> ObterPorSlugAsync(ObterPorSlugGraficoPercentualMulherUpvParametroRequest request);
        Task<ResponseResult<List<PercentualMulherUpvParametro>>> ObterListaAsync(ObterListaGraficoPercentualMulherUpvParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoPercentualMulherUpvParametroRequest request);
    }
}
