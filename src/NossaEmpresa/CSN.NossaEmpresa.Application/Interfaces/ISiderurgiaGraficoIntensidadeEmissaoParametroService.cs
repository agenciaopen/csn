﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaGraficoIntensidadeEmissaoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoIntensidadeEmissaoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoIntensidadeEmissaoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoIntensidadeEmissaoParametroRequest request);
        Task<ResponseResult<IntensidadeEmissaoParametro>> ObterPorIdAsync(ObterPorIdGraficoIntensidadeEmissaoParametroRequest request);
        Task<ResponseResult<IntensidadeEmissaoParametro>> ObterPorSlugAsync(ObterPorSlugGraficoIntensidadeEmissaoParametroRequest request);
        Task<ResponseResult<List<IntensidadeEmissaoParametro>>> ObterListaAsync(ObterListaGraficoIntensidadeEmissaoParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoIntensidadeEmissaoParametroRequest request);
    }
}
