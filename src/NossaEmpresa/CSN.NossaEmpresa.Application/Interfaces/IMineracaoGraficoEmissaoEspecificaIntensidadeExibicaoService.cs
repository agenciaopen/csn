﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult<IEnumerable<MineracaoEmissaoEspecificaIntensidadeExibicao>>> ObterDadosAsync(
            ObterDadosMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
    }
}
