﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ILogisticaTabelaSaudeSegurancaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<LogisticaSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<LogisticaSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<List<LogisticaSaudeSegurancaParametro>>> ObterListaAsync(ObterListaLogisticaTabelaSaudeSegurancaParametroRequest request);
        Task<bool> ExisteAsync(ExisteLogisticaTabelaSaudeSegurancaParametroRequest request);
    }
}
