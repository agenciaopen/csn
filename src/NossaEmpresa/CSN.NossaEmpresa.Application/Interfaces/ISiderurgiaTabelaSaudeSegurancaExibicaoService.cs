﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaTabelaSaudeSegurancaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarSiderurgiaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirSiderurgiaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<SiderurgiaSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosSiderurgiaTabelaSaudeSegurancaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoSiderurgiaTabelaSaudeSegurancaExibicaoRequest request);
    }
}
