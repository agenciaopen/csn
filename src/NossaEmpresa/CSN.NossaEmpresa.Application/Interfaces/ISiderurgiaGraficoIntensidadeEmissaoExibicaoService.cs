﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaGraficoIntensidadeEmissaoExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoIntensidadeEmissaoExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoIntensidadeEmissaoExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoIntensidadeEmissaoExibicaoRequest request);
        Task<ResponseResult<IntensidadeEmissaoExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<IntensidadeEmissaoExibicao>>> ObterDadosAsync(
            ObterDadosGraficoIntensidadeEmissaoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest request);
    }
}
