﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoGraficoEmissaoEspecificaIntensidadeParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<CimentoEmissaoEspecificaIntensidadeParametro>> ObterPorIdAsync(ObterPorIdCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<CimentoEmissaoEspecificaIntensidadeParametro>> ObterPorSlugAsync(ObterPorSlugCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<List<CimentoEmissaoEspecificaIntensidadeParametro>>> ObterListaAsync(ObterListaCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<bool> ExisteAsync(ExisteCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
    }
}
