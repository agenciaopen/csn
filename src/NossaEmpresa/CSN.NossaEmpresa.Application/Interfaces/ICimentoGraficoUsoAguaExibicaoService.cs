﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoGraficoUsoAguaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoUsoAguaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoUsoAguaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoUsoAguaExibicaoRequest request);
        Task<ResponseResult<UsoAguaExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoUsoAguaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<UsoAguaExibicao>>> ObterDadosAsync(
            ObterDadosGraficoUsoAguaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoUsoAguaExibicaoRequest request);
    }
}
