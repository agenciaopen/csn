﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoTabelaSaudeSegurancaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<MineracaoSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<MineracaoSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<List<MineracaoSaudeSegurancaParametro>>> ObterListaAsync(ObterListaMineracaoTabelaSaudeSegurancaParametroRequest request);
        Task<bool> ExisteAsync(ExisteMineracaoTabelaSaudeSegurancaParametroRequest request);
    }
}
