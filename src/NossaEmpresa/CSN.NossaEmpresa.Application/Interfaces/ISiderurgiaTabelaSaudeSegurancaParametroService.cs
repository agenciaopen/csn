﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaTabelaSaudeSegurancaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<SiderurgiaSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<SiderurgiaSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<List<SiderurgiaSaudeSegurancaParametro>>> ObterListaAsync(ObterListaSiderurgiaTabelaSaudeSegurancaParametroRequest request);
        Task<bool> ExisteAsync(ExisteSiderurgiaTabelaSaudeSegurancaParametroRequest request);
    }
}
