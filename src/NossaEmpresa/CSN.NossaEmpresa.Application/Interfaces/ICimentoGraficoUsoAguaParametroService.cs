﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoGraficoUsoAguaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoUsoAguaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoUsoAguaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoUsoAguaParametroRequest request);
        Task<ResponseResult<UsoAguaParametro>> ObterPorIdAsync(ObterPorIdGraficoUsoAguaParametroRequest request);
        Task<ResponseResult<UsoAguaParametro>> ObterPorSlugAsync(ObterPorSlugGraficoUsoAguaParametroRequest request);
        Task<ResponseResult<List<UsoAguaParametro>>> ObterListaAsync(ObterListaGraficoUsoAguaParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoUsoAguaParametroRequest request);
    }
}
