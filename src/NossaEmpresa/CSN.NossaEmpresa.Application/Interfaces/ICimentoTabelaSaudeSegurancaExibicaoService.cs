﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoTabelaSaudeSegurancaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarCimentoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarCimentoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirCimentoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<CimentoSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosCimentoTabelaSaudeSegurancaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoCimentoTabelaSaudeSegurancaExibicaoRequest request);
    }
}
