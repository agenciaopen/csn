﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoTabelaSaudeSegurancaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<CimentoSaudeSegurancaParametro>> ObterPorIdAsync(ObterPorIdCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<CimentoSaudeSegurancaParametro>> ObterPorSlugAsync(ObterPorSlugCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<ResponseResult<List<CimentoSaudeSegurancaParametro>>> ObterListaAsync(ObterListaCimentoTabelaSaudeSegurancaParametroRequest request);
        Task<bool> ExisteAsync(ExisteCimentoTabelaSaudeSegurancaParametroRequest request);
    }
}
