﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoGraficoBalancoHidricoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoBalancoHidricoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoBalancoHidricoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoBalancoHidricoRequest request);
        Task<MineracaoBalancoHidrico?> ObterUltimoAtivoAsync(ObterUltimoAtivoMineracaoGraficoBalancoHidricoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoMineracaoGraficoBalancoHidricoRequest request);
    }
}
