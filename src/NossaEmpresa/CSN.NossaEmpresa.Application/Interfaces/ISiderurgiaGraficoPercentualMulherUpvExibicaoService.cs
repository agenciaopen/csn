﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Siderurgia;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ISiderurgiaGraficoPercentualMulherUpvExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherUpvExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherUpvExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherUpvExibicaoRequest request);
        Task<ResponseResult<PercentualMulherUpvExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest request);
        Task<ResponseResult<IEnumerable<PercentualMulherUpvExibicao>>> ObterDadosAsync(
            ObterDadosGraficoPercentualMulherUpvExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest request);
    }
}
