﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Cimento;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult<CimentoEmissaoEspecificaIntensidadeExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<ResponseResult<IEnumerable<CimentoEmissaoEspecificaIntensidadeExibicao>>> ObterDadosAsync(
            ObterDadosCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest request);
    }
}
