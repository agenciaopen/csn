﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ILogisticaGraficoEmissaoAtmosfericaService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoEmissaoAtmosfericaRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoEmissaoAtmosfericaRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoEmissaoAtmosfericaRequest request);
        Task<ResponseResult<EmissaoAtmosferica>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoEmissaoAtmosfericaRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEmissaoAtmosfericaRequest request);
    }
}
