﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Logistica;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface ILogisticaTabelaSaudeSegurancaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarLogisticaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarLogisticaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirLogisticaTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<LogisticaSaudeSegurancaExibicao>>> ObterDadosAsync(ObterDadosLogisticaTabelaSaudeSegurancaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteAtivoLogisticaTabelaSaudeSegurancaExibicaoRequest request);
    }
}
