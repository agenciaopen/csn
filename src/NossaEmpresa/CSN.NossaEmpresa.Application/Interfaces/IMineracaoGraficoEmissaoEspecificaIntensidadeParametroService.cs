﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>> ObterPorIdAsync(ObterPorIdMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<MineracaoEmissaoEspecificaIntensidadeParametro>> ObterPorSlugAsync(ObterPorSlugMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<ResponseResult<List<MineracaoEmissaoEspecificaIntensidadeParametro>>> ObterListaAsync(ObterListaMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
        Task<bool> ExisteAsync(ExisteMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest request);
    }
}
