﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.NossaEmpresa.Application.Requests.Mineracao;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;

namespace CSN.NossaEmpresa.Application.Interfaces
{
    public interface IMineracaoTabelaSaudeSegurancaExibicaoService
    {
        Task<ResponseResult> AdicionarDescricaoAsync(AdicionarMineracaoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> AtualizarDescricaoAsync(AtualizarMineracaoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult> ExcluirDescricaoAsync(ExcluirMineracaoTabelaSaudeSegurancaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicao>>> ObterDadosDescricaoAsync(ObterDadosMineracaoTabelaSaudeSegurancaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoMineracaoTabelaSaudeSegurancaExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<MineracaoSaudeSegurancaExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
        Task<ResponseResult<MineracaoSaudeSegurancaExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest request);
    }
}
