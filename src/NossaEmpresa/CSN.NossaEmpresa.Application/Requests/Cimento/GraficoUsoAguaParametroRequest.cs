﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarGraficoUsoAguaParametroRequest
    { 
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoUsoAguaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoUsoAguaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoUsoAguaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoUsoAguaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoUsoAguaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoUsoAguaParametroRequest
    {
        public string Slug { get; set; }
    }
}
