﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarCimentoTabelaSaudeSegurancaExibicaoRequest
    {
        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarCimentoTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirCimentoTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosCimentoTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteAtivoCimentoTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
