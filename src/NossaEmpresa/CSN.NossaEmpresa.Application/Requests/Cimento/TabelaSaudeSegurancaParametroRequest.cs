﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarCimentoTabelaSaudeSegurancaParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarCimentoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirCimentoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdCimentoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugCimentoTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaCimentoTabelaSaudeSegurancaParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteCimentoTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }
}
