﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarGraficoUsoAguaExibicaoRequest
    {
        public int Ano { get; set; }
        public double CaptacaoAgua { get; set; }
        public double TotalAguaDescartada { get; set; }
        public double ConsumoAgua { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoUsoAguaExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double CaptacaoAgua { get; set; }
        public double TotalAguaDescartada { get; set; }
        public double ConsumoAgua { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoUsoAguaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoUsoAguaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoUsoAguaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoUsoAguaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
