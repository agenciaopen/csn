﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteCimentoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }
}
