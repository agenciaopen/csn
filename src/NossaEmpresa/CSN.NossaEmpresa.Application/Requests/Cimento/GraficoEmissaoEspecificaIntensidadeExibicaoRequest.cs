﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Cimento
{
    public class AdicionarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoCimentoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
