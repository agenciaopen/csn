﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarMineracaoGraficoBalancoHidricoRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoGraficoBalancoHidricoRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoGraficoBalancoHidricoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoMineracaoGraficoBalancoHidricoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoMineracaoGraficoBalancoHidricoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
