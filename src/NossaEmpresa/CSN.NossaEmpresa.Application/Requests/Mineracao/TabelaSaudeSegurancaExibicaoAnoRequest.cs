﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class MineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class AdicionarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorDescricaoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
    }

    public class ExisteAnoAtivoMineracaoTabelaSaudeSegurancaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
