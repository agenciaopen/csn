﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteMineracaoGraficoEmissaoEspecificaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }

}
