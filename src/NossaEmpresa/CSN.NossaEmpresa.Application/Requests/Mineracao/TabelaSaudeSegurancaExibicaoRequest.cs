﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarMineracaoTabelaSaudeSegurancaExibicaoRequest
    {
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosMineracaoTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoMineracaoTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
