﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarGraficoDivisaoAreaPreservacaoRequest
    {
        public string Nome { get; set; }
        public double AreaReabilitacao { get; set; }
        public double AreaIntervinda { get; set; }
        public double AreaConservacao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoDivisaoAreaPreservacaoRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public double AreaReabilitacao { get; set; }
        public double AreaIntervinda { get; set; }
        public double AreaConservacao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoDivisaoAreaPreservacaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoDivisaoAreaPreservacaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoDivisaoAreaPreservacaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoDivisaoAreaPreservacaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}
