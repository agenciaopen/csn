﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public int Ano { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
