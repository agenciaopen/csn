﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Mineracao
{
    public class AdicionarMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public string Titulo { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteMineracaoTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }

}
