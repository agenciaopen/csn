﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Logistica
{
    public class AdicionarLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteLogisticaTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }
}
