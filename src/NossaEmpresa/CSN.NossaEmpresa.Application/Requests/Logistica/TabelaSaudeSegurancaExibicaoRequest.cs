﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Logistica
{
    public class AdicionarLogisticaTabelaSaudeSegurancaExibicaoRequest
    {
        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarLogisticaTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Unidade { get; set; }
        public string Grupo { get; set; }
        public int Ano { get; set; }
        public double Taxa { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirLogisticaTabelaSaudeSegurancaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosLogisticaTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteAtivoLogisticaTabelaSaudeSegurancaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
