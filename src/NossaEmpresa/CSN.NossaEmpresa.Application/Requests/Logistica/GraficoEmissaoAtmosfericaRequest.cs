﻿using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Logistica
{
    public class AdicionarGraficoEmissaoAtmosfericaRequest
    {
        public string Nome { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double EscopoTres { get; set; }
        public double Fugitivas { get; set; }
        public double Residuos { get; set; }
        public double CombustaoMovel { get; set; }
        public double CombustaoEstacionario { get; set; }
        public double ProcessoIndustrial { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoEmissaoAtmosfericaRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public double EscopoUm { get; set; }
        public double EscopoDois { get; set; }
        public double EscopoTres { get; set; }
        public double Fugitivas { get; set; }
        public double Residuos { get; set; }
        public double CombustaoMovel { get; set; }
        public double CombustaoEstacionario { get; set; }
        public double ProcessoIndustrial { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoEmissaoAtmosfericaRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoEmissaoAtmosfericaRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoEmissaoAtmosfericaRequest
    {
        public Idioma Idioma { get; set; }
    }

}
