﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    public class AdicionarGraficoIntensidadeEmissaoParametroRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoIntensidadeEmissaoParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoIntensidadeEmissaoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoIntensidadeEmissaoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoIntensidadeEmissaoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoIntensidadeEmissaoParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoIntensidadeEmissaoParametroRequest
    {
        public string Slug { get; set; }
    }

}
