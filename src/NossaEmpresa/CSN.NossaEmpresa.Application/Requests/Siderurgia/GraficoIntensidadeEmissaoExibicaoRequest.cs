﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    public class AdicionarGraficoIntensidadeEmissaoExibicaoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoIntensidadeEmissaoExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoIntensidadeEmissaoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoIntensidadeEmissaoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoIntensidadeEmissaoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
