﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    public class AdicionarGraficoPercentualMulherUpvExibicaoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoPercentualMulherUpvExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoPercentualMulherUpvExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoPercentualMulherUpvExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoPercentualMulherUpvExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}
