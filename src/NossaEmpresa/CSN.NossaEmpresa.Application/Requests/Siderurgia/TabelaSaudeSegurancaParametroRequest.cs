﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    public class AdicionarSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteSiderurgiaTabelaSaudeSegurancaParametroRequest
    {
        public string Slug { get; set; }
    }
}
