﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    internal class GraficoPercentualMulherUpvParametroRequest
    {
    }
    public class AdicionarGraficoPercentualMulherUpvParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoPercentualMulherUpvParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoPercentualMulherUpvParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoPercentualMulherUpvParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoPercentualMulherUpvParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoPercentualMulherUpvParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoPercentualMulherUpvParametroRequest
    {
        public string Slug { get; set; }
    }

}
