﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.NossaEmpresa.Application.Requests.Siderurgia
{
    public class AdicionarSiderurgiaGraficoBalancoHidricoRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarSiderurgiaGraficoBalancoHidricoRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Captacao { get; set; }
        public double Recirculacao { get; set; }
        public double Retorno { get; set; }
        public double Consumo { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirSiderurgiaGraficoBalancoHidricoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoSiderurgiaGraficoBalancoHidricoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
