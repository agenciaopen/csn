﻿using System.ComponentModel.DataAnnotations;
using CSN.Core;
using CSN.Core.Domain.Interfaces;
using CSN.NossaEmpresa.Application.Interfaces;
using CSN.NossaEmpresa.Application.Services;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Cimento.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Logistica.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Mineracao.Tabela;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Grafico;
using CSN.NossaEmpresa.Domain.Entities.Siderurgia.Tabela;
using CSN.NossaEmpresa.Domain.Interfaces;
using CSN.NossaEmpresa.Infrastructure.Data.Context;
using CSN.NossaEmpresa.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CSN.NossaEmpresa.Infrastructure.IoC
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection AddNossaEmpresaContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<NossaEmpresaDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("CSN")));

            return services;
        }

        public static IServiceCollection AddNossaEmpresaServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<NossaEmpresaDbContext>();

            services.AddScoped<IRepositoryAsync<UsoAguaParametro>, BaseRepositoryParametersAsync<UsoAguaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<UsoAguaExibicao>, BaseRepositoryAsync<UsoAguaExibicao>>();
            services.AddScoped<ICimentoGraficoUsoAguaExibicaoRepository, CimentoGraficoUsoAguaExibicaoRepository>();
            services.AddScoped<IRepositoryAsync<CimentoSaudeSegurancaParametro>, BaseRepositoryParametersAsync<CimentoSaudeSegurancaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<CimentoSaudeSegurancaExibicao>, BaseRepositoryAsync<CimentoSaudeSegurancaExibicao>>();
            services.AddScoped<ICimentoTabelaSaudeSegurancaExibicaoRepository, CimentoTabelaSaudeSegurancaExibicaoRepository>();
            services.AddScoped<IRepositoryAsync<CimentoEmissaoEspecificaIntensidadeParametro>, BaseRepositoryParametersAsync<CimentoEmissaoEspecificaIntensidadeParametro>>();
            services.AddScoped<ICsnRepositoryAsync<CimentoEmissaoEspecificaIntensidadeExibicao>, BaseRepositoryAsync<CimentoEmissaoEspecificaIntensidadeExibicao>>();
            services.AddScoped<ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoRepository, CimentoGraficoEmissaoEspecificaIntensidadeExibicaoRepository>();

            services.AddScoped<ICsnRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeParametro>, BaseRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeParametro>>();
            services.AddScoped<IRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeParametro>, BaseRepositoryParametersAsync<MineracaoEmissaoEspecificaIntensidadeParametro>>();

            services.AddScoped<ICsnRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeExibicao>, BaseRepositoryAsync<MineracaoEmissaoEspecificaIntensidadeExibicao>>();
            services.AddScoped<IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRepository, MineracaoGraficoEmissaoEspecificaIntensidadeExibicaoRepository>();

            services.AddScoped<IRepositoryAsync<MineracaoSaudeSegurancaParametro>, BaseRepositoryParametersAsync<MineracaoSaudeSegurancaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicao>, BaseRepositoryAsync<MineracaoSaudeSegurancaExibicao>>();
            services.AddScoped<IMineracaoTabelaSaudeSegurancaExibicaoRepository, MineracaoTabelaSaudeSegurancaExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<MineracaoSaudeSegurancaExibicaoAno>, BaseRepositoryAsync<MineracaoSaudeSegurancaExibicaoAno>>();
            services.AddScoped<ICsnRepositoryAsync<DivisaoAreaPreservacao>, BaseRepositoryAsync<DivisaoAreaPreservacao>>();
            services.AddScoped<ICsnRepositoryAsync<MineracaoBalancoHidrico>, BaseRepositoryAsync<MineracaoBalancoHidrico>>();

            services.AddScoped<ICsnRepositoryAsync<SiderurgiaBalancoHidrico>, BaseRepositoryAsync<SiderurgiaBalancoHidrico>>();
            
            services.AddScoped<ICsnRepositoryAsync<IntensidadeEmissaoParametro>, BaseRepositoryAsync<IntensidadeEmissaoParametro>>();
            services.AddScoped<IRepositoryAsync<IntensidadeEmissaoParametro>, BaseRepositoryParametersAsync<IntensidadeEmissaoParametro>>();

            services.AddScoped<IRepositoryAsync<PercentualMulherUpvParametro>, BaseRepositoryParametersAsync<PercentualMulherUpvParametro>>();
            services.AddScoped<ISiderurgiaGraficoPercentualMulherUpvExibicaoRepository, SiderurgiaGraficoPercentualMulherUpvExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<PercentualMulherUpvExibicao>, BaseRepositoryAsync<PercentualMulherUpvExibicao>>();

            services.AddScoped<ISiderurgiaGraficoIntensidadeEmissaoExibicaoRepository, SiderurgiaGraficoIntensidadeEmissaoExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<IntensidadeEmissaoExibicao>, BaseRepositoryAsync<IntensidadeEmissaoExibicao>>();

            services.AddScoped<IRepositoryAsync<SiderurgiaSaudeSegurancaParametro>, BaseRepositoryParametersAsync<SiderurgiaSaudeSegurancaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<SiderurgiaSaudeSegurancaExibicao>, BaseRepositoryAsync<SiderurgiaSaudeSegurancaExibicao>>();
            services.AddScoped<ISiderurgiaTabelaSaudeSegurancaExibicaoRepository, SiderurgiaTabelaSaudeSegurancaExibicaoRepository>();

            services.AddScoped<ICsnRepositoryAsync<EmissaoAtmosferica>, BaseRepositoryAsync<EmissaoAtmosferica>>();
            services.AddScoped<IRepositoryAsync<LogisticaSaudeSegurancaParametro>, BaseRepositoryParametersAsync<LogisticaSaudeSegurancaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<LogisticaSaudeSegurancaExibicao>, BaseRepositoryAsync<LogisticaSaudeSegurancaExibicao>>();
            services.AddScoped<ILogisticaTabelaSaudeSegurancaExibicaoRepository, LogisticaTabelaSaudeSegurancaExibicaoRepository>();

            services.AddScoped<ICimentoGraficoUsoAguaParametroService, CimentoGraficoUsoAguaParametroService>();
            services.AddScoped<ICimentoGraficoUsoAguaExibicaoService, CimentoGraficoUsoAguaExibicaoService>();
            services.AddScoped<ICimentoTabelaSaudeSegurancaParametroService, CimentoTabelaSaudeSegurancaParametroService>();
            services.AddScoped<ICimentoTabelaSaudeSegurancaExibicaoService, CimentoTabelaSaudeSegurancaExibicaoService>();
            services.AddScoped<ICimentoGraficoEmissaoEspecificaIntensidadeParametroService, CimentoGraficoEmissaoEspecificaIntensidadeParametroService>();
            services.AddScoped<ICimentoGraficoEmissaoEspecificaIntensidadeExibicaoService, CimentoGraficoEmissaoEspecificaIntensidadeExibicaoService>();
            
            services.AddScoped<IMineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService, MineracaoGraficoEmissaoEspecificaIntensidadeExibicaoService>();
            services.AddScoped<IMineracaoGraficoEmissaoEspecificaIntensidadeParametroService, MineracaoGraficoEmissaoEspecificaIntensidadeParametroService>();
            services.AddScoped<IMineracaoTabelaSaudeSegurancaParametroService, MineracaoTabelaSaudeSegurancaParametroService>();
            services.AddScoped<IMineracaoTabelaSaudeSegurancaExibicaoService, MineracaoTabelaSaudeSegurancaExibicaoService>();
            services.AddScoped<IMineracaoGraficoDivisaoAreaPreservacaoService, MineracaoGraficoDivisaoAreaPreservacaoService>();
            services.AddScoped<IMineracaoGraficoBalancoHidricoService, MineracaoGraficoBalancoHidricoService>();

            services.AddScoped<ISiderurgiaGraficoBalancoHidricoService, SiderurgiaGraficoBalancoHidricoService>();
            services.AddScoped<ISiderurgiaGraficoIntensidadeEmissaoExibicaoService, SiderurgiaGraficoIntensidadeEmissaoExibicaoService>();
            services.AddScoped<ISiderurgiaGraficoIntensidadeEmissaoParametroService, SiderurgiaGraficoIntensidadeEmissaoParametroService>();
            services.AddScoped<ISiderurgiaTabelaSaudeSegurancaParametroService, SiderurgiaTabelaSaudeSegurancaParametroService>();
            services.AddScoped<ISiderurgiaTabelaSaudeSegurancaExibicaoService, SiderurgiaTabelaSaudeSegurancaExibicaoService>();

            services.AddScoped<ISiderurgiaGraficoPercentualMulherUpvParametroService, SiderurgiaGraficoPercentualMulherUpvParametroService>();
            services.AddScoped<ISiderurgiaGraficoPercentualMulherUpvExibicaoService, SiderurgiaGraficoPercentualMulherUpvExibicaoService>();

            services.AddScoped<ILogisticaGraficoEmissaoAtmosfericaService, LogisticaGraficoEmissaoAtmosfericaService>();
            services.AddScoped<ILogisticaTabelaSaudeSegurancaParametroService, LogisticaTabelaSaudeSegurancaParametroService>();
            services.AddScoped<ILogisticaTabelaSaudeSegurancaExibicaoService, LogisticaTabelaSaudeSegurancaExibicaoService>();

            return services;
        }
    }
}