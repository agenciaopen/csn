﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace CSN.MeioAmbiente.Infrastructure.Data.Context
{
    public class MeioAmbienteDbContext : DbContext
    {
        public MeioAmbienteDbContext(DbContextOptions<MeioAmbienteDbContext> options)
        : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public async Task<bool> CommitAsync()
        {
            var sucesso = true;
            try
            {
                sucesso = await base.SaveChangesAsync() > 0;
            }
            catch (System.Exception ex)
            {
                sucesso = false;
                throw new System.Exception($"error:{ex.Message} >>>> {ex.InnerException?.Message} <<<<>>>>>{ex.InnerException?.InnerException?.Message} <<<<<");
            }

            return sucesso;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseSqlServer(config.GetConnectionString("CSN"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                         .SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(MeioAmbienteDbContext).Assembly);
        }

        public DbSet<TEntity> DbSet<TEntity>() where TEntity : class
        {
            return Set<TEntity>();
        }
    }

    public class DesignTimeApplicationDbContext : IDesignTimeDbContextFactory<MeioAmbienteDbContext>
    {
        public MeioAmbienteDbContext CreateDbContext(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{envName}.json", true)
                .AddEnvironmentVariables()
                .Build();

            var optionsBuilder = new DbContextOptionsBuilder<MeioAmbienteDbContext>();
            // pass your design time connection string here
            optionsBuilder.UseSqlServer(config.GetConnectionString("CSN"));
            return new MeioAmbienteDbContext(optionsBuilder.Options);
        }
    }
}
