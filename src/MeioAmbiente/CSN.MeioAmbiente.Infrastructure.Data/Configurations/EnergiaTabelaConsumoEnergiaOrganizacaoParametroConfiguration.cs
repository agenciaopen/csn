﻿using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoParametroConfiguration : IEntityTypeConfiguration<ConsumoEnergiaOrganizacaoParametro>
    {
        public void Configure(EntityTypeBuilder<ConsumoEnergiaOrganizacaoParametro> builder)
        {
            builder.ToTable("EnergiaTabelaConsumoEnergiaOrganizacao", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.AreaNegocio)
                .IsRequired()
                .HasColumnType("varchar(500)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
