﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class AguaGraficoDescarteConfiguration : IEntityTypeConfiguration<Descarte>
    {
        public void Configure(EntityTypeBuilder<Descarte> builder)
        {
            builder.ToTable("AguaGraficoDescarte", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Subtitulo)
                .IsRequired()
                .HasColumnType("varchar(15)");


            builder.Property(c => c.Mineracao)
                .IsRequired();

            builder.Property(c => c.Siderurgia)
                .IsRequired();

            builder.Property(c => c.Cimento)
                .IsRequired();

            builder.Property(c => c.Logistica)
                .IsRequired();

            builder.Property(c => c.OutrasMineracoes)
                .IsRequired();

            builder.Property(c => c.InformacaoUm)
                .IsRequired()
                .HasColumnType("varchar(55)");

            builder.Property(c => c.InformacaoDois)
                .IsRequired()
                .HasColumnType("varchar(55)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
