﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class QualidadeArTabelaEmissaoAtmosfericaParametroConfiguration : IEntityTypeConfiguration<EmissaoAtmosfericaParametro>
    {
        public void Configure(EntityTypeBuilder<EmissaoAtmosfericaParametro> builder)
        {
            builder.ToTable("QualidadeArTabelaEmissaoAtmosferica", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(c => c.Subtitulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Nota)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
