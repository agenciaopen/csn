﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class QualidadeArTabelaEmissaoAtmosfericaCategoriaConfiguration : IEntityTypeConfiguration<EmissaoAtmosfericaCategoria>
    {
        public void Configure(EntityTypeBuilder<EmissaoAtmosfericaCategoria> builder)
        {
            builder.ToTable("QualidadeArTabelaEmissaoAtmosfericaCategoria", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasMany(x => x.FontesEmissoras).WithOne(x => x.Categoria);
            builder.HasMany(x => x.Anos).WithOne(x => x.Categoria);
        }
    }
}
