﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class AguaTabelaAguaEfluenteExibicaoAnoConfiguration : IEntityTypeConfiguration<AguaEfluenteExibicaoAno>
    {
        public void Configure(EntityTypeBuilder<AguaEfluenteExibicaoAno> builder)
        {
            builder.ToTable("AguaTabelaAguaEfluenteAno", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.TodasAreas)
                .IsRequired();

            builder.Property(c => c.AreasComEstresseHidrico)
                .IsRequired();

            builder.Property(c => c.FonteCaptacaoId)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasOne(x => x.FonteCaptacao).WithMany(x => x.Anos);
        }
    }
}
