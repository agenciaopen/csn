﻿using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class ResiduosGraficoResiduosPerigososNaoConfiguration : IEntityTypeConfiguration<ResiduosNaoPerigosos>
    {
        public void Configure(EntityTypeBuilder<ResiduosNaoPerigosos> builder)
        {
            builder.ToTable("ResiduosGraficoResiduosNaoPerigosos", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(15)");

            builder.Property(c => c.Mineracao)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.OutrasMineracoes)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Siderurgia)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Cimento)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Logistica)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
