﻿using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class MudancasClimaticasGraficoEscopoConfiguration : IEntityTypeConfiguration<GraficoEscopo>
    {
        public void Configure(EntityTypeBuilder<GraficoEscopo> builder)
        {
            builder.ToTable("MudancasClimaticasGraficoEscopo", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Subtitulo)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.Mineracao)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Siderurgia)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Cimento)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Logistica)
                .IsRequired()
                .HasColumnType("float");

            builder.Property(c => c.Nota)
                .IsRequired()
                .HasColumnType("varchar(40)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
