﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraConfiguration : IEntityTypeConfiguration<EmissaoAtmosfericaFonteEmissora>
    {
        public void Configure(EntityTypeBuilder<EmissaoAtmosfericaFonteEmissora> builder)
        {
            builder.ToTable("QualidadeArTabelaEmissaoAtmosfericaFonteEmissora", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(1000)");

            builder.Property(c => c.CategoriaId)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasOne(x => x.Categoria).WithMany(x => x.FontesEmissoras);
        }
    }
}
