﻿using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class MudancasClimaticasTabelaEscopoParametroConfiguration : IEntityTypeConfiguration<TabelaEscopoParametro>
    {
        public void Configure(EntityTypeBuilder<TabelaEscopoParametro> builder)
        {
            builder.ToTable("MudancasClimaticasTabelaEscopo", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Nota)
                .IsRequired()
                .HasColumnType("varchar(350)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");
            
            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
