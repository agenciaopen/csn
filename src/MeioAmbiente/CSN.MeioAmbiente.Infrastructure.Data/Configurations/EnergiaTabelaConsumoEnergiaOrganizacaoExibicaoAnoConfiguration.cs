﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoAnoConfiguration : IEntityTypeConfiguration<ConsumoEnergiaOrganizacaoExibicaoAno>
    {
        public void Configure(EntityTypeBuilder<ConsumoEnergiaOrganizacaoExibicaoAno> builder)
        {
            builder.ToTable("EnergiaTabelaConsumoEnergiaOrganizacaoAno", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.Valor)
                .IsRequired();

            builder.Property(c => c.DescricaoId)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasOne(x => x.Descricao).WithMany(x => x.Anos);
        }
    }
}
