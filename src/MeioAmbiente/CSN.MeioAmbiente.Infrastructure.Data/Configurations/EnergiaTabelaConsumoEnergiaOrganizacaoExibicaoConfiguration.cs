﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoConfiguration : IEntityTypeConfiguration<ConsumoEnergiaOrganizacaoExibicao>
    {
        public void Configure(EntityTypeBuilder<ConsumoEnergiaOrganizacaoExibicao> builder)
        {
            builder.ToTable("EnergiaTabelaConsumoEnergiaOrganizacao", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Descricao)
                .IsRequired()
                .HasColumnType("varchar(70)");

            builder.Property(c => c.Unidade)
                .IsRequired()
                .HasColumnType("varchar(20)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasMany(x => x.Anos).WithOne(x => x.Descricao);
        }
    }
}
