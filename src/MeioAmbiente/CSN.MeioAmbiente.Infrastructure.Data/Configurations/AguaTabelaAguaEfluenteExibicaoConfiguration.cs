﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.MeioAmbiente.Infrastructure.Data.Configurations
{
    public class AguaTabelaAguaEfluenteExibicaoConfiguration : IEntityTypeConfiguration<AguaEfluenteExibicao>
    {
        public void Configure(EntityTypeBuilder<AguaEfluenteExibicao> builder)
        {
            builder.ToTable("AguaTabelaAguaEfluente", "MeioAmbiente");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.FonteCaptacao)
                .IsRequired()
                .HasColumnType("varchar(70)");

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();

            builder.HasMany(x => x.Anos).WithOne(x => x.FonteCaptacao);
        }
    }
}
