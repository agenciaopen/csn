﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository : BaseRepositoryParametersAsync<EmissaoAtmosfericaFonteEmissora>, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<EmissaoAtmosfericaFonteEmissora>> ObterFontesEmissorasPorCategoria(Guid categoriaId, bool? ativo = null)
        {
            var predicate = PredicateBuilder.True<EmissaoAtmosfericaFonteEmissora>();
            predicate = predicate.And(x => x.CategoriaId == categoriaId);

            if(ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<EmissaoAtmosfericaFonteEmissora>()
                .Include(c => c.Categoria)
                .Where(predicate)
                .OrderBy(x=>x.Nome)
                .ToListAsync();
        }

        public async Task<EmissaoAtmosfericaFonteEmissora?> ObterFontesEmissorasPorId(Guid id)
        {
            return await _context.DbSet<EmissaoAtmosfericaFonteEmissora>()
                .Include(c => c.Categoria)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<EmissaoAtmosfericaFonteEmissora>> ObterFontesEmissorasPorNome(string? nome = null, bool? ativo = null)
        {
            var predicate = PredicateBuilder.True<EmissaoAtmosfericaFonteEmissora>();

            if (!string.IsNullOrEmpty(nome))
                predicate = predicate.And(x => x.Nome.StartsWith(nome));

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<EmissaoAtmosfericaFonteEmissora>()
                .Include(c => c.Categoria)
                .Where(predicate)
                .OrderBy(x => x.Nome)
                .ToListAsync();
        }
    }
}
