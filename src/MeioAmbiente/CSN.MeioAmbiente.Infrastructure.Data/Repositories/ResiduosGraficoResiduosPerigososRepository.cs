﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class ResiduosGraficoResiduosPerigososRepository : BaseRepositoryAsync<ResiduosPerigosos>
    {
        protected readonly MeioAmbienteDbContext _context;

        public ResiduosGraficoResiduosPerigososRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
