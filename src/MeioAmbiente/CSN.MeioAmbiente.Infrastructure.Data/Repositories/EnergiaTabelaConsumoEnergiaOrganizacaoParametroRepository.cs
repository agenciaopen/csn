﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoParametroRepository : BaseRepositoryParametersAsync<ConsumoEnergiaOrganizacaoParametro>
    {
        protected readonly MeioAmbienteDbContext _context;

        public EnergiaTabelaConsumoEnergiaOrganizacaoParametroRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
