﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class AguaGraficoCaptacaoRepository : BaseRepositoryAsync<Captacao>
    {
        protected readonly MeioAmbienteDbContext _context;

        public AguaGraficoCaptacaoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
