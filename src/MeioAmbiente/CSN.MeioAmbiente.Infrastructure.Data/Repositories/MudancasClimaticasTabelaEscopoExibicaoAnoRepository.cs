﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class MudancasClimaticasTabelaEscopoExibicaoAnoRepository : BaseRepositoryAsync<TabelaEscopoExibicaoAno>
    {
        protected readonly MeioAmbienteDbContext _context;

        public MudancasClimaticasTabelaEscopoExibicaoAnoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
