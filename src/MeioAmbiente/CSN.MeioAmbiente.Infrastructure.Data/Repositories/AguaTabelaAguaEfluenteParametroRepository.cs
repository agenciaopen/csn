﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class AguaTabelaAguaEfluenteParametroRepository : BaseRepositoryParametersAsync<AguaEfluenteParametro>
    {
        protected readonly MeioAmbienteDbContext _context;

        public AguaTabelaAguaEfluenteParametroRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
