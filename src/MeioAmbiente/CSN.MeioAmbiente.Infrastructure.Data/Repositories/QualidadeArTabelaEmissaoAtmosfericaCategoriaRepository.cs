﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public  class QualidadeArTabelaEmissaoAtmosfericaCategoriaRepository : BaseRepositoryParametersAsync<EmissaoAtmosfericaCategoria>, IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository
    {

        protected readonly MeioAmbienteDbContext _context;

        public QualidadeArTabelaEmissaoAtmosfericaCategoriaRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<EmissaoAtmosfericaCategoria?> ObterCategoriaPorId(Guid id)
        {
            return await _context.DbSet<EmissaoAtmosfericaCategoria>()
                .Include(c => c.FontesEmissoras)
                .Include(x=>x.Anos)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<EmissaoAtmosfericaCategoria>> ObterCategoriasPorIds(List<Guid> ids)
        {
            return await _context.DbSet<EmissaoAtmosfericaCategoria>()
                .Include(c => c.FontesEmissoras)
                .Include(x => x.Anos)
                .Where(x => ids.Contains(x.Id))
                .ToListAsync();
        }

        public async Task<List<EmissaoAtmosfericaCategoria>> ObterCategorias(string? nome = null, bool? ativo = null)
        {
            var predicate = PredicateBuilder.True<EmissaoAtmosfericaCategoria>();

            if(!string.IsNullOrEmpty(nome))
                predicate = predicate.And(x => x.Nome.StartsWith(nome));

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<EmissaoAtmosfericaCategoria>()
                .Include(c => c.FontesEmissoras)
                .Include(x => x.Anos)
                .Where(predicate)
                .OrderBy(x => x.Nome)
                .ToListAsync();
        }
    }
}
