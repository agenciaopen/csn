﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class AguaTabelaAguaEfluenteExibicaoRepository : BaseRepositoryAsync<AguaEfluenteExibicao>, IAguaTabelaAguaEfluenteExibicaoRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public AguaTabelaAguaEfluenteExibicaoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<AguaEfluenteExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<AguaEfluenteParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<AguaEfluenteExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .Take(parametro.QuantidadeAnos)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }

        public async Task<IEnumerable<AguaEfluenteExibicaoAno>> ObterAnosPorFonteCaptacaoAsync(Guid fonteCaptacaoId, Idioma idioma, bool? ativo)
        {
            var predicate = PredicateBuilder.True<AguaEfluenteExibicaoAno>();
            predicate = predicate.And(x => x.FonteCaptacaoId == fonteCaptacaoId);
            predicate = predicate.And(x => x.Idioma == idioma);

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<AguaEfluenteExibicaoAno>().Where(predicate).ToListAsync();
        }
    }
}
