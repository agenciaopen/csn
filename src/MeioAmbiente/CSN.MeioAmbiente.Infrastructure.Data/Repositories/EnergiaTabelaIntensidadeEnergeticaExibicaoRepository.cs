﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class EnergiaTabelaIntensidadeEnergeticaExibicaoRepository : BaseRepositoryAsync<IntensidadeEnergeticaExibicao>, IEnergiaTabelaIntensidadeEnergeticaExibicaoRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public EnergiaTabelaIntensidadeEnergeticaExibicaoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<IntensidadeEnergeticaExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<IntensidadeEnergeticaParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<IntensidadeEnergeticaExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .Take(parametro.QuantidadeAnos)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }

        public async Task<IEnumerable<IntensidadeEnergeticaExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo)
        {
            var predicate = PredicateBuilder.True<IntensidadeEnergeticaExibicaoAno>();
            predicate = predicate.And(x => x.DescricaoId == descricaoId);
            predicate = predicate.And(x => x.Idioma == idioma);

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<IntensidadeEnergeticaExibicaoAno>().Where(predicate).ToListAsync();
        }
    }
}
