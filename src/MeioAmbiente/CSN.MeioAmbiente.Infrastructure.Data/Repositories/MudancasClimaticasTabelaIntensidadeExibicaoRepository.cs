﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class MudancasClimaticasTabelaIntensidadeExibicaoRepository : BaseRepositoryAsync<TabelaIntensidadeExibicao>, IMudancasClimaticasTabelaIntensidadeExibicaoRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public MudancasClimaticasTabelaIntensidadeExibicaoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TabelaIntensidadeExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<TabelaIntensidadeParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<TabelaIntensidadeExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .Take(parametro.QuantidadeAnos)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }

        public async Task<IEnumerable<TabelaIntensidadeExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo)
        {
            var predicate = PredicateBuilder.True<TabelaIntensidadeExibicaoAno>();
            predicate = predicate.And(x => x.DescricaoId == descricaoId);
            predicate = predicate.And(x => x.Idioma == idioma);

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<TabelaIntensidadeExibicaoAno>().Where(predicate).ToListAsync();
        }
    }
}
