﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Infrastructure.Data.Context;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class QualidadeArTabelaEmissaoAtmosfericaUnidadeRepository : BaseRepositoryParametersAsync<EmissaoAtmosfericaUnidade>
    {

        protected readonly MeioAmbienteDbContext _context;

        public QualidadeArTabelaEmissaoAtmosfericaUnidadeRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
