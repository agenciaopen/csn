﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using CSN.MeioAmbiente.Infrastructure.Data.Repositories;

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public class AguaTabelaAguaEfluenteExibicaoAnoRepository : BaseRepositoryAsync<AguaEfluenteExibicaoAno>
    {
        protected readonly MeioAmbienteDbContext _context;

        public AguaTabelaAguaEfluenteExibicaoAnoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }
    }

}
