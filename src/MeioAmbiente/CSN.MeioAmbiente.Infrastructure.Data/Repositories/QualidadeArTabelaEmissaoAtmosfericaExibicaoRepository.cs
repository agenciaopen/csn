﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class QualidadeArTabelaEmissaoAtmosfericaExibicaoRepository : BaseRepositoryAsync<EmissaoAtmosfericaExibicao> , IQualidadeArTabelaEmissaoAtmosfericaExibicaoRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public QualidadeArTabelaEmissaoAtmosfericaExibicaoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<EmissaoAtmosfericaExibicao?> ObterUltimoAtivo(Idioma idioma)
        {
            var parametro = await _context.DbSet<EmissaoAtmosfericaParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            return await _context.DbSet<EmissaoAtmosfericaExibicao>()
                .Include(c => c.Unidade)
                .Include(c => c.Categorias.OrderBy(x => x.Nome))
                .ThenInclude(a => a.FontesEmissoras.OrderBy(x => x.Nome))
                .Include(c => c.Categorias.OrderBy(x => x.Nome))
                .ThenInclude(a => a.Anos.OrderByDescending(x => x.Ano).Take(parametro.QuantidadeAnos))
                .FirstOrDefaultAsync(x => x.Idioma == idioma && x.Ativo);
        }
    }
}
