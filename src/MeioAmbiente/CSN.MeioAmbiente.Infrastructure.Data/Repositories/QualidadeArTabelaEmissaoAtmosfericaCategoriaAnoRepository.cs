﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.MeioAmbiente.Infrastructure.Data.Repositories
{
    public class QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository : BaseRepositoryParametersAsync<EmissaoAtmosfericaCategoriaAno>, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository
    {
        protected readonly MeioAmbienteDbContext _context;

        public QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository(MeioAmbienteDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<List<EmissaoAtmosfericaCategoriaAno>> ObterAnosPorCategoria(Guid categoriaId, bool? ativo = null)
        {
            var predicate = PredicateBuilder.True<EmissaoAtmosfericaCategoriaAno>();

            predicate = predicate.And(x => x.CategoriaId == categoriaId);

            if (ativo != null)
                predicate = predicate.And(x => x.Ativo == ativo);

            return await _context.DbSet<EmissaoAtmosfericaCategoriaAno>()
                .Include(c => c.Categoria)
                .Where(predicate)
                .OrderByDescending(x => x.Ano)
                .ToListAsync();
        }
    }
}
