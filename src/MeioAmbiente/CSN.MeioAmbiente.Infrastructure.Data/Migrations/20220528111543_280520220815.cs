﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _280520220815 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AguaGraficoCaptacao",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(15)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    OutrasMineracoes = table.Column<double>(type: "float", nullable: false),
                    InformacaoUm = table.Column<string>(type: "varchar(55)", nullable: false),
                    InformacaoDois = table.Column<string>(type: "varchar(55)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaGraficoCaptacao", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AguaGraficoCaptacao",
                schema: "MeioAmbiente");
        }
    }
}
