﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _270520221918 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AguaGraficoDescarte",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(15)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    InformacaoUm = table.Column<string>(type: "varchar(55)", nullable: false),
                    InformacaoDois = table.Column<string>(type: "varchar(55)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaGraficoDescarte", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Descricao = table.Column<string>(type: "varchar(60)", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaIntensidade", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MudancasClimaticasTabelaIntensidade_MudancasClimaticasTabelaIntensidade_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "MudancasClimaticasTabelaIntensidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaIntensidadeAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DescricaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaIntensidadeAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MudancasClimaticasTabelaIntensidadeAno_MudancasClimaticasTabelaIntensidade_DescricaoId",
                        column: x => x.DescricaoId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "MudancasClimaticasTabelaIntensidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaIntensidade_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaIntensidadeAno_DescricaoId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidadeAno",
                column: "DescricaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AguaGraficoDescarte",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaIntensidadeAno",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "MeioAmbiente");
        }
    }
}
