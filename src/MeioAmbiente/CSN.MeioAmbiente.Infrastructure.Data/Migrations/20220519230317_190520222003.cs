﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _190520222003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosPerigosos",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosNaoPerigosos",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasGraficoEscopo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosPerigosos");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosNaoPerigosos");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasGraficoEscopo");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao");

            migrationBuilder.DropColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo");
        }
    }
}
