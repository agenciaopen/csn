﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _290520221530 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaFonteEmissora",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(1000)", nullable: false),
                    CategoriaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QualidadeArTabelaEmissaoAtmosfericaFonteEmissora", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QualidadeArTabelaEmissaoAtmosfericaFonteEmissora_QualidadeArTabelaEmissaoAtmosfericaCategoria_CategoriaId",
                        column: x => x.CategoriaId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "QualidadeArTabelaEmissaoAtmosfericaCategoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QualidadeArTabelaEmissaoAtmosfericaFonteEmissora_CategoriaId",
                schema: "MeioAmbiente",
                table: "QualidadeArTabelaEmissaoAtmosfericaFonteEmissora",
                column: "CategoriaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaFonteEmissora",
                schema: "MeioAmbiente");
        }
    }
}
