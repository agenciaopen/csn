﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _220520221504 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Descricao = table.Column<string>(type: "varchar(60)", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaEscopo", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MudancasClimaticasTabelaEscopo_MudancasClimaticasTabelaEscopo_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "MudancasClimaticasTabelaEscopo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaEscopoAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DescricaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaEscopoAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MudancasClimaticasTabelaEscopoAno_MudancasClimaticasTabelaEscopo_DescricaoId",
                        column: x => x.DescricaoId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "MudancasClimaticasTabelaEscopo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaEscopo_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaEscopoAno_DescricaoId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopoAno",
                column: "DescricaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaEscopoAno",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "MeioAmbiente");
        }
    }
}
