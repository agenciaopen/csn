﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _220520221334 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Descricao = table.Column<string>(type: "varchar(70)", nullable: false),
                    Unidade = table.Column<string>(type: "varchar(20)", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaIntensidadeEnergetica", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnergiaTabelaIntensidadeEnergetica_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "EnergiaTabelaIntensidadeEnergetica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EnergiaTabelaIntensidadeEnergeticaAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DescricaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaIntensidadeEnergeticaAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnergiaTabelaIntensidadeEnergeticaAno_EnergiaTabelaIntensidadeEnergetica_DescricaoId",
                        column: x => x.DescricaoId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "EnergiaTabelaIntensidadeEnergetica",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaIntensidadeEnergeticaAno_DescricaoId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergeticaAno",
                column: "DescricaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnergiaTabelaIntensidadeEnergeticaAno",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "MeioAmbiente");
        }
    }
}
