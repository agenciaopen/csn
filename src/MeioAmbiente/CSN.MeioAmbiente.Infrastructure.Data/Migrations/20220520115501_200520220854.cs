﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _200520220854 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosPerigosos",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosNaoPerigosos",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasGraficoEscopo",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                newName: "Referencia");

            migrationBuilder.RenameColumn(
                name: "ReferenciaIdioma",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo",
                newName: "Referencia");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosPerigosos",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "MeioAmbiente",
                table: "ResiduosGraficoResiduosNaoPerigosos",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasGraficoEscopo",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                newName: "ReferenciaIdioma");

            migrationBuilder.RenameColumn(
                name: "Referencia",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo",
                newName: "ReferenciaIdioma");
        }
    }
}
