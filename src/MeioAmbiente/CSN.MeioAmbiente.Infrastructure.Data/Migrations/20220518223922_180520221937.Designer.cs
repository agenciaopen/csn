﻿// <auto-generated />
using System;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    [DbContext(typeof(MeioAmbienteDbContext))]
    [Migration("20220518223922_180520221937")]
    partial class _180520221937
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.Agua.Grafico.Consumo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("Cimento")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("InformacaoDois")
                        .IsRequired()
                        .HasColumnType("varchar(55)");

                    b.Property<string>("InformacaoUm")
                        .IsRequired()
                        .HasColumnType("varchar(55)");

                    b.Property<double>("Logistica")
                        .HasColumnType("float");

                    b.Property<double>("Mineracao")
                        .HasColumnType("float");

                    b.Property<double>("Siderurgia")
                        .HasColumnType("float");

                    b.Property<string>("Subtitulo")
                        .IsRequired()
                        .HasColumnType("varchar(15)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("AguaGraficoConsumo", "MeioAmbiente");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.Energia.Tabela.ConsumoEnergiaOrganizacaoParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AreaNegocio")
                        .IsRequired()
                        .HasColumnType("varchar(500)");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("EnergiaTabelaConsumoEnergiaOrganizacao", "Parametros");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.Energia.Tabela.IntensidadeEnergeticaParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AreaNegocio")
                        .IsRequired()
                        .HasColumnType("varchar(500)");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Subtitulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("EnergiaTabelaIntensidadeEnergetica", "Parametros");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico.GraficoEscopo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("Cimento")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<double>("Logistica")
                        .HasColumnType("float");

                    b.Property<double>("Mineracao")
                        .HasColumnType("float");

                    b.Property<string>("Nota")
                        .IsRequired()
                        .HasColumnType("varchar(40)");

                    b.Property<double>("Siderurgia")
                        .HasColumnType("float");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(20)");

                    b.Property<string>("Subtitulo")
                        .IsRequired()
                        .HasColumnType("varchar(30)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("MudancasClimaticasGraficoEscopo", "MeioAmbiente");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela.TabelaEscopoParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("Nota")
                        .IsRequired()
                        .HasColumnType("varchar(350)");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("MudancasClimaticasTabelaEscopo", "Parametros");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela.TabelaIntensidadeParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("Nota")
                        .IsRequired()
                        .HasColumnType("varchar(350)");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("MudancasClimaticasTabelaIntensidade", "Parametros");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico.ResiduosNaoPerigosos", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("Cimento")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<double>("Logistica")
                        .HasColumnType("float");

                    b.Property<double>("Mineracao")
                        .HasColumnType("float");

                    b.Property<double>("OutrasMineracoes")
                        .HasColumnType("float");

                    b.Property<double>("Siderurgia")
                        .HasColumnType("float");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(15)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<double>("Total")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("ResiduosGraficoResiduosNaoPerigosos", "MeioAmbiente");
                });

            modelBuilder.Entity("CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico.ResiduosPerigosos", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("Cimento")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<double>("Logistica")
                        .HasColumnType("float");

                    b.Property<double>("Mineracao")
                        .HasColumnType("float");

                    b.Property<double>("OutrasMineracoes")
                        .HasColumnType("float");

                    b.Property<double>("Siderurgia")
                        .HasColumnType("float");

                    b.Property<string>("Sigla")
                        .IsRequired()
                        .HasColumnType("varchar(15)");

                    b.Property<string>("Titulo")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.Property<double>("Total")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("ResiduosGraficoResiduosPerigosos", "MeioAmbiente");
                });
#pragma warning restore 612, 618
        }
    }
}
