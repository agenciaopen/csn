﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _220520221200 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Descricao = table.Column<string>(type: "varchar(70)", nullable: false),
                    Unidade = table.Column<string>(type: "varchar(20)", nullable: false),
                    ParametroId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaConsumoEnergiaOrganizacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnergiaTabelaConsumoEnergiaOrganizacao_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                        column: x => x.ParametroId,
                        principalSchema: "Parametros",
                        principalTable: "EnergiaTabelaConsumoEnergiaOrganizacao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacaoAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DescricaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaConsumoEnergiaOrganizacaoAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EnergiaTabelaConsumoEnergiaOrganizacaoAno_EnergiaTabelaConsumoEnergiaOrganizacao_DescricaoId",
                        column: x => x.DescricaoId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "EnergiaTabelaConsumoEnergiaOrganizacao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaConsumoEnergiaOrganizacaoAno_DescricaoId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacaoAno",
                column: "DescricaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacaoAno",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "MeioAmbiente");
        }
    }
}
