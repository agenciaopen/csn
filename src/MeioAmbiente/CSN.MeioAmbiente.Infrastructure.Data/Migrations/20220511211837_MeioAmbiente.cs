﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class MeioAmbiente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "MeioAmbiente");

            migrationBuilder.CreateTable(
                name: "AguaGraficoConsumo",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(15)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    InformacaoUm = table.Column<string>(type: "varchar(55)", nullable: false),
                    InformacaoDois = table.Column<string>(type: "varchar(55)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaGraficoConsumo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false),
                    AreaNegocio = table.Column<string>(type: "varchar(500)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaConsumoEnergiaOrganizacao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false),
                    AreaNegocio = table.Column<string>(type: "varchar(500)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnergiaTabelaIntensidadeEnergetica", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasGraficoEscopo",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(30)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(20)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    Nota = table.Column<string>(type: "varchar(40)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasGraficoEscopo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Nota = table.Column<string>(type: "varchar(350)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaEscopo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Nota = table.Column<string>(type: "varchar(350)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MudancasClimaticasTabelaIntensidade", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResiduosGraficoResiduosNaoPerigosos",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(15)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    OutrasMineracoes = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    Total = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResiduosGraficoResiduosNaoPerigosos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResiduosGraficoResiduosPerigosos",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(15)", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    OutrasMineracoes = table.Column<double>(type: "float", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    Total = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResiduosGraficoResiduosPerigosos", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AguaGraficoConsumo",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasGraficoEscopo",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "ResiduosGraficoResiduosNaoPerigosos",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "ResiduosGraficoResiduosPerigosos",
                schema: "MeioAmbiente");
        }
    }
}
