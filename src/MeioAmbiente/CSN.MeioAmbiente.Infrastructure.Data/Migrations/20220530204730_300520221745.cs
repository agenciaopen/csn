﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _300520221745 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaCategoriaAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Valor = table.Column<double>(type: "float", nullable: false),
                    CategoriaId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QualidadeArTabelaEmissaoAtmosfericaCategoriaAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QualidadeArTabelaEmissaoAtmosfericaCategoriaAno_QualidadeArTabelaEmissaoAtmosfericaCategoria_CategoriaId",
                        column: x => x.CategoriaId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "QualidadeArTabelaEmissaoAtmosfericaCategoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QualidadeArTabelaEmissaoAtmosfericaCategoriaAno_CategoriaId",
                schema: "MeioAmbiente",
                table: "QualidadeArTabelaEmissaoAtmosfericaCategoriaAno",
                column: "CategoriaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaCategoriaAno",
                schema: "MeioAmbiente");
        }
    }
}
