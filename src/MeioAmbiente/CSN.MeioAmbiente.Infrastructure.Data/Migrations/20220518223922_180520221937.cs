﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _180520221937 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Parametros");

            migrationBuilder.RenameTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "MeioAmbiente",
                newName: "MudancasClimaticasTabelaIntensidade",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "MeioAmbiente",
                newName: "MudancasClimaticasTabelaEscopo",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "MeioAmbiente",
                newName: "EnergiaTabelaIntensidadeEnergetica",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "MeioAmbiente",
                newName: "EnergiaTabelaConsumoEnergiaOrganizacao",
                newSchema: "Parametros");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Slug",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                type: "varchar(50)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaIntensidade");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "MudancasClimaticasTabelaEscopo");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "EnergiaTabelaIntensidadeEnergetica");

            migrationBuilder.DropColumn(
                name: "Slug",
                schema: "Parametros",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao");

            migrationBuilder.RenameTable(
                name: "MudancasClimaticasTabelaIntensidade",
                schema: "Parametros",
                newName: "MudancasClimaticasTabelaIntensidade",
                newSchema: "MeioAmbiente");

            migrationBuilder.RenameTable(
                name: "MudancasClimaticasTabelaEscopo",
                schema: "Parametros",
                newName: "MudancasClimaticasTabelaEscopo",
                newSchema: "MeioAmbiente");

            migrationBuilder.RenameTable(
                name: "EnergiaTabelaIntensidadeEnergetica",
                schema: "Parametros",
                newName: "EnergiaTabelaIntensidadeEnergetica",
                newSchema: "MeioAmbiente");

            migrationBuilder.RenameTable(
                name: "EnergiaTabelaConsumoEnergiaOrganizacao",
                schema: "Parametros",
                newName: "EnergiaTabelaConsumoEnergiaOrganizacao",
                newSchema: "MeioAmbiente");
        }
    }
}
