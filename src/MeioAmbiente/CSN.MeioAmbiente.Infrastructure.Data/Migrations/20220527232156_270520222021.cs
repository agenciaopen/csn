﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _270520222021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EnergiaTabelaConsumoEnergiaOrganizacao_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao");

            migrationBuilder.DropForeignKey(
                name: "FK_EnergiaTabelaIntensidadeEnergetica_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica");

            migrationBuilder.DropForeignKey(
                name: "FK_MudancasClimaticasTabelaEscopo_MudancasClimaticasTabelaEscopo_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo");

            migrationBuilder.DropForeignKey(
                name: "FK_MudancasClimaticasTabelaIntensidade_MudancasClimaticasTabelaIntensidade_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade");

            migrationBuilder.DropIndex(
                name: "IX_MudancasClimaticasTabelaIntensidade_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade");

            migrationBuilder.DropIndex(
                name: "IX_MudancasClimaticasTabelaEscopo_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo");

            migrationBuilder.DropIndex(
                name: "IX_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica");

            migrationBuilder.DropIndex(
                name: "IX_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica");

            migrationBuilder.DropColumn(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao");

            migrationBuilder.AddColumn<double>(
                name: "OutrasMineracoes",
                schema: "MeioAmbiente",
                table: "AguaGraficoDescarte",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "OutrasMineracoes",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.CreateTable(
                name: "AguaTabelaAguaEfluenteParametro",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    EmMegaLitrosMl = table.Column<string>(type: "varchar(30)", nullable: false),
                    AreaNegocio = table.Column<string>(type: "varchar(500)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaTabelaAguaEfluenteParametro", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AguaTabelaAguaEfluenteParametro",
                schema: "Parametros");

            migrationBuilder.DropColumn(
                name: "OutrasMineracoes",
                schema: "MeioAmbiente",
                table: "AguaGraficoDescarte");

            migrationBuilder.DropColumn(
                name: "OutrasMineracoes",
                schema: "MeioAmbiente",
                table: "AguaGraficoConsumo");

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaIntensidade_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_MudancasClimaticasTabelaEscopo_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica",
                column: "ParametroId");

            migrationBuilder.CreateIndex(
                name: "IX_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                column: "ParametroId");

            migrationBuilder.AddForeignKey(
                name: "FK_EnergiaTabelaConsumoEnergiaOrganizacao_EnergiaTabelaConsumoEnergiaOrganizacao_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaConsumoEnergiaOrganizacao",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "EnergiaTabelaConsumoEnergiaOrganizacao",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EnergiaTabelaIntensidadeEnergetica_EnergiaTabelaIntensidadeEnergetica_ParametroId",
                schema: "MeioAmbiente",
                table: "EnergiaTabelaIntensidadeEnergetica",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "EnergiaTabelaIntensidadeEnergetica",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MudancasClimaticasTabelaEscopo_MudancasClimaticasTabelaEscopo_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaEscopo",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "MudancasClimaticasTabelaEscopo",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MudancasClimaticasTabelaIntensidade_MudancasClimaticasTabelaIntensidade_ParametroId",
                schema: "MeioAmbiente",
                table: "MudancasClimaticasTabelaIntensidade",
                column: "ParametroId",
                principalSchema: "Parametros",
                principalTable: "MudancasClimaticasTabelaIntensidade",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
