﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _300520221805 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaExibicao",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UnidadeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QualidadeArTabelaEmissaoAtmosfericaExibicao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QualidadeArTabelaEmissaoAtmosfericaExibicao_QualidadeArTabelaEmissaoAtmosfericaUnidade_UnidadeId",
                        column: x => x.UnidadeId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "QualidadeArTabelaEmissaoAtmosfericaUnidade",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaExibicaoCategorias",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    CategoriasId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExibicaosId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmissaoAtmosfericaCategoriaEmissaoAtmosfericaExibicao", x => new { x.CategoriasId, x.ExibicaosId });
                    table.ForeignKey(
                        name: "FK_EmissaoAtmosfericaCategoriaEmissaoAtmosfericaExibicao_QualidadeArTabelaEmissaoAtmosfericaCategoria_CategoriasId",
                        column: x => x.CategoriasId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "QualidadeArTabelaEmissaoAtmosfericaCategoria",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmissaoAtmosfericaCategoriaEmissaoAtmosfericaExibicao_QualidadeArTabelaEmissaoAtmosfericaExibicao_ExibicaosId",
                        column: x => x.ExibicaosId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "QualidadeArTabelaEmissaoAtmosfericaExibicao",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmissaoAtmosfericaCategoriaEmissaoAtmosfericaExibicao_ExibicaosId",
                schema: "MeioAmbiente",
                table: "QualidadeArTabelaEmissaoAtmosfericaExibicaoCategorias",
                column: "ExibicaosId");

            migrationBuilder.CreateIndex(
                name: "IX_QualidadeArTabelaEmissaoAtmosfericaExibicao_UnidadeId",
                schema: "MeioAmbiente",
                table: "QualidadeArTabelaEmissaoAtmosfericaExibicao",
                column: "UnidadeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaExibicaoCategorias",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "QualidadeArTabelaEmissaoAtmosfericaExibicao",
                schema: "MeioAmbiente");
        }
    }
}
