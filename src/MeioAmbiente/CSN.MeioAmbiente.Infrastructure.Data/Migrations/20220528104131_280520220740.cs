﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.MeioAmbiente.Infrastructure.Data.Migrations
{
    public partial class _280520220740 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AguaTabelaAguaEfluente",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FonteCaptacao = table.Column<string>(type: "varchar(70)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaTabelaAguaEfluente", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AguaTabelaAguaEfluenteAno",
                schema: "MeioAmbiente",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FonteCaptacaoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    TodasAreas = table.Column<double>(type: "float", nullable: false),
                    AreasComEstresseHidrico = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AguaTabelaAguaEfluenteAno", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AguaTabelaAguaEfluenteAno_AguaTabelaAguaEfluente_FonteCaptacaoId",
                        column: x => x.FonteCaptacaoId,
                        principalSchema: "MeioAmbiente",
                        principalTable: "AguaTabelaAguaEfluente",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AguaTabelaAguaEfluenteAno_FonteCaptacaoId",
                schema: "MeioAmbiente",
                table: "AguaTabelaAguaEfluenteAno",
                column: "FonteCaptacaoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AguaTabelaAguaEfluenteAno",
                schema: "MeioAmbiente");

            migrationBuilder.DropTable(
                name: "AguaTabelaAguaEfluente",
                schema: "MeioAmbiente");
        }
    }
}
