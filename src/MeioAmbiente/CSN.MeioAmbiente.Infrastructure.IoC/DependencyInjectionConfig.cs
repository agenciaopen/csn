﻿using CSN.Core;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Services;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;
using CSN.MeioAmbiente.Domain.Interfaces;
using CSN.MeioAmbiente.Infrastructure.Data.Context;
using CSN.MeioAmbiente.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CSN.MeioAmbiente.Infrastructure.IoC
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection AddMeioAmbienteContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<MeioAmbienteDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("CSN")));

            return services;
        }

        public static IServiceCollection AddMeioAmbienteServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<MeioAmbienteDbContext>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaParametro>, BaseRepositoryParametersAsync<EmissaoAtmosfericaParametro>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaParametroService, QualidadeArTabelaEmissaoAtmosfericaParametroService>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaCategoria>, BaseRepositoryParametersAsync<EmissaoAtmosfericaCategoria>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository, QualidadeArTabelaEmissaoAtmosfericaCategoriaRepository>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaCategoriaService, QualidadeArTabelaEmissaoAtmosfericaCategoriaService>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaCategoriaAno>, BaseRepositoryParametersAsync<EmissaoAtmosfericaCategoriaAno>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository, QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService, QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaFonteEmissora>, BaseRepositoryParametersAsync<EmissaoAtmosfericaFonteEmissora>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository, QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService, QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaUnidade>, BaseRepositoryParametersAsync<EmissaoAtmosfericaUnidade>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaUnidadeService, QualidadeArTabelaEmissaoAtmosfericaUnidadeService>();

            services.AddScoped<IRepositoryAsync<EmissaoAtmosfericaExibicao>, BaseRepositoryParametersAsync<EmissaoAtmosfericaExibicao>>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaExibicaoRepository, QualidadeArTabelaEmissaoAtmosfericaExibicaoRepository>();
            services.AddScoped<IQualidadeArTabelaEmissaoAtmosfericaExibicaoService, QualidadeArTabelaEmissaoAtmosfericaExibicaoService>();

            services.AddScoped<ICsnRepositoryAsync<Captacao>, BaseRepositoryAsync<Captacao>>();
            services.AddScoped<IAguaGraficoCaptacaoService, AguaGraficoCaptacaoService>();

            services.AddScoped<ICsnRepositoryAsync<Consumo>, BaseRepositoryAsync<Consumo>>();
            services.AddScoped<IAguaGraficoConsumoService, AguaGraficoConsumoService>();

            services.AddScoped<ICsnRepositoryAsync<Descarte>, BaseRepositoryAsync<Descarte>>();
            services.AddScoped<IAguaGraficoDescarteService, AguaGraficoDescarteService>();

            services.AddScoped<IRepositoryAsync<AguaEfluenteParametro>, BaseRepositoryParametersAsync<AguaEfluenteParametro>>();
            services.AddScoped<IAguaTabelaAguaEfluenteParametroService, AguaTabelaAguaEfluenteParametroService>();

            services.AddScoped<ICsnRepositoryAsync<AguaEfluenteExibicao>, BaseRepositoryAsync<AguaEfluenteExibicao>>();
            services.AddScoped<IAguaTabelaAguaEfluenteExibicaoService, AguaTabelaAguaEfluenteExibicaoService>();

            services.AddScoped<IAguaTabelaAguaEfluenteExibicaoRepository, AguaTabelaAguaEfluenteExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<AguaEfluenteExibicaoAno>, BaseRepositoryAsync<AguaEfluenteExibicaoAno>>();


            services.AddScoped<IRepositoryAsync<ConsumoEnergiaOrganizacaoParametro>, BaseRepositoryParametersAsync<ConsumoEnergiaOrganizacaoParametro>>();
            services.AddScoped<ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicao>, BaseRepositoryAsync<ConsumoEnergiaOrganizacaoExibicao>>();
            services.AddScoped<IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoRepository, EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicaoAno>, BaseRepositoryAsync<ConsumoEnergiaOrganizacaoExibicaoAno>>();


            services.AddScoped<IRepositoryAsync<IntensidadeEnergeticaParametro>, BaseRepositoryParametersAsync<IntensidadeEnergeticaParametro>>();
            services.AddScoped<ICsnRepositoryAsync<IntensidadeEnergeticaExibicao>, BaseRepositoryAsync<IntensidadeEnergeticaExibicao>>();
            services.AddScoped<IEnergiaTabelaIntensidadeEnergeticaExibicaoRepository, EnergiaTabelaIntensidadeEnergeticaExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<IntensidadeEnergeticaExibicaoAno>, BaseRepositoryAsync<IntensidadeEnergeticaExibicaoAno>>();
            
            services.AddScoped<ICsnRepositoryAsync<CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico.GraficoEscopo>, BaseRepositoryAsync<CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico.GraficoEscopo>>();
            
            services.AddScoped<IRepositoryAsync<TabelaEscopoParametro>, BaseRepositoryParametersAsync<TabelaEscopoParametro>>();
            services.AddScoped<ICsnRepositoryAsync<TabelaEscopoExibicao>, BaseRepositoryAsync<TabelaEscopoExibicao>>();
            services.AddScoped<IMudancasClimaticasTabelaEscopoExibicaoRepository, MudancasClimaticasTabelaEscopoExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<TabelaEscopoExibicaoAno>, BaseRepositoryAsync<TabelaEscopoExibicaoAno>>();

            services.AddScoped<IRepositoryAsync<TabelaIntensidadeParametro>, BaseRepositoryParametersAsync<TabelaIntensidadeParametro>>();
            services.AddScoped<ICsnRepositoryAsync<TabelaIntensidadeExibicao>, BaseRepositoryAsync<TabelaIntensidadeExibicao>>();
            services.AddScoped<IMudancasClimaticasTabelaIntensidadeExibicaoRepository, MudancasClimaticasTabelaIntensidadeExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<TabelaIntensidadeExibicaoAno>, BaseRepositoryAsync<TabelaIntensidadeExibicaoAno>>();

            services.AddScoped<ICsnRepositoryAsync<ResiduosNaoPerigosos>, BaseRepositoryAsync<ResiduosNaoPerigosos>>();
            services.AddScoped<ICsnRepositoryAsync<ResiduosPerigosos>, BaseRepositoryAsync<ResiduosPerigosos>>();


            services.AddScoped<IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService, EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService>();
            services.AddScoped<IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService, EnergiaTabelaConsumoEnergiaOrganizacaoParametroService>();
            services.AddScoped<IEnergiaTabelaIntensidadeEnergeticaParametroService, EnergiaTabelaIntensidadeEnergeticaParametroService>();
            services.AddScoped<IMudancasClimaticasGraficoEscopoService, MudancasClimaticasGraficoEscopoService>();
            services.AddScoped<IMudancasClimaticasTabelaEscopoParametroService, MudancasClimaticasTabelaEscopoParametroService>();
            services.AddScoped<IMudancasClimaticasTabelaIntensidadeParametroService, MudancasClimaticasTabelaIntensidadeParametroService>();
            services.AddScoped<IMudancasClimaticasTabelaEscopoExibicaoService, MudancasClimaticasTabelaEscopoExibicaoService>();
            services.AddScoped<IMudancasClimaticasTabelaIntensidadeExibicaoService, MudancasClimaticasTabelaIntensidadeExibicaoService>();
            services.AddScoped<IEnergiaTabelaIntensidadeEnergeticaExibicaoService, EnergiaTabelaIntensidadeEnergeticaExibicaoService>();

            services.AddScoped<IResiduosGraficoResiduosNaoPerigososService, ResiduosGraficoResiduosNaoPerigososService>();
            services.AddScoped<IResiduosGraficoResiduosPerigososService, ResiduosGraficoResiduosPerigososService>();

            return services;
        }
    }
}