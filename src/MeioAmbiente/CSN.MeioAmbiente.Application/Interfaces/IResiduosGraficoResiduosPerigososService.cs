﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Residuos;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IResiduosGraficoResiduosPerigososService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoResiduosPerigososRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoResiduosPerigososRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoResiduosPerigososRequest request);
        Task<ResponseResult<ResiduosPerigosos>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoResiduosPerigososRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoResiduosPerigososRequest request);
    }
}
