﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IMudancasClimaticasTabelaIntensidadeParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaIntensidadeParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaIntensidadeParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaIntensidadeParametroRequest request);
        Task<ResponseResult<TabelaIntensidadeParametro>> ObterPorIdAsync(ObterPorIdTabelaIntensidadeParametroRequest request);
        Task<ResponseResult<TabelaIntensidadeParametro>> ObterPorSlugAsync(ObterPorSlugTabelaIntensidadeParametroRequest request);
        Task<ResponseResult<List<TabelaIntensidadeParametro>>> ObterListaAsync(ObterListaTabelaIntensidadeParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaIntensidadeParametroRequest request);
    }
}
