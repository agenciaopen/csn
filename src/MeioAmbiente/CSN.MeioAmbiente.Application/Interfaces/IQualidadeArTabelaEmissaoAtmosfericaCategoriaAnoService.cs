﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaCategoriaAnoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaCategoriaAnoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaCategoriaAnoRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaCategoriaAno>>> ObterPorCategoriaAsync(ObterPorCategoriaTabelaEmissaoAtmosfericaCategoriaAnoRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaCategoriaAnoRequest request);
    }
}
