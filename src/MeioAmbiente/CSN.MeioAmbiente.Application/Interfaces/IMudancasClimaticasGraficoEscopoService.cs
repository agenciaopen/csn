﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IMudancasClimaticasGraficoEscopoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoEscopoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoEscopoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoEscopoRequest request);
        Task<ResponseResult<GraficoEscopo>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoEscopoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEscopoRequest request);
    }
}
