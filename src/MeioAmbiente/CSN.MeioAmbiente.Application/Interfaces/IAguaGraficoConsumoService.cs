﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IAguaGraficoConsumoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoConsumoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoConsumoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoConsumoRequest request);
        Task<ResponseResult<Consumo>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoConsumoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoConsumoRequest request);
    }
}
