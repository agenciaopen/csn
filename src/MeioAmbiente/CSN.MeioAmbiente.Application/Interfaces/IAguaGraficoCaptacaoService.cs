﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IAguaGraficoCaptacaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoCaptacaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoCaptacaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoCaptacaoRequest request);
        Task<ResponseResult<Captacao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoCaptacaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoCaptacaoRequest request);
    }
}
