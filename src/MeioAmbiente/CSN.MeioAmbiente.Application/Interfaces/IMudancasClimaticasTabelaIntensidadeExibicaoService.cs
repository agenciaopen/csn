﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IMudancasClimaticasTabelaIntensidadeExibicaoService
    {
        Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaIntensidadeExibicaoRequest request);
        Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaIntensidadeExibicaoRequest request);
        Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaIntensidadeExibicaoRequest request);
        Task<ResponseResult<IEnumerable<TabelaIntensidadeExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaIntensidadeExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaIntensidadeExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaIntensidadeExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaIntensidadeExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaIntensidadeExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<TabelaIntensidadeExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaIntensidadeExibicaoAnoRequest request);
        Task<ResponseResult<TabelaIntensidadeExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaIntensidadeExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaIntensidadeExibicaoAnoRequest request);
    }
}
