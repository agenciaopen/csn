﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaParametroRequest request);
        Task<ResponseResult<EmissaoAtmosfericaParametro>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaParametroRequest request);
        Task<ResponseResult<EmissaoAtmosfericaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaEmissaoAtmosfericaParametroRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaParametro>>> ObterListaAsync(ObterListaTabelaEmissaoAtmosfericaParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaParametroRequest request);
    }
}
