﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Residuos;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IResiduosGraficoResiduosNaoPerigososService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoResiduosNaoPerigososRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoResiduosNaoPerigososRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoResiduosNaoPerigososRequest request);
        Task<ResponseResult<ResiduosNaoPerigosos>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoResiduosNaoPerigososRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoResiduosNaoPerigososRequest request);
    }
}
