﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<ResponseResult<ConsumoEnergiaOrganizacaoParametro>> ObterPorIdAsync(ObterPorIdTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<ResponseResult<ConsumoEnergiaOrganizacaoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<ResponseResult<List<ConsumoEnergiaOrganizacaoParametro>>> ObterListaAsync(ObterListaTabelaConsumoEnergiaOrganizacaoParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaConsumoEnergiaOrganizacaoParametroRequest request);
    }
}
