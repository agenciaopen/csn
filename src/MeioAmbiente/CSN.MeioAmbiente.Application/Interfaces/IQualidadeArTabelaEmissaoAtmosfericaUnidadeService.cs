﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaUnidadeService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaUnidadeRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaUnidadeRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaUnidadeRequest request);
        Task<ResponseResult<EmissaoAtmosfericaUnidade>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaUnidadeRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaUnidade>>> ObterPorNomeAsync(ObterPorNomeTabelaEmissaoAtmosfericaUnidadeRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaUnidadeRequest request);
    }
}
