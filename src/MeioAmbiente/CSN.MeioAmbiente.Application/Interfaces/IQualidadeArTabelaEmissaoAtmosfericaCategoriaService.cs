﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaCategoriaService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaCategoriaRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaCategoriaRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaCategoriaRequest request);
        Task<ResponseResult<EmissaoAtmosfericaCategoria>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaCategoriaRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaCategoria>>> ObterPorNomeAsync(ObterPorNomeTabelaEmissaoAtmosfericaCategoriaRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaCategoriaRequest request);
    }
}
