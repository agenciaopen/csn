﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IAguaGraficoDescarteService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoDescarteRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoDescarteRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoDescarteRequest request);
        Task<ResponseResult<Descarte>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoDescarteRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoDescarteRequest request);
    }
}
