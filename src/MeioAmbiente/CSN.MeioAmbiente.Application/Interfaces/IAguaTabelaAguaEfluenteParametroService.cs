﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IAguaTabelaAguaEfluenteParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaAguaEfluenteParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaAguaEfluenteParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaAguaEfluenteParametroRequest request);
        Task<ResponseResult<AguaEfluenteParametro>> ObterPorIdAsync(ObterPorIdTabelaAguaEfluenteParametroRequest request);
        Task<ResponseResult<AguaEfluenteParametro>> ObterPorSlugAsync(ObterPorSlugTabelaAguaEfluenteParametroRequest request);
        Task<ResponseResult<List<AguaEfluenteParametro>>> ObterListaAsync(ObterListaTabelaAguaEfluenteParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaAguaEfluenteParametroRequest request);

    }
}
