﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IEnergiaTabelaIntensidadeEnergeticaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaIntensidadeEnergeticaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaIntensidadeEnergeticaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaIntensidadeEnergeticaParametroRequest request);
        Task<ResponseResult<IntensidadeEnergeticaParametro>> ObterPorIdAsync(ObterPorIdTabelaIntensidadeEnergeticaParametroRequest request);
        Task<ResponseResult<IntensidadeEnergeticaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaIntensidadeEnergeticaParametroRequest request);
        Task<ResponseResult<List<IntensidadeEnergeticaParametro>>> ObterListaAsync(ObterListaTabelaIntensidadeEnergeticaParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaIntensidadeEnergeticaParametroRequest request);
    }
}
