﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IMudancasClimaticasTabelaEscopoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEscopoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEscopoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEscopoParametroRequest request);
        Task<ResponseResult<TabelaEscopoParametro>> ObterPorIdAsync(ObterPorIdTabelaEscopoParametroRequest request);
        Task<ResponseResult<TabelaEscopoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaEscopoParametroRequest request);
        Task<ResponseResult<List<TabelaEscopoParametro>>> ObterListaAsync(ObterListaTabelaEscopoParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEscopoParametroRequest request);
    }
}
