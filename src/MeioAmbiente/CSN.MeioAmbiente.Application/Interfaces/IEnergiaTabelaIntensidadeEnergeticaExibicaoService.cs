﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IEnergiaTabelaIntensidadeEnergeticaExibicaoService
    {
        Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaIntensidadeEnergeticaExibicaoRequest request);
        Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaIntensidadeEnergeticaExibicaoRequest request);
        Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaIntensidadeEnergeticaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<IntensidadeEnergeticaExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaIntensidadeEnergeticaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaIntensidadeEnergeticaExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<IntensidadeEnergeticaExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
        Task<ResponseResult<IntensidadeEnergeticaExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaIntensidadeEnergeticaExibicaoAnoRequest request);
    }
}
