﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IAguaTabelaAguaEfluenteExibicaoService
    {
        Task<ResponseResult> AdicionarFonteCaptacaoAsync(AdicionarTabelaAguaEfluenteExibicaoRequest request);
        Task<ResponseResult> AtualizarFonteCaptacaoAsync(AtualizarTabelaAguaEfluenteExibicaoRequest request);
        Task<ResponseResult> ExcluirFonteCaptacaoAsync(ExcluirTabelaAguaEfluenteExibicaoRequest request);
        Task<ResponseResult<IEnumerable<AguaEfluenteExibicao>>> ObterDadosFonteCaptacaoAsync(ObterDadosTabelaAguaEfluenteExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoFonteCaptacaoAsync(ExisteUltimoAtivoTabelaAguaEfluenteExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaAguaEfluenteExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaAguaEfluenteExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaAguaEfluenteExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<AguaEfluenteExibicaoAno>>> ObterAnosPorFonteCaptacaoAsync(ObterPorFonteCaptacaoTabelaAguaEfluenteExibicaoAnoRequest request);
        Task<ResponseResult<AguaEfluenteExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaAguaEfluenteExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaAguaEfluenteExibicaoAnoRequest request);
    }
}
