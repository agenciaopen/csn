﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
        Task<ResponseResult<EmissaoAtmosfericaFonteEmissora>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>> ObterPorNomeAsync(ObterPorNomeTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
        Task<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>> ObterPorCalegoriaAsync(ObterPorCategoriaTabelaEmissaoAtmosfericaFontesEmissorasRequest request);
        Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaFonteEmissoraRequest request);
    }
}
