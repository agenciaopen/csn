﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IMudancasClimaticasTabelaEscopoExibicaoService
    {
        Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaEscopoExibicaoRequest request);
        Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaEscopoExibicaoRequest request);
        Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaEscopoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<TabelaEscopoExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaEscopoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaEscopoExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaEscopoExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaEscopoExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaEscopoExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<TabelaEscopoExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaEscopoExibicaoAnoRequest request);
        Task<ResponseResult<TabelaEscopoExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaEscopoExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaEscopoExibicaoAnoRequest request);
    }
}
