﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Application.Response;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaExibicaoCategoriaRequest request);
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaExibicaoListaCategoriaRequest request);
        Task<ResponseResult> DesativarAsync(DesativarTabelaEmissaoAtmosfericaExibicaoRequest request);
        Task<ResponseResult<TabelaEmissaoAtmosfericaExibicaoResponse>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaEmissaoAtmosfericaExibicaoRequest request);
    }
}
