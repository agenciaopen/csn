﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Interfaces
{
    public interface IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService
    {
        Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request);
        Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request);
        Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaConsumoEnergiaOrganizacaoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoRequest request);

        Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
        Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
        Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
        Task<ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
        Task<ResponseResult<ConsumoEnergiaOrganizacaoExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
        Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request);
    }
}
