﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Application.Response;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class QualidadeArTabelaEmissaoAtmosfericaExibicaoService : IQualidadeArTabelaEmissaoAtmosfericaExibicaoService
    {
        private readonly IRepositoryAsync<EmissaoAtmosfericaExibicao> _repository;
        private readonly IQualidadeArTabelaEmissaoAtmosfericaExibicaoRepository _exibicaoRepository;

        private readonly IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository _categoriaRepository;
        private readonly IRepositoryAsync<EmissaoAtmosfericaUnidade> _unidadeRepository;

        public QualidadeArTabelaEmissaoAtmosfericaExibicaoService(IRepositoryAsync<EmissaoAtmosfericaExibicao> repository, IQualidadeArTabelaEmissaoAtmosfericaExibicaoRepository exibicaoRepository, IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository categoriaRepository, IRepositoryAsync<EmissaoAtmosfericaUnidade> unidadeRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
            _categoriaRepository = categoriaRepository;
            _unidadeRepository = unidadeRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaExibicaoCategoriaRequest request)
        {
            try
            {
                var categoria = await _categoriaRepository.ObterCategoriaPorId(request.CategoriaId);
                var unidade = await _unidadeRepository.FindByAsync(x => x.Id == request.UnidadeId);
                var categorias = new List<EmissaoAtmosfericaCategoria>();
                categorias.Add(categoria);
                var entity = new EmissaoAtmosfericaExibicao(unidade.Id, categorias, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaExibicaoListaCategoriaRequest request)
        {
            try
            {
                var categorias = await _categoriaRepository.ObterCategoriasPorIds(request.CategoriaIds);
                var unidade = await _unidadeRepository.FindByAsync(x => x.Id == request.UnidadeId);
                var entity = new EmissaoAtmosfericaExibicao(unidade.Id, categorias, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> DesativarAsync(DesativarTabelaEmissaoAtmosfericaExibicaoRequest request)
        {
            try
            {
                var entity = await _repository.FindByAsync(x => x.Id == request.Id);
                entity.Ativo = false;
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TabelaEmissaoAtmosfericaExibicaoResponse>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaEmissaoAtmosfericaExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterUltimoAtivo(request.Idioma);

            var response = new TabelaEmissaoAtmosfericaExibicaoResponse
            {
                Unidade = entity.Unidade.Nome,
                Nota = entity.Parametro.Nota,
                Subtitulo = entity.Parametro.Subtitulo,
                Categorias = entity.Categorias.Select(x => new TabelaEmissaoAtmosfericaCategoriaResponse
                {
                    FontesEmissoras = x.FontesEmissoras.Select(f => new TabelaEmissaoAtmosfericaFonteEmissoraResponse
                    {
                        Nome = f.Nome
                    }).ToList(),
                    Nome = x.Nome,
                    Anos = x.Anos.Select(a => new TabelaEmissaoAtmosfericaAnoResponse
                    {
                        Valor = a.Valor,
                        Ano = a.Ano
                    }).ToList()
                }).ToList(),
                Titulo = entity.Parametro.Titulo
            };

            var result = new ResponseResult<TabelaEmissaoAtmosfericaExibicaoResponse>(true, response);
            return result;
        }
    }
}
