﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class QualidadeArTabelaEmissaoAtmosfericaCategoriaService : IQualidadeArTabelaEmissaoAtmosfericaCategoriaService
    {
        private readonly IRepositoryAsync<EmissaoAtmosfericaCategoria> _repository;
        private readonly IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository _categoriaRepository;

        public QualidadeArTabelaEmissaoAtmosfericaCategoriaService(IRepositoryAsync<EmissaoAtmosfericaCategoria> repository, IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository categoriaRepository)
        {
            _repository = repository;
            _categoriaRepository = categoriaRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaCategoria(request.Nome, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaCategoria(request.Id, request.Nome, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<EmissaoAtmosfericaCategoria>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            var entity = await _categoriaRepository
                .ObterCategoriaPorId(request.Id);
            var result = new ResponseResult<EmissaoAtmosfericaCategoria>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<EmissaoAtmosfericaCategoria>>> ObterPorNomeAsync(ObterPorNomeTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            var entity = await _categoriaRepository.ObterCategorias(request.Nome, request.Ativo);

            var result = new ResponseResult<List<EmissaoAtmosfericaCategoria>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaCategoriaRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Nome == request.Nome);
            return entity;
        }
    }
}
