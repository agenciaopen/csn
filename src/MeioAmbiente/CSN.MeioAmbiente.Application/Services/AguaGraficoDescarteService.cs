﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Grafico;

namespace CSN.MeioAmbiente.Application.Services
{
    public class AguaGraficoDescarteService : IAguaGraficoDescarteService
    {
        private readonly ICsnRepositoryAsync<Descarte> _csnRepository;

        public AguaGraficoDescarteService(ICsnRepositoryAsync<Descarte> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoDescarteRequest request)
        {
            try
            {
                var entity = new Descarte(request.Titulo, request.Subtitulo, request.Mineracao, request.Siderurgia,
                    request.Cimento, request.Logistica, request.OutrasMineracoes, request.InformacaoUm, request.InformacaoDois, request.Idioma,
                    true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoDescarteRequest request)
        {
            try
            {
                var entity = new Descarte(request.Id, request.Titulo, request.Subtitulo, request.Mineracao, request.Siderurgia,
                    request.Cimento, request.Logistica, request.OutrasMineracoes, request.InformacaoUm, request.InformacaoDois, request.Idioma,
                    request.Ativo);

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoDescarteRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<Descarte>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoDescarteRequest request)
        {
            var result = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<Descarte>(true, result);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoDescarteRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
