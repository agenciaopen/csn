﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Services
{
    public class EnergiaTabelaIntensidadeEnergeticaParametroService : IEnergiaTabelaIntensidadeEnergeticaParametroService
    {
        private readonly IRepositoryAsync<IntensidadeEnergeticaParametro> _repository;

        public EnergiaTabelaIntensidadeEnergeticaParametroService(IRepositoryAsync<IntensidadeEnergeticaParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaIntensidadeEnergeticaParametroRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaParametro(request.Titulo, request.Subtitulo, request.Sigla, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaIntensidadeEnergeticaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaIntensidadeEnergeticaParametroRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaParametro(request.Id, request.Titulo, request.Subtitulo, request.Sigla, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IntensidadeEnergeticaParametro>> ObterPorIdAsync(ObterPorIdTabelaIntensidadeEnergeticaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<IntensidadeEnergeticaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IntensidadeEnergeticaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaIntensidadeEnergeticaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<IntensidadeEnergeticaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<IntensidadeEnergeticaParametro>>> ObterListaAsync(ObterListaTabelaIntensidadeEnergeticaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<IntensidadeEnergeticaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<IntensidadeEnergeticaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaIntensidadeEnergeticaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }

}
