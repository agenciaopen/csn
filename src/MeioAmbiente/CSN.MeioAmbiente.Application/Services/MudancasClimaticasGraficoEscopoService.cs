﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico;

namespace CSN.MeioAmbiente.Application.Services
{
    public class MudancasClimaticasGraficoEscopoService : IMudancasClimaticasGraficoEscopoService
    {
        private readonly ICsnRepositoryAsync<GraficoEscopo> _csnRepository;

        public MudancasClimaticasGraficoEscopoService(ICsnRepositoryAsync<GraficoEscopo> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoEscopoRequest request)
        {
            try
            {
                var entity = new GraficoEscopo(request.Titulo, request.Subtitulo, request.Sigla, request.Mineracao, request.Siderurgia, request.Cimento, request.Logistica, request.Nota, request.Idioma, true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoEscopoRequest request)
        {
            try
            {
                var entity = new GraficoEscopo
                {
                    Id = request.Id,
                    Idioma = request.Idioma,
                    Ativo = request.Ativo,
                    Cadastro = DateTime.Now,
                    Titulo = request.Titulo,
                    Sigla = request.Sigla,
                    Subtitulo = request.Subtitulo,
                    Siderurgia = request.Siderurgia,
                    Cimento = request.Cimento,
                    Logistica = request.Logistica,
                    Mineracao = request.Cimento,
                    Nota = request.Nota
                };

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoEscopoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<GraficoEscopo>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoEscopoRequest request)
        {
            var result = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<GraficoEscopo>(true, result);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoEscopoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
