﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class MudancasClimaticasTabelaIntensidadeExibicaoService : IMudancasClimaticasTabelaIntensidadeExibicaoService
    {
        private readonly ICsnRepositoryAsync<TabelaIntensidadeExibicao> _csnDescricaoRepository;
        private readonly IMudancasClimaticasTabelaIntensidadeExibicaoRepository _descricaoRepository;
        private readonly ICsnRepositoryAsync<TabelaIntensidadeExibicaoAno> _csnAnoRepository;

        public MudancasClimaticasTabelaIntensidadeExibicaoService(ICsnRepositoryAsync<TabelaIntensidadeExibicao> csnDescricaoRepository, IMudancasClimaticasTabelaIntensidadeExibicaoRepository descricaoRepository, ICsnRepositoryAsync<TabelaIntensidadeExibicaoAno> csnAnoRepository)
        {
            _csnDescricaoRepository = csnDescricaoRepository;
            _descricaoRepository = descricaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new TabelaIntensidadeExibicao(request.Descricao, request.Idioma);
                var result = await _csnDescricaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaIntensidadeExibicaoRequest request)
        {
            try
            {
                var entity = new TabelaIntensidadeExibicao(request.Id, request.Descricao, request.Idioma, request.Ativo);
                var result = await _csnDescricaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaIntensidadeExibicaoRequest request)
        {
            try
            {
                var result = await _csnDescricaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<TabelaIntensidadeExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaIntensidadeExibicaoRequest request)
        {
            var entity = await _descricaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<TabelaIntensidadeExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaIntensidadeExibicaoRequest request)
        {
            return await _csnDescricaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaIntensidadeExibicaoAnoRequest request)
        {
            try
            {
                var entity = new TabelaIntensidadeExibicaoAno(request.DescricaoId, request.Ano, request.Valor, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaIntensidadeExibicaoAnoRequest request)
        {
            try
            {
                var entity = new TabelaIntensidadeExibicaoAno(request.Id, request.DescricaoId, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaIntensidadeExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<TabelaIntensidadeExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaIntensidadeExibicaoAnoRequest request)
        {
            var entity = await _descricaoRepository.ObterAnosPorDescricaoAsync(request.DescricaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<TabelaIntensidadeExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<TabelaIntensidadeExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaIntensidadeExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x => x.DescricaoId == request.DescricaoId, o => o.Cadastro);
            return new ResponseResult<TabelaIntensidadeExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaIntensidadeExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
