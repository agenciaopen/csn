﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Application.Services
{
    public class MudancasClimaticasTabelaEscopoParametroService : IMudancasClimaticasTabelaEscopoParametroService
    {
        private readonly IRepositoryAsync<TabelaEscopoParametro> _repository;

        public MudancasClimaticasTabelaEscopoParametroService(IRepositoryAsync<TabelaEscopoParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEscopoParametroRequest request)
        {
            try
            {
                var entity = new TabelaEscopoParametro(request.Titulo, request.QuantidadeAnos, request.Nota, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaEscopoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaEscopoParametroRequest request)
        {
            try
            {
                var entity = new TabelaEscopoParametro(request.Id, request.Titulo, request.QuantidadeAnos, request.Nota, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TabelaEscopoParametro>> ObterPorIdAsync(ObterPorIdTabelaEscopoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<TabelaEscopoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<TabelaEscopoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaEscopoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<TabelaEscopoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<TabelaEscopoParametro>>> ObterListaAsync(ObterListaTabelaEscopoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<TabelaEscopoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<TabelaEscopoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaEscopoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
