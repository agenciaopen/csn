﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;

namespace CSN.MeioAmbiente.Application.Services
{
    public class AguaTabelaAguaEfluenteParametroService : IAguaTabelaAguaEfluenteParametroService
    {
        private readonly IRepositoryAsync<AguaEfluenteParametro> _repository;

        public AguaTabelaAguaEfluenteParametroService(IRepositoryAsync<AguaEfluenteParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaAguaEfluenteParametroRequest request)
        {
            try
            {
                var entity = new AguaEfluenteParametro(request.Titulo, request.EmMegaLitrosMl, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaAguaEfluenteParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaAguaEfluenteParametroRequest request)
        {
            try
            {
                var entity = new AguaEfluenteParametro(request.Id, request.Titulo, request.EmMegaLitrosMl, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<AguaEfluenteParametro>> ObterPorIdAsync(ObterPorIdTabelaAguaEfluenteParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<AguaEfluenteParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<AguaEfluenteParametro>> ObterPorSlugAsync(ObterPorSlugTabelaAguaEfluenteParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<AguaEfluenteParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<AguaEfluenteParametro>>> ObterListaAsync(ObterListaTabelaAguaEfluenteParametroRequest request)
        {
            var predicate = PredicateBuilder.True<AguaEfluenteParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<AguaEfluenteParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaAguaEfluenteParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
