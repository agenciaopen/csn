﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Residuos;
using CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico;

namespace CSN.MeioAmbiente.Application.Services
{
    public class ResiduosGraficoResiduosPerigososService : IResiduosGraficoResiduosPerigososService
    {
        private readonly ICsnRepositoryAsync<ResiduosPerigosos> _csnRepository;

        public ResiduosGraficoResiduosPerigososService(ICsnRepositoryAsync<ResiduosPerigosos> csnRepository)
        {
            _csnRepository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoResiduosPerigososRequest request)
        {
            try
            {
                var entity = new ResiduosPerigosos(request.Titulo, request.Sigla, request.Mineracao, request.OutrasMineracoes, request.Siderurgia, request.Cimento, request.Logistica, request.Total, request.Idioma, true);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoResiduosPerigososRequest request)
        {
            try
            {
                var entity = new ResiduosPerigosos
                {
                    Id = request.Id,
                    Idioma = request.Idioma,
                    Ativo = request.Ativo,
                    Cadastro = DateTime.Now,
                    Titulo = request.Titulo,
                    Cimento = request.Cimento,
                    Logistica = request.Logistica,
                    Mineracao = request.Mineracao,
                    OutrasMineracoes = request.OutrasMineracoes,
                    Siderurgia = request.Siderurgia,
                    Sigla = request.Sigla,
                    Total = request.Total
                };

                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoResiduosPerigososRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ResiduosPerigosos>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoResiduosPerigososRequest request)
        {
            var result = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<ResiduosPerigosos>(true, result);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoResiduosPerigososRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
