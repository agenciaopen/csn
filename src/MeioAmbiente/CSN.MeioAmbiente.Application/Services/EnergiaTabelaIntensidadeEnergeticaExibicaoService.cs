﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class EnergiaTabelaIntensidadeEnergeticaExibicaoService : IEnergiaTabelaIntensidadeEnergeticaExibicaoService
    {
        private readonly ICsnRepositoryAsync<IntensidadeEnergeticaExibicao> _csnDescricaoRepository;
        private readonly IEnergiaTabelaIntensidadeEnergeticaExibicaoRepository _descricaoRepository;
        private readonly ICsnRepositoryAsync<IntensidadeEnergeticaExibicaoAno> _csnAnoRepository;

        public EnergiaTabelaIntensidadeEnergeticaExibicaoService(ICsnRepositoryAsync<IntensidadeEnergeticaExibicao> csnDescricaoRepository, IEnergiaTabelaIntensidadeEnergeticaExibicaoRepository descricaoRepository, ICsnRepositoryAsync<IntensidadeEnergeticaExibicaoAno> csnAnoRepository)
        {
            _csnDescricaoRepository = csnDescricaoRepository;
            _descricaoRepository = descricaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaIntensidadeEnergeticaExibicaoRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaExibicao(request.Descricao, request.Unidade, request.Idioma);
                var result = await _csnDescricaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaIntensidadeEnergeticaExibicaoRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaExibicao(request.Id, request.Descricao, request.Unidade, request.Idioma, request.Ativo);
                var result = await _csnDescricaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaIntensidadeEnergeticaExibicaoRequest request)
        {
            try
            {
                var result = await _csnDescricaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<IntensidadeEnergeticaExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaIntensidadeEnergeticaExibicaoRequest request)
        {
            var entity = await _descricaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<IntensidadeEnergeticaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaIntensidadeEnergeticaExibicaoRequest request)
        {
            return await _csnDescricaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaExibicaoAno(request.DescricaoId, request.Ano, request.Valor, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            try
            {
                var entity = new IntensidadeEnergeticaExibicaoAno(request.Id, request.DescricaoId, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<IntensidadeEnergeticaExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            var entity = await _descricaoRepository.ObterAnosPorDescricaoAsync(request.DescricaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<IntensidadeEnergeticaExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<IntensidadeEnergeticaExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x => x.DescricaoId == request.DescricaoId, o => o.Cadastro);
            return new ResponseResult<IntensidadeEnergeticaExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaIntensidadeEnergeticaExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
