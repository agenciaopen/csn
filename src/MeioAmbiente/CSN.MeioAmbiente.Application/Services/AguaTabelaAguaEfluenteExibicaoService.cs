﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Agua;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class AguaTabelaAguaEfluenteExibicaoService : IAguaTabelaAguaEfluenteExibicaoService
    {
        private readonly ICsnRepositoryAsync<AguaEfluenteExibicao> _csnFonteCaptacaoRepository;
        private readonly IAguaTabelaAguaEfluenteExibicaoRepository _exibicaoRepository;
        private readonly ICsnRepositoryAsync<AguaEfluenteExibicaoAno> _csnAnoRepository;

        public AguaTabelaAguaEfluenteExibicaoService(ICsnRepositoryAsync<AguaEfluenteExibicao> csnFonteCaptacaoRepository, IAguaTabelaAguaEfluenteExibicaoRepository exibicaoRepository, ICsnRepositoryAsync<AguaEfluenteExibicaoAno> csnAnoRepository)
        {
            _csnFonteCaptacaoRepository = csnFonteCaptacaoRepository;
            _exibicaoRepository = exibicaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarFonteCaptacaoAsync(AdicionarTabelaAguaEfluenteExibicaoRequest request)
        {
            try
            {
                var entity = new AguaEfluenteExibicao(request.FonteCaptacao, request.Idioma);
                var result = await _csnFonteCaptacaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarFonteCaptacaoAsync(AtualizarTabelaAguaEfluenteExibicaoRequest request)
        {
            try
            {
                var entity = new AguaEfluenteExibicao(request.Id, request.FonteCaptacao, request.Idioma, request.Ativo);
                var result = await _csnFonteCaptacaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirFonteCaptacaoAsync(ExcluirTabelaAguaEfluenteExibicaoRequest request)
        {
            try
            {
                var result = await _csnFonteCaptacaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<AguaEfluenteExibicao>>> ObterDadosFonteCaptacaoAsync(ObterDadosTabelaAguaEfluenteExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<AguaEfluenteExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoFonteCaptacaoAsync(ExisteUltimoAtivoTabelaAguaEfluenteExibicaoRequest request)
        {
            return await _csnFonteCaptacaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            try
            {
                var entity = new AguaEfluenteExibicaoAno(request.FonteCaptacaoId, request.Ano, request.TodasAreas, request.AreasComEstresseHidrico, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            try
            {
                var entity = new AguaEfluenteExibicaoAno(request.Id, request.FonteCaptacaoId, request.Ano, request.TodasAreas, request.AreasComEstresseHidrico, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<AguaEfluenteExibicaoAno>>> ObterAnosPorFonteCaptacaoAsync(ObterPorFonteCaptacaoTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            var entity = await _exibicaoRepository.ObterAnosPorFonteCaptacaoAsync(request.FonteCaptacaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<AguaEfluenteExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<AguaEfluenteExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x => x.FonteCaptacaoId == request.FonteCaptacaoId, o => o.Cadastro);
            return new ResponseResult<AguaEfluenteExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaAguaEfluenteExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
