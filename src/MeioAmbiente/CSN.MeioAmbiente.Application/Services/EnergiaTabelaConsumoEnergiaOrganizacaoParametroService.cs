﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Application.Services
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoParametroService : IEnergiaTabelaConsumoEnergiaOrganizacaoParametroService
    {
        private readonly IRepositoryAsync<ConsumoEnergiaOrganizacaoParametro> _repository;

        public EnergiaTabelaConsumoEnergiaOrganizacaoParametroService(IRepositoryAsync<ConsumoEnergiaOrganizacaoParametro> csnRepository)
        {
            _repository = csnRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoParametro(request.Titulo, request.Sigla, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoParametro(request.Id, request.Titulo, request.Sigla, request.AreaNegocio, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ConsumoEnergiaOrganizacaoParametro>> ObterPorIdAsync(ObterPorIdTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<ConsumoEnergiaOrganizacaoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<ConsumoEnergiaOrganizacaoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<ConsumoEnergiaOrganizacaoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<ConsumoEnergiaOrganizacaoParametro>>> ObterListaAsync(ObterListaTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<ConsumoEnergiaOrganizacaoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<ConsumoEnergiaOrganizacaoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaConsumoEnergiaOrganizacaoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }

}
