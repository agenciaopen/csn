﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.MudancasClimaticas;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class MudancasClimaticasTabelaEscopoExibicaoService : IMudancasClimaticasTabelaEscopoExibicaoService
    {
        private readonly ICsnRepositoryAsync<TabelaEscopoExibicao> _csnDescricaoRepository;
        private readonly IMudancasClimaticasTabelaEscopoExibicaoRepository _descricaoRepository;
        private readonly ICsnRepositoryAsync<TabelaEscopoExibicaoAno> _csnAnoRepository;

        public MudancasClimaticasTabelaEscopoExibicaoService(ICsnRepositoryAsync<TabelaEscopoExibicao> csnDescricaoRepository, IMudancasClimaticasTabelaEscopoExibicaoRepository descricaoRepository, ICsnRepositoryAsync<TabelaEscopoExibicaoAno> csnAnoRepository)
        {
            _csnDescricaoRepository = csnDescricaoRepository;
            _descricaoRepository = descricaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaEscopoExibicaoRequest request)
        {
            try
            {
                var entity = new TabelaEscopoExibicao(request.Descricao, request.Idioma);
                var result = await _csnDescricaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaEscopoExibicaoRequest request)
        {
            try
            {
                var entity = new TabelaEscopoExibicao(request.Id, request.Descricao, request.Idioma, request.Ativo);
                var result = await _csnDescricaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaEscopoExibicaoRequest request)
        {
            try
            {
                var result = await _csnDescricaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<TabelaEscopoExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaEscopoExibicaoRequest request)
        {
            var entity = await _descricaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<TabelaEscopoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaEscopoExibicaoRequest request)
        {
            return await _csnDescricaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaEscopoExibicaoAnoRequest request)
        {
            try
            {
                var entity = new TabelaEscopoExibicaoAno(request.DescricaoId, request.Ano, request.Valor, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaEscopoExibicaoAnoRequest request)
        {
            try
            {
                var entity = new TabelaEscopoExibicaoAno(request.Id, request.DescricaoId, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaEscopoExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<TabelaEscopoExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaEscopoExibicaoAnoRequest request)
        {
            var entity = await _descricaoRepository.ObterAnosPorDescricaoAsync(request.DescricaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<TabelaEscopoExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<TabelaEscopoExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaEscopoExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x => x.DescricaoId == request.DescricaoId, o => o.Cadastro);
            return new ResponseResult<TabelaEscopoExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaEscopoExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
