﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Services
{
    public class QualidadeArTabelaEmissaoAtmosfericaParametroService : IQualidadeArTabelaEmissaoAtmosfericaParametroService
    {
        private readonly IRepositoryAsync<EmissaoAtmosfericaParametro> _repository;

        public QualidadeArTabelaEmissaoAtmosfericaParametroService(IRepositoryAsync<EmissaoAtmosfericaParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaParametroRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaParametro(request.Titulo, request.Subtitulo, request.Nota, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaParametroRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaParametro(request.Id, request.Titulo, request.Subtitulo, request.Nota, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<EmissaoAtmosfericaParametro>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<EmissaoAtmosfericaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<EmissaoAtmosfericaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaEmissaoAtmosfericaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<EmissaoAtmosfericaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<EmissaoAtmosfericaParametro>>> ObterListaAsync(ObterListaTabelaEmissaoAtmosfericaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<EmissaoAtmosfericaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<EmissaoAtmosfericaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
