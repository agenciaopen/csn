﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService : IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService
    {
        private readonly IRepositoryAsync<EmissaoAtmosfericaFonteEmissora> _repository;
        private readonly IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository _fonteEmissoraRepository;

        public QualidadeArTabelaEmissaoAtmosfericaFonteEmissoraService(IRepositoryAsync<EmissaoAtmosfericaFonteEmissora> repository, IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository fonteEmissoraRepository)
        {
            _repository = repository;
            _fonteEmissoraRepository = fonteEmissoraRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaFonteEmissora(request.Nome, request.CategoriaId, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaFonteEmissora(request.Id, request.Nome, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<EmissaoAtmosfericaFonteEmissora>> ObterPorIdAsync(ObterPorIdTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            var entity = await _fonteEmissoraRepository
                .ObterFontesEmissorasPorId(request.Id);
            var result = new ResponseResult<EmissaoAtmosfericaFonteEmissora>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>> ObterPorNomeAsync(ObterPorNomeTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            var entity = await _fonteEmissoraRepository
                .ObterFontesEmissorasPorNome(request.Nome, request.Ativo);
            var result = new ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>> ObterPorCalegoriaAsync(ObterPorCategoriaTabelaEmissaoAtmosfericaFontesEmissorasRequest request)
        {
            var entity = await _fonteEmissoraRepository
                .ObterFontesEmissorasPorCategoria(request.CategoriaId, request.Ativo);
            var result = new ResponseResult<List<EmissaoAtmosfericaFonteEmissora>>(true, entity);
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaFonteEmissoraRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Nome == request.Nome);
            return entity;
        }
    }
}
