﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.Energia;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService : IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService
    {
        private readonly ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicao> _csnDescricaoRepository;
        private readonly IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoRepository _descricaoRepository;
        private readonly ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicaoAno> _csnAnoRepository;

        public EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoService(ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicao> csnDescricaoRepository, IEnergiaTabelaConsumoEnergiaOrganizacaoExibicaoRepository descricaoRepository, ICsnRepositoryAsync<ConsumoEnergiaOrganizacaoExibicaoAno> csnAnoRepository)
        {
            _csnDescricaoRepository = csnDescricaoRepository;
            _descricaoRepository = descricaoRepository;
            _csnAnoRepository = csnAnoRepository;
        }

        public async Task<ResponseResult> AdicionarDescricaoAsync(AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoExibicao(request.Descricao, request.Unidade, request.Idioma);
                var result = await _csnDescricaoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarDescricaoAsync(AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoExibicao(request.Id, request.Descricao, request.Unidade, request.Idioma, request.Ativo);
                var result = await _csnDescricaoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirDescricaoAsync(ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoRequest request)
        {
            try
            {
                var result = await _csnDescricaoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicao>>> ObterDadosDescricaoAsync(ObterDadosTabelaConsumoEnergiaOrganizacaoExibicaoRequest request)
        {
            var entity = await _descricaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoDescricaoAsync(ExisteUltimoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoRequest request)
        {
            return await _csnDescricaoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

        public async Task<ResponseResult> AdicionarAnoAsync(AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoExibicaoAno(request.DescricaoId, request.Ano, request.Valor, request.Idioma);
                var result = await _csnAnoRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAnoAsync(AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            try
            {
                var entity = new ConsumoEnergiaOrganizacaoExibicaoAno(request.Id, request.DescricaoId, request.Ano, request.Valor, request.Idioma, request.Ativo);
                var result = await _csnAnoRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAnoAsync(ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            try
            {
                var result = await _csnAnoRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicaoAno>>> ObterAnosPorDescricaoAsync(ObterPorDescricaoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            var entity = await _descricaoRepository.ObterAnosPorDescricaoAsync(request.DescricaoId, request.Idioma, request.Ativo);
            return new ResponseResult<IEnumerable<ConsumoEnergiaOrganizacaoExibicaoAno>>(true, entity);
        }

        public async Task<ResponseResult<ConsumoEnergiaOrganizacaoExibicaoAno>> ObterAnoPorIdAsync(ObterPorIdTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            var entity = await _csnAnoRepository.ObterAsync(x => x.DescricaoId == request.DescricaoId, o => o.Cadastro);
            return new ResponseResult<ConsumoEnergiaOrganizacaoExibicaoAno>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAnoAsync(ExisteAnoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest request)
        {
            return await _csnAnoRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
