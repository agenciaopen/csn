﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.MeioAmbiente.Application.Interfaces;
using CSN.MeioAmbiente.Application.Requests.QualidadeAr;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;
using CSN.MeioAmbiente.Domain.Interfaces;

namespace CSN.MeioAmbiente.Application.Services
{
    public class QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService : IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService
    {
        private readonly IRepositoryAsync<EmissaoAtmosfericaCategoriaAno> _repository;
        private readonly IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository _anoRepository;

        public QualidadeArTabelaEmissaoAtmosfericaCategoriaAnoService(IRepositoryAsync<EmissaoAtmosfericaCategoriaAno> repository, IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository anoRepository)
        {
            _repository = repository;
            _anoRepository = anoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaEmissaoAtmosfericaCategoriaAnoRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaCategoriaAno(request.Ano, request.Valor, request.CategoriaId, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaEmissaoAtmosfericaCategoriaAnoRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaEmissaoAtmosfericaCategoriaAnoRequest request)
        {
            try
            {
                var entity = new EmissaoAtmosfericaCategoriaAno(request.Id, request.Ano, request.Valor, request.CategoriaId, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<List<EmissaoAtmosfericaCategoriaAno>>> ObterPorCategoriaAsync(ObterPorCategoriaTabelaEmissaoAtmosfericaCategoriaAnoRequest request)
        {
            var entity = await _anoRepository
                .ObterAnosPorCategoria(request.CategoriaId);
            var result = new ResponseResult<List<EmissaoAtmosfericaCategoriaAno>> (true, entity);
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaEmissaoAtmosfericaCategoriaAnoRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Ano == request.Ano && x.CategoriaId == request.CategoriaId);
            return entity;
        }
    }
}
