﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Application.Response
{
    public class TabelaEmissaoAtmosfericaExibicaoResponse
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Nota { get; set; }
        public string Unidade { get; set; }
        public List<TabelaEmissaoAtmosfericaCategoriaResponse> Categorias { get; set; }
    }

    public class TabelaEmissaoAtmosfericaCategoriaResponse
    {
        public string Nome { get; set; }
        public List<TabelaEmissaoAtmosfericaFonteEmissoraResponse> FontesEmissoras { get; set; }
        public List<TabelaEmissaoAtmosfericaAnoResponse> Anos { get; set; }
    }

    public class TabelaEmissaoAtmosfericaFonteEmissoraResponse
    {
        public string Nome { get; set; }
    }

    public class TabelaEmissaoAtmosfericaAnoResponse
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
    }
}
