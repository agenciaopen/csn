﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Residuos
{
    public class AdicionarGraficoResiduosPerigososRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double OutrasMineracoes { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoResiduosPerigososRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double OutrasMineracoes { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoResiduosPerigososRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoResiduosPerigososRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoResiduosPerigososRequest
    {
        public Idioma Idioma { get; set; }
    }

}
