﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Agua
{
    public class AdicionarGraficoDescarteRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoDescarteRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoDescarteRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoDescarteRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoDescarteRequest
    {
        public Idioma Idioma { get; set; }
    }
}
