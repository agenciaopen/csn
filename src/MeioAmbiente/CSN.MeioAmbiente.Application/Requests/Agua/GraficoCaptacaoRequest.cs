﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Agua
{
    public class AdicionarGraficoCaptacaoRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoCaptacaoRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoCaptacaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoCaptacaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoCaptacaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
