﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Agua
{
    public class AdicionarTabelaAguaEfluenteParametroRequest
    {
        public string Titulo { get; set; }
        public string EmMegaLitrosMl { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaAguaEfluenteParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string EmMegaLitrosMl { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaAguaEfluenteParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaAguaEfluenteParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaAguaEfluenteParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaAguaEfluenteParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaAguaEfluenteParametroRequest
    {
        public string Slug { get; set; }
    }

}
