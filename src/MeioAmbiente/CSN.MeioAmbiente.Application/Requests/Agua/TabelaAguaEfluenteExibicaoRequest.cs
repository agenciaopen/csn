﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Agua
{
    public class AdicionarTabelaAguaEfluenteExibicaoRequest
    {
        public string FonteCaptacao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaAguaEfluenteExibicaoRequest
    {
        public Guid Id { get; set; }
        public string FonteCaptacao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaAguaEfluenteExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosTabelaAguaEfluenteExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaAguaEfluenteExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class AdicionarTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid FonteCaptacaoId { get; set; }
        public int Ano { get; set; }
        public double TodasAreas { get; set; }
        public double AreasComEstresseHidrico { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid FonteCaptacaoId { get; set; }
        public int Ano { get; set; }
        public double TodasAreas { get; set; }
        public double AreasComEstresseHidrico { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorFonteCaptacaoTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid FonteCaptacaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid FonteCaptacaoId { get; set; }
    }

    public class ExisteAnoAtivoTabelaAguaEfluenteExibicaoAnoRequest
    {
        public Guid FonteCaptacaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
