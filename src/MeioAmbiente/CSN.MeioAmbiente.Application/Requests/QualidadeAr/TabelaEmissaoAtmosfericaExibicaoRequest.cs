﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaExibicaoCategoriaRequest
    {
        public Guid UnidadeId { get; set; }
        public Guid CategoriaId { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AdicionarTabelaEmissaoAtmosfericaExibicaoListaCategoriaRequest
    {
        public Guid UnidadeId { get; set; }
        public List<Guid> CategoriaIds { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class ObterUltimoAtivoTabelaEmissaoAtmosfericaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaExibicaoRequest
    {
        public Guid id { get; set; }
    }

    public class DesativarTabelaEmissaoAtmosfericaExibicaoRequest
    {
        public Guid Id { get; set; }
    }
}
