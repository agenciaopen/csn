﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public string Nome { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorNomeTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaEmissaoAtmosfericaCategoriaRequest
    {
        public string Nome { get; set; }
    }
}
