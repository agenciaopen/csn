﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaCategoriaAnoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Guid CategoriaId { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEmissaoAtmosfericaCategoriaAnoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Guid CategoriaId { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaCategoriaAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorCategoriaTabelaEmissaoAtmosfericaCategoriaAnoRequest
    {
        public Guid CategoriaId { get; set; }
    }

    public class ExisteTabelaEmissaoAtmosfericaCategoriaAnoRequest
    {
        public int Ano { get; set; }
        public Guid CategoriaId { get; set; }
    }
}
