﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public string Nome { get; set; }
        public Guid CategoriaId { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorNomeTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorCategoriaTabelaEmissaoAtmosfericaFontesEmissorasRequest
    {
        public Guid CategoriaId { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaEmissaoAtmosfericaFonteEmissoraRequest
    {
        public string Nome { get; set; }
    }
}
