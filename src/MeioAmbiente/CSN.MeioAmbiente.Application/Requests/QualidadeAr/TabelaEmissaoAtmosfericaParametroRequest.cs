﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaParametroRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEmissaoAtmosfericaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaEmissaoAtmosfericaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaEmissaoAtmosfericaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaEmissaoAtmosfericaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaEmissaoAtmosfericaParametroRequest
    {
        public string Slug { get; set; }
    }

}
