﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.QualidadeAr
{
    public class AdicionarTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public string Nome { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorNomeTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaEmissaoAtmosfericaUnidadeRequest
    {
        public string Nome { get; set; }
    }
}
