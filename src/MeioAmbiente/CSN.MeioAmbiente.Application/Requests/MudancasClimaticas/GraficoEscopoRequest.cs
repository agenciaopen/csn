﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class AdicionarGraficoEscopoRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoEscopoRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoEscopoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoEscopoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoEscopoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
