﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class AdicionarTabelaEscopoExibicaoRequest
    {
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEscopoExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEscopoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosTabelaEscopoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaEscopoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
