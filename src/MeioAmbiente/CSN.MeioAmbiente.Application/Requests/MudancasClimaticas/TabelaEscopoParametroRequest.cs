﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class AdicionarTabelaEscopoParametroRequest
    {
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEscopoParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEscopoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaEscopoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaEscopoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaEscopoParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaEscopoParametroRequest
    {
        public string Slug { get; set; }
    }
}
