﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class AdicionarTabelaIntensidadeExibicaoRequest
    {
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosTabelaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaIntensidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
