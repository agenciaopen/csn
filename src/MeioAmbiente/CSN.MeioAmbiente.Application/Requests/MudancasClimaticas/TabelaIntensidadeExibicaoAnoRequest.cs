﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class TabelaIntensidadeExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class AdicionarTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorDescricaoTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
    }

    public class ExisteAnoAtivoTabelaIntensidadeExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
