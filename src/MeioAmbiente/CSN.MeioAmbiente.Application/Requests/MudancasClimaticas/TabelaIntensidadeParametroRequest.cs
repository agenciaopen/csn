﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class AdicionarTabelaIntensidadeParametroRequest
    {
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Nota { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaIntensidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaIntensidadeParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaIntensidadeParametroRequest
    {
        public string Slug { get; set; }
    }
}
