﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.MudancasClimaticas
{
    public class TabelaEscopoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class AdicionarTabelaEscopoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaEscopoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaEscopoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorDescricaoTabelaEscopoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdTabelaEscopoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
    }

    public class ExisteAnoAtivoTabelaEscopoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
