﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoRequest
    {
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosTabelaConsumoEnergiaOrganizacaoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
