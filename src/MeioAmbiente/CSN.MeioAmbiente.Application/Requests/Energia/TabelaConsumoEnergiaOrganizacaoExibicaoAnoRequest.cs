﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class TabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class AdicionarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorDescricaoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
    }

    public class ExisteAnoAtivoTabelaConsumoEnergiaOrganizacaoExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
