﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class AdicionarTabelaIntensidadeEnergeticaExibicaoRequest
    {
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeEnergeticaExibicaoRequest
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeEnergeticaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterDadosTabelaIntensidadeEnergeticaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaIntensidadeEnergeticaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
