﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class AdicionarTabelaIntensidadeEnergeticaParametroRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeEnergeticaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeEnergeticaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaIntensidadeEnergeticaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaIntensidadeEnergeticaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaIntensidadeEnergeticaParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaIntensidadeEnergeticaParametroRequest
    {
        public string Slug { get; set; }
    }
}
