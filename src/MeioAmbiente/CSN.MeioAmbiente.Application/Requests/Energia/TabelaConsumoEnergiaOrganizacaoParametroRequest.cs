﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class AdicionarTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public string? Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaConsumoEnergiaOrganizacaoParametroRequest
    {
        public string Slug { get; set; }
    }
}
