﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Application.Requests.Energia
{
    public class TabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class AdicionarTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorDescricaoTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public Idioma Idioma { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ObterPorIdTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
    }

    public class ExisteAnoAtivoTabelaIntensidadeEnergeticaExibicaoAnoRequest
    {
        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public Idioma Idioma { get; set; }
    }
}
