﻿namespace CSN.MeioAmbiente.Domain.ValueObjects
{
    public static class Referencia
    {
        public static Guid Valor(this ReferenciaTipo me)
        {
            switch (me)
            {
                case ReferenciaTipo.AguaGraficoConsumo:
                    return Guid.Parse("effc0ab6-5845-4d24-80cd-a1eccb86076e");
                case ReferenciaTipo.AguaGraficoDescarte:
                    return Guid.Parse("3e64849f-94bf-4ee3-9181-40a3092d3357");
                case ReferenciaTipo.AguaGraficoCaptacao:
                    return Guid.Parse("ddad8bd2-f49c-4dcc-a479-c0b24833ac55");
                case ReferenciaTipo.AguaTabelaAguaEfluenteParametro:
                    return Guid.Parse("203c29d6-5867-48b1-9f03-4d5304589b60");
                case ReferenciaTipo.AguaTabelaAguaEfluenteExibicao:
                    return Guid.Parse("cfa7427d-b050-4457-b317-3abc99d83856");
                case ReferenciaTipo.AguaTabelaAguaEfluenteExibicaoAno:
                    return Guid.Parse("825e6073-6ba7-45a7-911a-218ae29b2a2b");
                case ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoParametro:
                    return Guid.Parse("34d975d6-2190-4d10-a0f4-03b351704002");
                case ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoExibicao:
                    return Guid.Parse("c59af8e0-80e2-4f32-903f-50acd38f251d");
                case ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoAno:
                    return Guid.Parse("0be34b8c-b266-4e2b-90e7-e175ae029518");
                case ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaParametro:
                    return Guid.Parse("c69c0b51-7e5a-46d4-ac2e-c313c17a54a2");
                case ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicao:
                    return Guid.Parse("a1adbee2-9969-4d15-b5b3-a84d83706506");
                case ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicaoAno:
                    return Guid.Parse("ba5268df-ab04-430d-badb-f1fbe6e9c18e");
                case ReferenciaTipo.MudancasClimaticasTabelaEscopoParametro:
                    return Guid.Parse("298a9c99-a7b5-4a9d-b75a-b2f1eb95d6d3");
                case ReferenciaTipo.MudancasClimaticasTabelaEscopoExibicao:
                    return Guid.Parse("60a8a561-0027-4833-ae9c-57dfb7ab930e");
                case ReferenciaTipo.MudancasClimaticasTabelaEscopoExibicaoAno:
                    return Guid.Parse("7fa5c24b-938e-4a82-bb3e-f4739f65eae3");
                case ReferenciaTipo.MudancasClimaticasTabelaIntensidadeParametro:
                    return Guid.Parse("c50c74e4-a3c6-4faa-ac3d-8fc1aeb4b7e5");
                case ReferenciaTipo.MudancasClimaticasTabelaIntensidadeExibicao:
                    return Guid.Parse("7b432e0b-227c-4ab9-9328-78679baeabf9");
                case ReferenciaTipo.MudancasClimaticasTabelaIntensidadeExibicaoAno:
                    return Guid.Parse("c22c6f58-b252-4e7b-a7ed-0fdf0b90233c");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaParametro:
                    return Guid.Parse("324f48aa-2e16-453f-b087-e66b2b29f7bd");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoria:
                    return Guid.Parse("c15fd6e0-9367-41ef-89ee-d6106c3d44e8");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaFonteEmissora:
                    return Guid.Parse("dec06508-28db-443d-ac26-94964bf1bf0f");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaUnidade:
                    return Guid.Parse("efc8f06a-d3bf-4b5b-b970-f928f67bb6d4");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoriaAno:
                    return Guid.Parse("91934d22-ed72-4841-ae6d-bb9e4454250e");
                case ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaExibicao:
                    return Guid.Parse("069d401d-94ed-4ce6-9244-172a1c06f0be");
                default:
                    return Guid.Empty;
            }
        }
    }

    public enum ReferenciaTipo
    {
        AguaGraficoConsumo,
        AguaGraficoDescarte,
        AguaGraficoCaptacao,
        AguaTabelaAguaEfluenteParametro,
        AguaTabelaAguaEfluenteExibicao,
        AguaTabelaAguaEfluenteExibicaoAno,
        EnergiaTabelaConsumoEnergiaOrganizacaoParametro,
        EnergiaTabelaConsumoEnergiaOrganizacaoExibicao,
        EnergiaTabelaConsumoEnergiaOrganizacaoExibicaoAno,
        EnergiaTabelaIntensidadeEnergeticaParametro,
        EnergiaTabelaIntensidadeEnergeticaExibicao,
        EnergiaTabelaIntensidadeEnergeticaExibicaoAno,
        MudancasClimaticasTabelaEscopoParametro,
        MudancasClimaticasTabelaEscopoExibicao,
        MudancasClimaticasTabelaEscopoExibicaoAno,
        MudancasClimaticasTabelaIntensidadeParametro,
        MudancasClimaticasTabelaIntensidadeExibicao,
        MudancasClimaticasTabelaIntensidadeExibicaoAno,
        QualidadeArTabelaEmissaoAtmosfericaParametro,
        QualidadeArTabelaEmissaoAtmosfericaCategoria,
        QualidadeArTabelaEmissaoAtmosfericaFonteEmissora,
        QualidadeArTabelaEmissaoAtmosfericaUnidade,
        QualidadeArTabelaEmissaoAtmosfericaCategoriaAno,
        QualidadeArTabelaEmissaoAtmosfericaExibicao
    }
}
