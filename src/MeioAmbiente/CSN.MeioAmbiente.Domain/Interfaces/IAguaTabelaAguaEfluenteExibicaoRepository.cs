﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.Agua.Tabela;

namespace CSN.MeioAmbiente.Domain.Interfaces
{
    public interface IAguaTabelaAguaEfluenteExibicaoRepository
    {
        Task<IEnumerable<AguaEfluenteExibicao>> ObterDadosAsync(Idioma idioma);
        Task<IEnumerable<AguaEfluenteExibicaoAno>> ObterAnosPorFonteCaptacaoAsync(Guid fonteCaptacaoId, Idioma idioma, bool? ativo);
    }
}
