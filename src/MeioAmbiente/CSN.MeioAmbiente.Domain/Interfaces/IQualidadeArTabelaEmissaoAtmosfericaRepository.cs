﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela;

namespace CSN.MeioAmbiente.Domain.Interfaces
{
    public interface IQualidadeArTabelaEmissaoAtmosfericaCategoriaRepository
    {
        Task<EmissaoAtmosfericaCategoria?> ObterCategoriaPorId(Guid id);
        Task<List<EmissaoAtmosfericaCategoria>> ObterCategoriasPorIds(List<Guid> ids);
        Task<List<EmissaoAtmosfericaCategoria>> ObterCategorias(string? nome = null, bool? ativo = null);
    }

    public interface IQualidadeArTabelaEmissaoAtmosfericaFonteEmissoraRepository
    {
        Task<List<EmissaoAtmosfericaFonteEmissora>> ObterFontesEmissorasPorCategoria(Guid categoriaId, bool? ativo = null);
        Task<EmissaoAtmosfericaFonteEmissora?> ObterFontesEmissorasPorId(Guid id);
        Task<List<EmissaoAtmosfericaFonteEmissora>> ObterFontesEmissorasPorNome(string? nome = null, bool? ativo = null);
    }

    public interface IQualidadeArTabelaEmissaoAtmosfericaCategoriaAnoRepository
    {
        Task<List<EmissaoAtmosfericaCategoriaAno>> ObterAnosPorCategoria(Guid categoriaId, bool? ativo = null);
    }

    public interface IQualidadeArTabelaEmissaoAtmosfericaExibicaoRepository
    {
        Task<EmissaoAtmosfericaExibicao?> ObterUltimoAtivo(Idioma idioma);

    }
}
