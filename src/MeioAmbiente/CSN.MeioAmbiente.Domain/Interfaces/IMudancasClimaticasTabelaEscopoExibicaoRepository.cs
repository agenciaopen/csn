﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela;

namespace CSN.MeioAmbiente.Domain.Interfaces
{
    public interface IMudancasClimaticasTabelaEscopoExibicaoRepository
    {
        Task<IEnumerable<TabelaEscopoExibicao>> ObterDadosAsync(Idioma idioma);
        Task<IEnumerable<TabelaEscopoExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo);
    }
}
