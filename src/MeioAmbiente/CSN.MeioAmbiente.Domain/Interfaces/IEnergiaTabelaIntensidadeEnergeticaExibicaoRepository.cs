﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;

namespace CSN.MeioAmbiente.Domain.Interfaces
{
    public interface IEnergiaTabelaIntensidadeEnergeticaExibicaoRepository
    {
        Task<IEnumerable<IntensidadeEnergeticaExibicao>> ObterDadosAsync(Idioma idioma);
        Task<IEnumerable<IntensidadeEnergeticaExibicaoAno>> ObterAnosPorDescricaoAsync(Guid descricaoId, Idioma idioma, bool? ativo);
    }
}
