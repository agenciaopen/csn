﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Agua.Tabela
{
    public class AguaEfluenteParametro : Entity
    {
        public AguaEfluenteParametro(){}

        public AguaEfluenteParametro(string titulo, 
            string emMegaLitrosMl, 
            string areaNegocio, 
            int quantidadeAnos, 
            Idioma idioma,
            bool ativo = true)
        {
            Titulo = titulo;
            EmMegaLitrosMl = emMegaLitrosMl;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteParametro.Valor();
        }

        public AguaEfluenteParametro(Guid id,
            string titulo,
            string emMegaLitrosMl,
            string areaNegocio,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Titulo = titulo;
            EmMegaLitrosMl = emMegaLitrosMl;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteParametro.Valor();
        }

        public string Titulo { get; set; }
        public string EmMegaLitrosMl { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
