﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Agua.Tabela
{
    public class AguaEfluenteExibicaoAno : Entity
    {
        public AguaEfluenteExibicaoAno() { }

        public AguaEfluenteExibicaoAno(Guid fonteCaptacaoId, 
            int ano, 
            double todasAreas,
            double areasComEstresseHidrico,
            Idioma idioma, 
            bool ativo = true)
        {
            FonteCaptacaoId = fonteCaptacaoId;
            Ano = ano;
            TodasAreas = todasAreas;
            AreasComEstresseHidrico = areasComEstresseHidrico;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteExibicaoAno.Valor();
        }

        public AguaEfluenteExibicaoAno(Guid id, 
            Guid fonteCaptacaoId, 
            int ano,
            double todasAreas,
            double areasComEstresseHidrico,
            Idioma idioma, 
            bool ativo)
        {
            Id = id;
            FonteCaptacaoId = fonteCaptacaoId;
            Ano = ano;
            TodasAreas = todasAreas;
            AreasComEstresseHidrico = areasComEstresseHidrico;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteExibicaoAno.Valor();
        }

        public Guid FonteCaptacaoId { get; set; }
        public int Ano { get; set; }
        public double TodasAreas { get; set; }
        public double AreasComEstresseHidrico { get; set; }

        public virtual AguaEfluenteExibicao FonteCaptacao { get; set; }
    }
}
