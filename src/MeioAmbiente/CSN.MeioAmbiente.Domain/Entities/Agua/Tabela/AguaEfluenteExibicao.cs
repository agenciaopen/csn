﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Agua.Tabela
{
    public class AguaEfluenteExibicao : Entity
    {
        public AguaEfluenteExibicao() { }

        public AguaEfluenteExibicao(string fonteCaptacao, Idioma idioma, bool ativo = true)
        {
            FonteCaptacao = fonteCaptacao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteExibicao.Valor();
        }

        public AguaEfluenteExibicao(Guid id, string fonteCaptacao, Idioma idioma, bool ativo)
        {
            Id = id;
            FonteCaptacao = fonteCaptacao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.AguaTabelaAguaEfluenteExibicao.Valor();
        }

        public string FonteCaptacao { get; set; }

        public virtual ICollection<AguaEfluenteExibicaoAno> Anos { get; set; }

        [NotMapped]
        public AguaEfluenteParametro Parametro { get; set; }
    }
}
