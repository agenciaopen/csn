﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Agua.Grafico
{
    public class Descarte : Entity
    {
        public Descarte() { }

        public Descarte(string titulo,
            string subtitulo,
            double mineracao,
            double siderurgia,
            double cimento,
            double logistica,
            double outrasMineracoes,
            string informacaoUm,
            string informacaoDois,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            InformacaoUm = informacaoUm;
            InformacaoDois = informacaoDois;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Referencia = ReferenciaTipo.AguaGraficoDescarte.Valor();
        }

        public Descarte(Guid id,
            string titulo,
            string subtitulo,
            double mineracao,
            double siderurgia,
            double cimento,
            double logistica,
            double outrasMineracoes,
            string informacaoUm,
            string informacaoDois,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            InformacaoUm = informacaoUm;
            InformacaoDois = informacaoDois;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Referencia = ReferenciaTipo.AguaGraficoDescarte.Valor();
        }

        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
    }
}
