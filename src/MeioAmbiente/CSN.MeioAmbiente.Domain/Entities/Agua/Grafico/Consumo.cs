﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Agua.Grafico
{
    public class Consumo : Entity
    {
        public Consumo() { }

        public Consumo(string titulo,
            string subtitulo,
            double mineracao,
            double siderurgia,
            double cimento,
            double logistica,
            double outrasMineracoes,
            string informacaoUm,
            string informacaoDois,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            InformacaoUm = informacaoUm;
            InformacaoDois = informacaoDois;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Referencia = ReferenciaTipo.AguaGraficoConsumo.Valor();
        }

        public Consumo(Guid id,
            string titulo,
            string subtitulo,
            double mineracao,
            double siderurgia,
            double cimento,
            double logistica,
            double outrasMineracoes,
            string informacaoUm,
            string informacaoDois,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            InformacaoUm = informacaoUm;
            InformacaoDois = informacaoDois;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Referencia = ReferenciaTipo.AguaGraficoConsumo.Valor();
        }

        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double OutrasMineracoes { get; set; }
        public string InformacaoUm { get; set; }
        public string InformacaoDois { get; set; }
    }
}
