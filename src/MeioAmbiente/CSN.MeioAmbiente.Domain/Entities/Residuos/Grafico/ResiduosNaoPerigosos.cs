﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Residuos.Grafico
{
    public class ResiduosNaoPerigosos : Entity
    {
        public ResiduosNaoPerigosos() { }

        public ResiduosNaoPerigosos(string titulo,
            string sigla,
            double mineracao,
            double outrasMineracoes,
            double siderurgia,
            double cimento,
            double logistica,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
        }

        public ResiduosNaoPerigosos(Guid id,
            string titulo,
            string sigla,
            double mineracao,
            double outrasMineracoes,
            double siderurgia,
            double cimento,
            double logistica,
            double total,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            OutrasMineracoes = outrasMineracoes;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
        }
        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double OutrasMineracoes { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public double Total { get; set; }
    }
}
