﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Energia.Tabela
{
    public class ConsumoEnergiaOrganizacaoExibicao : Entity
    {
        public ConsumoEnergiaOrganizacaoExibicao() { }

        public ConsumoEnergiaOrganizacaoExibicao(string descricao, string unidade, Idioma idioma, bool ativo = true)
        {
            Descricao = descricao;
            Unidade = unidade;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoExibicao.Valor();
        }

        public ConsumoEnergiaOrganizacaoExibicao(Guid id, string descricao, string unidade, Idioma idioma, bool ativo)
        {
            Id = id;
            Descricao = descricao;
            Unidade = unidade;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoExibicao.Valor();
        }
        
        public string Descricao { get; set; }
        public string Unidade { get; set; }

        public virtual ICollection<ConsumoEnergiaOrganizacaoExibicaoAno> Anos { get; set; }

        [NotMapped]
        public ConsumoEnergiaOrganizacaoParametro Parametro { get; set; }
    }
}
