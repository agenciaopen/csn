﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Energia.Tabela
{
    public class IntensidadeEnergeticaExibicaoAno : Entity
    {
        public IntensidadeEnergeticaExibicaoAno() { }

        public IntensidadeEnergeticaExibicaoAno(Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo = true)
        {
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicaoAno.Valor();
        }

        public IntensidadeEnergeticaExibicaoAno(Guid id, Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo)
        {
            Id = id;
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicaoAno.Valor();
        }

        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public virtual IntensidadeEnergeticaExibicao Descricao { get; set; }

    }
}
