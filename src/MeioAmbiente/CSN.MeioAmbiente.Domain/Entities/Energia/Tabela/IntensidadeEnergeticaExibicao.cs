﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Energia.Tabela
{
    public class IntensidadeEnergeticaExibicao : Entity
    {
        public IntensidadeEnergeticaExibicao() { }

        public IntensidadeEnergeticaExibicao(string descricao, string unidade, Idioma idioma, bool ativo = true)
        {
            Descricao = descricao;
            Unidade = unidade;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicao.Valor();
        }

        public IntensidadeEnergeticaExibicao(Guid id, string descricao, string unidade, Idioma idioma, bool ativo)
        {
            Id = id;
            Descricao = descricao;
            Unidade = unidade;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.EnergiaTabelaIntensidadeEnergeticaExibicao.Valor();
        }

        public string Descricao { get; set; }
        public string Unidade { get; set; }

        public virtual ICollection<IntensidadeEnergeticaExibicaoAno> Anos { get; set; }

        [NotMapped]
        public IntensidadeEnergeticaParametro Parametro { get; set; }
    }
}
