﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.Energia.Tabela
{
    public class ConsumoEnergiaOrganizacaoParametro : Entity
    {
        public ConsumoEnergiaOrganizacaoParametro() { }

        public ConsumoEnergiaOrganizacaoParametro(string titulo,
            string sigla,
            string areaNegocio,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoParametro.Valor();
        }

        public ConsumoEnergiaOrganizacaoParametro(Guid id,
            string titulo,
            string sigla,
            string areaNegocio,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Sigla = sigla;
            AreaNegocio = areaNegocio;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.EnergiaTabelaConsumoEnergiaOrganizacaoParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Sigla { get; set; }
        public string AreaNegocio { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
