﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public class EmissaoAtmosfericaParametro : Entity
    {
        public EmissaoAtmosfericaParametro() { }

        public EmissaoAtmosfericaParametro(string titulo,
            string subtitulo,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaParametro.Valor();
        }

        public EmissaoAtmosfericaParametro(Guid id,
            string titulo,
            string subtitulo,
            string nota,
            int quantidadeAnos,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Nota = nota;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Nota { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
