﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public  class EmissaoAtmosfericaExibicao : Entity
    {
        public EmissaoAtmosfericaExibicao(){}

        public EmissaoAtmosfericaExibicao(Guid unidadeId, List<EmissaoAtmosfericaCategoria> categorias, Idioma idioma, bool ativo = true)
        {
            UnidadeId = unidadeId;
            Categorias = categorias;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaExibicao.Valor();
        }

        public Guid UnidadeId { get; set; }

        public virtual EmissaoAtmosfericaUnidade Unidade { get; set; }
        public virtual List<EmissaoAtmosfericaCategoria> Categorias { get; set; }

        [NotMapped]
        public EmissaoAtmosfericaParametro Parametro { get; set; }
    }
}
