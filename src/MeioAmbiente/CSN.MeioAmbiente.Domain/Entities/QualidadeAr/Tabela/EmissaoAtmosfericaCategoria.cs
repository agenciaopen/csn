﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.Entities.Energia.Tabela;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public class EmissaoAtmosfericaCategoria : Entity
    {
        public EmissaoAtmosfericaCategoria() { }

        public EmissaoAtmosfericaCategoria(string nome, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoria.Valor();
        }

        public EmissaoAtmosfericaCategoria(Guid id, string nome,  Idioma idioma, bool ativo)
        {
            Id = id;
            Nome = nome;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoria.Valor();
        }

        public string Nome { get; set; }

        public virtual List<EmissaoAtmosfericaFonteEmissora> FontesEmissoras { get; set; }
        public virtual List<EmissaoAtmosfericaCategoriaAno> Anos { get; set; }
        public virtual List<EmissaoAtmosfericaExibicao> Exibicaos { get; set; }
    }
}
