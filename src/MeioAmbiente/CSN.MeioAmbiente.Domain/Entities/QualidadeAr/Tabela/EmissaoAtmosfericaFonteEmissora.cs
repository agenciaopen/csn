﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public class EmissaoAtmosfericaFonteEmissora : Entity
    {
        public EmissaoAtmosfericaFonteEmissora() { }

        public EmissaoAtmosfericaFonteEmissora(string nome, Guid categoriaId, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            CategoriaId = categoriaId;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaFonteEmissora.Valor();
        }

        public EmissaoAtmosfericaFonteEmissora(Guid id, string nome, Idioma idioma, bool ativo)
        {
            Id = id;
            Nome = nome;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaFonteEmissora.Valor();
        }

        public string Nome { get; set; }
        public Guid CategoriaId { get; set; }

        public virtual EmissaoAtmosfericaCategoria Categoria { get; set; }
    }
}
