﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public class EmissaoAtmosfericaUnidade : Entity
    {
        public EmissaoAtmosfericaUnidade() { }

        public EmissaoAtmosfericaUnidade(string nome, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaUnidade.Valor();
        }

        public EmissaoAtmosfericaUnidade(Guid id, string nome, Idioma idioma, bool ativo)
        {
            Id = id;
            Nome = nome;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaUnidade.Valor();
        }

        public string Nome { get; set; }
    }
}
