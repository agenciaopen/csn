﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.QualidadeAr.Tabela
{
    public class EmissaoAtmosfericaCategoriaAno : Entity
    {
        public EmissaoAtmosfericaCategoriaAno(){}

        public EmissaoAtmosfericaCategoriaAno(int ano, double valor, Guid categoriaId, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            Valor = valor;
            CategoriaId = categoriaId;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoriaAno.Valor();
        }

        public EmissaoAtmosfericaCategoriaAno(Guid id, int ano, double valor, Guid categoriaId, Idioma idioma, bool ativo)
        {
            Id = id;
            Ano = ano;
            Valor = valor;
            CategoriaId = categoriaId;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.QualidadeArTabelaEmissaoAtmosfericaCategoriaAno.Valor();
        }

        public int Ano { get; set; }
        public double Valor { get; set; }
        public Guid CategoriaId { get; set; }

        public virtual EmissaoAtmosfericaCategoria Categoria { get; set; }
    }
}
