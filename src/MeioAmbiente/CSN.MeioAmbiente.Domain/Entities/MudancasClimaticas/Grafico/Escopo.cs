﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Grafico
{
    public class GraficoEscopo : Entity
    {
        public GraficoEscopo() {}

        public GraficoEscopo(string titulo, 
                            string subtitulo, 
                            string sigla, 
                            double mineracao, 
                            double siderurgia, 
                            double cimento, 
                            double logistica, 
                            string nota,
                            Idioma idioma,
                            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Sigla = sigla;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            Nota = nota;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
        }

        public GraficoEscopo(Guid id, 
                            string titulo,
                            string subtitulo,
                            string sigla,
                            double mineracao,
                            double siderurgia,
                            double cimento,
                            double logistica,
                            string nota,
                            Idioma idioma,
                            bool ativo)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            Sigla = sigla;
            Mineracao = mineracao;
            Siderurgia = siderurgia;
            Cimento = cimento;
            Logistica = logistica;
            Nota = nota;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
        }

        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public double Mineracao { get; set; }
        public double Siderurgia { get; set; }
        public double Cimento { get; set; }
        public double Logistica { get; set; }
        public string Nota { get; set; }
    }
}
