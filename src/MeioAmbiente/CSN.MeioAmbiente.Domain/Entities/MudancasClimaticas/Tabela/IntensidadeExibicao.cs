﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela
{
    public class TabelaIntensidadeExibicao : Entity
    {
        public TabelaIntensidadeExibicao() { }

        public TabelaIntensidadeExibicao(string descricao, Idioma idioma, bool ativo = true)
        {
            Descricao = descricao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaIntensidadeExibicao.Valor();
        }

        public TabelaIntensidadeExibicao(Guid id, string descricao, Idioma idioma, bool ativo)
        {
            Id = id;
            Descricao = descricao;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaIntensidadeExibicao.Valor();
        }

        public string Descricao { get; set; }

        public virtual ICollection<TabelaIntensidadeExibicaoAno> Anos { get; set; }

        [NotMapped]
        public TabelaIntensidadeParametro Parametro { get; set; }
    }
}
