﻿using CSN.Core;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela
{
    public class TabelaIntensidadeParametro : Entity
    {
        public TabelaIntensidadeParametro() { }

        public TabelaIntensidadeParametro(string titulo,
            int quantidadeAnos,
            string nota,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Nota = nota;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaIntensidadeParametro.Valor();
        }

        public TabelaIntensidadeParametro(Guid id,
            string titulo,
            int quantidadeAnos,
            string nota,
            Idioma idioma,
            bool ativo)
        {
            Titulo = titulo;
            QuantidadeAnos = quantidadeAnos;
            Nota = nota;
            Idioma = idioma;
            Ativo = ativo;
            Cadastro = DateTime.Now;
            Id = id;
            Slug = titulo.ToSlug();
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaIntensidadeParametro.Valor();
        }
        public string Titulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Nota { get; set; }
        public string Slug { get; set; }
    }
}
