﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.MeioAmbiente.Domain.ValueObjects;

namespace CSN.MeioAmbiente.Domain.Entities.MudancasClimaticas.Tabela
{
    public class TabelaEscopoExibicaoAno : Entity
    {
        public TabelaEscopoExibicaoAno() { }

        public TabelaEscopoExibicaoAno(Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo = true)
        {
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaEscopoExibicaoAno.Valor();
        }

        public TabelaEscopoExibicaoAno(Guid id, Guid descricaoId, int ano, double valor, Idioma idioma, bool ativo)
        {
            Id = id;
            DescricaoId = descricaoId;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.MudancasClimaticasTabelaEscopoExibicaoAno.Valor();
        }

        public Guid DescricaoId { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public virtual TabelaEscopoExibicao Descricao { get; set; }
    }
}
