﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using CSN.Social.Infrastructure.Data.Context;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroRepository : BaseRepositoryParametersAsync<TaxaFrequenciaParametro>
    {
        protected readonly SocialDbContext _context;

        public SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
