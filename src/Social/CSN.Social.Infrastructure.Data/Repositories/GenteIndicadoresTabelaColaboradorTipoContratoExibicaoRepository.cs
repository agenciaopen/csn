﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Interfaces;
using CSN.Social.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class GenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository : BaseRepositoryAsync<ColaboradorTipoContratoExibicao>, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository
    {
        protected readonly SocialDbContext _context;

        public GenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<ColaboradorTipoContratoExibicao>> ObterDadosAsync(Idioma idioma)
        {
            var parametro = await _context.DbSet<ColaboradorTipoContratoParametro>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro).Take(1).FirstOrDefaultAsync();

            var entities = await _context.DbSet<ColaboradorTipoContratoExibicao>()
                .Where(x => x.Idioma == idioma && x.Ativo)
                .OrderByDescending(x => x.Cadastro)
                .Take(parametro.QuantidadeAnos)
                .ToListAsync();

            foreach (var entity in entities)
            {
                entity.Parametro = parametro;
            }

            return entities;
        }
    }
}
