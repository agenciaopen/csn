﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.Diversidade.Grafico;
using CSN.Social.Infrastructure.Data.Context;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class DiversidadeGraficoPercentualMulherGrupoParametroRepository : BaseRepositoryParametersAsync<PercentualMulherGrupoParametro>
    {
        protected readonly SocialDbContext _context;

        public DiversidadeGraficoPercentualMulherGrupoParametroRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
