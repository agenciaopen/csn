﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Infrastructure.Data.Context;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class GenteIndicadoresTabelaColaboradorProprioParametroRepository : BaseRepositoryParametersAsync<ColaboradorProprioParametro>
    {
        protected readonly SocialDbContext _context;

        public GenteIndicadoresTabelaColaboradorProprioParametroRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
