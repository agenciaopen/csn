﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using CSN.Social.Infrastructure.Data.Context;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroRepository : BaseRepositoryParametersAsync<TaxaGravidadeParametro>
    {
        protected readonly SocialDbContext _context;

        public SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
