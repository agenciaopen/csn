﻿using System.Linq.Expressions;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class BaseRepositoryAsync<TEntity> : ICsnRepositoryAsync<TEntity> where TEntity : class
    {
        protected readonly SocialDbContext _context;

        public BaseRepositoryAsync(SocialDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AdicionarAsync(TEntity t)
        {
            try
            {
                _context.Set<TEntity>().Add(t);
                return await _context.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> AtualizarAsync(TEntity? t, object? key)
        {
            try
            {
                if (t == null)
                    return false;

                TEntity? exist = _context.Set<TEntity>().Find(key);

                //if (exist != null)
                //    _context.Entry(exist).CurrentValues.SetValues(t);

                if (exist != null)
                    _context.Update(t);

                return await _context.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<bool> ExcluirAsync(object id)
        {
            TEntity? exist = _context.Set<TEntity>().Find(id);

            if (exist != null)
                _context.Set<TEntity>().Remove(exist);

            return await _context.CommitAsync();
        }

        public async Task<TEntity?> ObterAsync(Expression<Func<TEntity, bool>> predicate, Func<TEntity, DateTime> order)
        {
            IOrderedEnumerable<TEntity> result = _context.DbSet<TEntity>().Where(predicate).OrderByDescending(order);
            return result.FirstOrDefault();
        }

        public async Task<bool> ExisteAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.DbSet<TEntity>().AnyAsync(predicate);
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    public class BaseRepositoryParametersAsync<TEntity> : IRepositoryAsync<TEntity> where TEntity : class
    {
        protected readonly SocialDbContext _context;

        public BaseRepositoryParametersAsync(SocialDbContext context)
        {
            _context = context;
        }

        public virtual async Task<ICollection<TEntity>> GetAllAsync()
        {

            return await _context.Set<TEntity>().ToListAsync();
        }

        public virtual async Task<TEntity> GetAsync(int id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task<bool> AddAsync(TEntity t)
        {
            try
            {
                _context.Set<TEntity>().Add(t);
                return await _context.CommitAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<ICollection<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().Where(match).ToListAsync();
        }

        public virtual async Task<bool> DeleteAsync(Guid id)
        {
            var entity = await _context.Set<TEntity>().FindAsync(id);
            _context.Set<TEntity>().Remove(entity);
            return await _context.CommitAsync();
        }

        public virtual async Task<bool> UpdateAsync(TEntity t, object key)
        {
            var result = false;
            if (t == null)
                return result;

            TEntity exist = await _context.Set<TEntity>().FindAsync(key);

            if (exist != null)
            {
                _context.Update(t);
                //_context.Entry(exist).CurrentValues.SetValues(t);
                result = await _context.CommitAsync();
            }

            return result;
        }

        public async Task<int> CountAsync()
        {
            return await _context.Set<TEntity>().CountAsync();
        }

        public async virtual Task<int> SaveAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public async Task<ICollection<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            var result = _context.Set<TEntity>().Where(predicate);

            return await result.ToListAsync();
        }

        public virtual async Task<TEntity> FindByAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().FirstOrDefaultAsync(match);
        }

        public async Task<bool> HasExistAsync(Expression<Func<TEntity, bool>> match)
        {
            return await _context.Set<TEntity>().AnyAsync(match);
        }
    }

}
