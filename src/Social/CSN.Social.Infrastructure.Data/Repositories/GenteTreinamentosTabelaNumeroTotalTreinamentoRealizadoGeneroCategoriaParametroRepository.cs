﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Infrastructure.Data.Context;

namespace CSN.Social.Infrastructure.Data.Repositories
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRepository : BaseRepositoryParametersAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>
    {
        protected readonly SocialDbContext _context;

        public GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRepository(SocialDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
