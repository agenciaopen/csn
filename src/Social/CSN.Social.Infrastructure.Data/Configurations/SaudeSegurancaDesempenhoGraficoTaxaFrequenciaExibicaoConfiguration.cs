﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoConfiguration : IEntityTypeConfiguration<TaxaFrequenciaExibicao>
    {
        public void Configure(EntityTypeBuilder<TaxaFrequenciaExibicao> builder)
        {
            builder.ToTable("SaudeSegurancaDesempenhoGraficoTaxaFrequencia", "Social");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.CAF)
                .IsRequired();

            builder.Property(c => c.SAF)
                .IsRequired();

            builder.Property(c => c.Reducao)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
