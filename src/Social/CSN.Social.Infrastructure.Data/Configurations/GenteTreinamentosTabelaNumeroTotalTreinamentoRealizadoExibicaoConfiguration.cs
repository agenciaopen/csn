﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoConfiguration : IEntityTypeConfiguration<NumeroTotalTreinamentoRealizadoExibicao>
    {
        public void Configure(EntityTypeBuilder<NumeroTotalTreinamentoRealizadoExibicao> builder)
        {
            builder.ToTable("GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado", "Social");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.QuantidadeHoras)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
