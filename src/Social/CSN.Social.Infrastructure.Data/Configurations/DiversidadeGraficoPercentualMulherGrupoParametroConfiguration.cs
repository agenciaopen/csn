﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.Diversidade.Grafico;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class DiversidadeGraficoPercentualMulherGrupoParametroConfiguration : IEntityTypeConfiguration<PercentualMulherGrupoParametro>
    {
        public void Configure(EntityTypeBuilder<PercentualMulherGrupoParametro> builder)
        {
            builder.ToTable("DiversidadeGraficoPercentualMulherGrupo", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
