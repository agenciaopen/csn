﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class GenteIndicadoresTabelaColaboradorTipoContratoExibicaoConfiguration : IEntityTypeConfiguration<ColaboradorTipoContratoExibicao>
    {
        public void Configure(EntityTypeBuilder<ColaboradorTipoContratoExibicao> builder)
        {
            builder.ToTable("GenteIndicadoresTabelaColaboradorTipoContrato", "Social");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Genero)
                .IsRequired();

            builder.Property(c => c.TempoDeterminado)
                .IsRequired();

            builder.Property(c => c.TempoIndeterminado)
                .IsRequired();

            builder.Property(c => c.Total)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }

}
