﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class GenteIndicadoresTabelaColaboradorProprioExibicaoConfiguration : IEntityTypeConfiguration<ColaboradorProprioExibicao>
    {
        public void Configure(EntityTypeBuilder<ColaboradorProprioExibicao> builder)
        {
            builder.ToTable("GenteIndicadoresTabelaColaboradorProprio", "Social");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.ColaborProprio)
                .IsRequired();

            builder.Property(c => c.ColaboradorTerceiro)
                .IsRequired();

            builder.Property(c => c.Total)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
