﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroConfiguration : IEntityTypeConfiguration<NumeroTotalTreinamentoRealizadoParametro>
    {
        public void Configure(EntityTypeBuilder<NumeroTotalTreinamentoRealizadoParametro> builder)
        {
            builder.ToTable("GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado", "Parametros");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Titulo)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.Subtitulo)
                .IsRequired()
                .HasColumnType("varchar(15)");

            builder.Property(c => c.Sigla)
                .IsRequired()
                .HasColumnType("varchar(10)");

            builder.Property(c => c.Slug)
                .IsRequired()
                .HasColumnType("varchar(50)");

            builder.Property(c => c.QuantidadeAnos)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
