﻿using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSN.Social.Infrastructure.Data.Configurations
{
    public class GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoConfiguration : IEntityTypeConfiguration<DivisaoColaboradorSegmentoExibicao>
    {
        public void Configure(EntityTypeBuilder<DivisaoColaboradorSegmentoExibicao> builder)
        {
            builder.ToTable("GenteIndicadoresTabelaDivisaoColaboradorSegmento", "Social");

            builder.HasKey(c => c.Id);

            builder.Property(c => c.Ano)
                .IsRequired();

            builder.Property(c => c.Cimento)
                .IsRequired();

            builder.Property(c => c.Corporativo)
                .IsRequired();
            
            builder.Property(c => c.Logistica)
                .IsRequired();

            builder.Property(c => c.Mineracao)
                .IsRequired();

            builder.Property(c => c.Siderurgia)
                .IsRequired();

            builder.Property(c => c.Total)
                .IsRequired();

            builder.Property(c => c.Ativo)
                .IsRequired()
                .HasColumnType("bit");

            builder.Property(c => c.Idioma)
                .IsRequired()
                .HasColumnType("int");

            builder.Property(c => c.Referencia)
                .IsRequired().HasColumnType("uniqueidentifier");

            builder.Property(c => c.Cadastro)
                .IsRequired();
        }
    }
}
