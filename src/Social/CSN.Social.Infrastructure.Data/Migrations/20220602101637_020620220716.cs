﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    public partial class _020620220716 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao",
                schema: "Social",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao");

            migrationBuilder.RenameTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao",
                schema: "Social",
                newName: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                newSchema: "Social");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Social",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Social",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidade");

            migrationBuilder.RenameTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Social",
                newName: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao",
                newSchema: "Social");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao",
                schema: "Social",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao",
                column: "Id");
        }
    }
}
