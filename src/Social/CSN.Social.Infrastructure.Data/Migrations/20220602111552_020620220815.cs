﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    public partial class _020620220815 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro",
                schema: "Parametros",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro");

            migrationBuilder.RenameTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro",
                schema: "Parametros",
                newName: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                newSchema: "Parametros");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Parametros",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaFrequencia",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(40)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(60)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaudeSegurancaDesempenhoGraficoTaxaFrequencia", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaFrequencia",
                schema: "Social",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    CAF = table.Column<double>(type: "float", nullable: false),
                    SAF = table.Column<double>(type: "float", nullable: false),
                    Reducao = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaudeSegurancaDesempenhoGraficoTaxaFrequencia", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaFrequencia",
                schema: "Parametros");

            migrationBuilder.DropTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaFrequencia",
                schema: "Social");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Parametros",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidade");

            migrationBuilder.RenameTable(
                name: "SaudeSegurancaDesempenhoGraficoTaxaGravidade",
                schema: "Parametros",
                newName: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro",
                newSchema: "Parametros");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro",
                schema: "Parametros",
                table: "SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro",
                column: "Id");
        }
    }
}
