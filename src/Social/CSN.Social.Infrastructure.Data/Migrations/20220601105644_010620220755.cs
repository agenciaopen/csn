﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    public partial class _010620220755 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprioParametro",
                schema: "Parametros",
                table: "GenteIndicadoresTabelaColaboradorProprioParametro");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprioExibicao",
                schema: "Social",
                table: "GenteIndicadoresTabelaColaboradorProprioExibicao");

            migrationBuilder.RenameTable(
                name: "GenteIndicadoresTabelaColaboradorProprioParametro",
                schema: "Parametros",
                newName: "GenteIndicadoresTabelaColaboradorProprio",
                newSchema: "Parametros");

            migrationBuilder.RenameTable(
                name: "GenteIndicadoresTabelaColaboradorProprioExibicao",
                schema: "Social",
                newName: "GenteIndicadoresTabelaColaboradorProprio",
                newSchema: "Social");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprio",
                schema: "Parametros",
                table: "GenteIndicadoresTabelaColaboradorProprio",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprio",
                schema: "Social",
                table: "GenteIndicadoresTabelaColaboradorProprio",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "GenteIndicadoresTabelaDivisaoColaboradorSegmento",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "varchar(40)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteIndicadoresTabelaDivisaoColaboradorSegmento", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenteIndicadoresTabelaDivisaoColaboradorSegmento",
                schema: "Social",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    Siderurgia = table.Column<double>(type: "float", nullable: false),
                    Mineracao = table.Column<double>(type: "float", nullable: false),
                    Logistica = table.Column<double>(type: "float", nullable: false),
                    Corporativo = table.Column<double>(type: "float", nullable: false),
                    Cimento = table.Column<double>(type: "float", nullable: false),
                    Total = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteIndicadoresTabelaDivisaoColaboradorSegmento", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GenteIndicadoresTabelaDivisaoColaboradorSegmento",
                schema: "Parametros");

            migrationBuilder.DropTable(
                name: "GenteIndicadoresTabelaDivisaoColaboradorSegmento",
                schema: "Social");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprio",
                schema: "Social",
                table: "GenteIndicadoresTabelaColaboradorProprio");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprio",
                schema: "Parametros",
                table: "GenteIndicadoresTabelaColaboradorProprio");

            migrationBuilder.RenameTable(
                name: "GenteIndicadoresTabelaColaboradorProprio",
                schema: "Social",
                newName: "GenteIndicadoresTabelaColaboradorProprioExibicao",
                newSchema: "Social");

            migrationBuilder.RenameTable(
                name: "GenteIndicadoresTabelaColaboradorProprio",
                schema: "Parametros",
                newName: "GenteIndicadoresTabelaColaboradorProprioParametro",
                newSchema: "Parametros");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprioExibicao",
                schema: "Social",
                table: "GenteIndicadoresTabelaColaboradorProprioExibicao",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GenteIndicadoresTabelaColaboradorProprioParametro",
                schema: "Parametros",
                table: "GenteIndicadoresTabelaColaboradorProprioParametro",
                column: "Id");
        }
    }
}
