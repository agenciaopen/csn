﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    public partial class _010620221435 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(15)", nullable: false),
                    Sigla = table.Column<string>(type: "varchar(10)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(50)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado",
                schema: "Social",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    QuantidadeHoras = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado",
                schema: "Parametros");

            migrationBuilder.DropTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizado",
                schema: "Social");
        }
    }
}
