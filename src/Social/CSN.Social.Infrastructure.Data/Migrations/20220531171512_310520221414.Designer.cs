﻿// <auto-generated />
using System;
using CSN.Social.Infrastructure.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    [DbContext(typeof(SocialDbContext))]
    [Migration("20220531171512_310520221414")]
    partial class _310520221414
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CSN.Social.Domain.Entities.GenteIndicadores.Tabela.ColaboradorProprioExibicao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<int>("Ano")
                        .HasColumnType("int");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<double>("ColaborProprio")
                        .HasColumnType("float");

                    b.Property<double>("ColaboradorTerceiro")
                        .HasColumnType("float");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<double>("Total")
                        .HasColumnType("float");

                    b.HasKey("Id");

                    b.ToTable("GenteIndicadoresTabelaColaboradorProprioExibicao", "Social");
                });

            modelBuilder.Entity("CSN.Social.Domain.Entities.GenteIndicadores.Tabela.ColaboradorProprioParametro", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<bool>("Ativo")
                        .HasColumnType("bit");

                    b.Property<DateTime>("Cadastro")
                        .HasColumnType("datetime2");

                    b.Property<int>("Idioma")
                        .HasColumnType("int");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("varchar(40)");

                    b.Property<int>("QuantidadeAnos")
                        .HasColumnType("int");

                    b.Property<Guid>("Referencia")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Slug")
                        .IsRequired()
                        .HasColumnType("varchar(50)");

                    b.HasKey("Id");

                    b.ToTable("GenteIndicadoresTabelaColaboradorProprioParametro", "Parametros");
                });
#pragma warning restore 612, 618
        }
    }
}
