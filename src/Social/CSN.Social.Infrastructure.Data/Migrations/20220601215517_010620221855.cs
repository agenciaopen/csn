﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CSN.Social.Infrastructure.Data.Migrations
{
    public partial class _010620221855 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria",
                schema: "Parametros",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Titulo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Subtitulo = table.Column<string>(type: "varchar(15)", nullable: false),
                    QuantidadeAnos = table.Column<int>(type: "int", nullable: false),
                    Slug = table.Column<string>(type: "varchar(60)", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria",
                schema: "Social",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ano = table.Column<int>(type: "int", nullable: false),
                    QuantidadeHoras = table.Column<double>(type: "float", nullable: false),
                    Idioma = table.Column<int>(type: "int", nullable: false),
                    Referencia = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Ativo = table.Column<bool>(type: "bit", nullable: false),
                    Cadastro = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria",
                schema: "Parametros");

            migrationBuilder.DropTable(
                name: "GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoria",
                schema: "Social");
        }
    }
}
