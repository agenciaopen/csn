﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Domain.Interfaces
{
    public interface IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRepository
    {
        Task<IEnumerable<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
