﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Domain.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorProprioExibicaoRepository
    {
        Task<IEnumerable<ColaboradorProprioExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
