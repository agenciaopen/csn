﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Domain.Interfaces
{
    public interface ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoRepository
    {
        Task<IEnumerable<TaxaGravidadeExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
