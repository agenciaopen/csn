﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.Diversidade.Grafico;

namespace CSN.Social.Domain.Interfaces
{
    public interface IDiversidadeGraficoPercentualMulherGrupoExibicaoRepository
    {
        Task<IEnumerable<PercentualMulherGrupoExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
