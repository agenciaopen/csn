﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Domain.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository
    {
        Task<IEnumerable<ColaboradorTipoContratoExibicao>> ObterDadosAsync(Idioma idioma);
    }
}
