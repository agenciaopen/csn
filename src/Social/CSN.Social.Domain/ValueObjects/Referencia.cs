﻿namespace CSN.Social.Domain.ValueObjects
{
    public static class Referencia
    {
        public static Guid Valor(this ReferenciaTipo me)
        {
            switch (me)
            {
                case ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioParametro:
                    return Guid.Parse("55368864-909d-4a48-97e2-49f3d348de93");
                case ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioExibicao:
                    return Guid.Parse("f599f55e-3e08-482d-a928-6d4d38259ea3");
                case ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametro:
                    return Guid.Parse("d1937a2e-af12-459a-812e-1cd744f04611");
                case ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicao:
                    return Guid.Parse("78616877-99e4-4e11-b5f3-1513ba6c3414");
                case ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoParametro:
                    return Guid.Parse("89858b89-9f32-47a3-aa81-af94ebcdd1bb");
                case ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoExibicao:
                    return Guid.Parse("42a3cedb-c3eb-49b9-bd2b-aad1d4595287");
                case ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametro:
                    return Guid.Parse("17171b69-f036-421c-af8b-ec3b14e25f95");
                case ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicao:
                    return Guid.Parse("545db75a-6c3d-46dc-88ab-c9adeb08203d");
                case ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro:
                    return Guid.Parse("89fb1266-1a0b-4d50-b14d-2bc09ca0ee98");
                case ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao:
                    return Guid.Parse("f310f112-0c85-4e3b-9170-0d7a8af556e9");
                case ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro:
                    return Guid.Parse("f96de215-06e4-498a-b451-26befa68f9a3");
                case ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao:
                    return Guid.Parse("12ddc6ea-9fcb-4774-bacd-69b52c47c419");
                case ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametro:
                    return Guid.Parse("54982c43-e5f7-4a7d-85f6-4fd1c721798a");
                case ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicao:
                    return Guid.Parse("971d59f2-7693-4307-b838-5d3fa24923ad");
                case ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoParametro:
                    return Guid.Parse("94e9ef67-7141-474a-9ffe-2da7b7055dbb");
                case ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoExibicao:
                    return Guid.Parse("f91fd6f9-ce8a-43a1-9eb2-a62910726b03");
                default:
                    return Guid.Empty;
            }
        }
    }

    public enum ReferenciaTipo
    {
        GenteIndicadoresTabelaColaboradorProprioParametro,
        GenteIndicadoresTabelaColaboradorProprioExibicao,
        GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametro,
        GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicao,
        GenteIndicadoresTabelaColaboradorTipoContratoParametro,
        GenteIndicadoresTabelaColaboradorTipoContratoExibicao,
        GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametro,
        GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicao,
        GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro,
        GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao,
        SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametro,
        SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao,
        SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametro,
        SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicao,
        DiversidadeGraficoPercentualMulherGrupoParametro,
        DiversidadeGraficoPercentualMulherGrupoExibicao
    }
}
