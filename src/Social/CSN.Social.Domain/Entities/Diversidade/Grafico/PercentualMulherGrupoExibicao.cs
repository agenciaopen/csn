﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.Diversidade.Grafico
{
    public class PercentualMulherGrupoExibicao : Entity
    {
        public PercentualMulherGrupoExibicao() { }

        public PercentualMulherGrupoExibicao(int ano, double valor, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoExibicao.Valor();
        }

        public PercentualMulherGrupoExibicao(Guid id, int ano, double valor, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Ano = ano;
            Valor = valor;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoExibicao.Valor();
        }

        public int Ano { get; set; }
        public double Valor { get; set; }

        [NotMapped]
        public PercentualMulherGrupoParametro Parametro { get; set; }
    }
}
