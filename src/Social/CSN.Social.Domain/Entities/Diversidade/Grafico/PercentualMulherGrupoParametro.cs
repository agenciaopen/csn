﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.Diversidade.Grafico
{
    public class PercentualMulherGrupoParametro : Entity
    {
        public PercentualMulherGrupoParametro(string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            Slug = nome.ToSlug();
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoParametro.Valor();
        }

        public PercentualMulherGrupoParametro(Guid id, string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Nome = nome;
            Slug = nome.ToSlug();
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.DiversidadeGraficoPercentualMulherGrupoParametro.Valor();
        }

        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
