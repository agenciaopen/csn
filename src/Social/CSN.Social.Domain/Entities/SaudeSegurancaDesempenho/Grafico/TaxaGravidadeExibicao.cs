﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico
{
    public class TaxaGravidadeExibicao : Entity
    {
        public TaxaGravidadeExibicao(){}

        public TaxaGravidadeExibicao(int ano, double valor, double reducao, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            Valor = valor;
            Reducao = reducao;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao.Valor();
        }

        public TaxaGravidadeExibicao(Guid id, int ano, double valor, double reducao, Idioma idioma, bool ativo)
        {
            Id = id;
            Ano = ano;
            Valor = valor;
            Reducao = reducao;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicao.Valor();
        }

        public int Ano { get; set; }
        public double Valor { get; set; }
        public double Reducao { get; set; }

        [NotMapped]
        public TaxaGravidadeParametro Parametro { get; set; }
    }
}
