﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico
{
    public class TaxaFrequenciaExibicao : Entity
    {
        public TaxaFrequenciaExibicao() { }

        public TaxaFrequenciaExibicao(int ano, double caf, double saf, double reducao, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            CAF = caf;
            SAF = saf;
            Reducao = reducao;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicao.Valor();
        }

        public TaxaFrequenciaExibicao(Guid id, int ano, double caf, double saf, double reducao, Idioma idioma, bool ativo)
        {
            Id = id;
            Ano = ano;
            CAF = caf;
            SAF = saf;
            Reducao = reducao;
            Ativo = ativo;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicao.Valor();
        }

        public int Ano { get; set; }
        public double CAF { get; set; }
        public double SAF { get; set; }
        public double Reducao { get; set; }

        [NotMapped]
        public TaxaFrequenciaParametro Parametro { get; set; }
    }
}