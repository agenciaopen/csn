﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico
{
    public class TaxaFrequenciaParametro : Entity
    {
        public TaxaFrequenciaParametro() { }

        public TaxaFrequenciaParametro(string nome, string sigla, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Slug = nome.ToSlug();
            Ativo = ativo;
            Idioma = idioma;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametro.Valor();
        }

        public TaxaFrequenciaParametro(Guid id, string nome, string sigla, int quantidadeAnos, Idioma idioma, bool ativo)
        {
            Id = id;
            Nome = nome;
            Sigla = sigla;
            QuantidadeAnos = quantidadeAnos;
            Slug = nome.ToSlug();
            Ativo = ativo;
            Idioma = idioma;
            Referencia = ReferenciaTipo.SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametro.Valor();
        }

        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
