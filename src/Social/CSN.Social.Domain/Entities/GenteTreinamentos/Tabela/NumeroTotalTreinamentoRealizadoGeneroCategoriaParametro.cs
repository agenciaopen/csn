﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteTreinamentos.Tabela
{
    public class NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro : Entity
    {
        public NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro() { }

        public NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro(string titulo,
            string subtitulo, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Titulo = titulo;
            Subtitulo = subtitulo;
            QuantidadeAnos = quantidadeAnos;
            Slug = titulo.ToSlug();
            Ativo = ativo;
            Idioma = idioma;
            Referencia = ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro.Valor();
        }

        public NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro(Guid id, string titulo,
            string subtitulo, int quantidadeAnos, Idioma idioma, bool ativo)
        {
            Id = id;
            Titulo = titulo;
            Subtitulo = subtitulo;
            QuantidadeAnos = quantidadeAnos;
            Slug = titulo.ToSlug();
            Ativo = ativo;
            Idioma = idioma;
            Referencia = ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametro.Valor();
        }

        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
