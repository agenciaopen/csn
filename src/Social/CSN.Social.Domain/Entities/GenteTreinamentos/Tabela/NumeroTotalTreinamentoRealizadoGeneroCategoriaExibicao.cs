﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteTreinamentos.Tabela
{
    public class NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao : Entity
    {
        public NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao() { }

        public NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao(int ano, double quantidadeHoras, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            QuantidadeHoras = quantidadeHoras;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao.Valor();
        }

        public NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao(Guid id, int ano, double quantidadeHoras, Idioma idioma, bool ativo)
        {
            Id = id;
            Ano = ano;
            QuantidadeHoras = quantidadeHoras;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao.Valor();
        }

        public int Ano { get; set; }
        public double QuantidadeHoras { get; set; }

        [NotMapped]
        public NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro Parametro { get; set; }
    }

}
