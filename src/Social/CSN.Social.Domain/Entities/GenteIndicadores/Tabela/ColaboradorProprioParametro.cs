﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class ColaboradorProprioParametro : Entity
    {
        public ColaboradorProprioParametro(){}

        public ColaboradorProprioParametro(string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioParametro.Valor();
            Slug = nome.ToSlug();
        }

        public ColaboradorProprioParametro(Guid id, string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioParametro.Valor();
            Slug = nome.ToSlug();
        }

        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }
}
