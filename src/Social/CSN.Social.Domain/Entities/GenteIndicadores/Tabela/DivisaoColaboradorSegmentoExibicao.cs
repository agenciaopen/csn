﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class DivisaoColaboradorSegmentoExibicao : Entity
    {
        public DivisaoColaboradorSegmentoExibicao() { }

        public DivisaoColaboradorSegmentoExibicao(int ano, 
            double siderurgia, 
            double mineracao, 
            double logistica, 
            double corporativo, 
            double cimento, 
            double total,
            Idioma idioma,
            bool ativo = true)
        {
            Ano = ano;
            Siderurgia = siderurgia;
            Mineracao = mineracao;
            Logistica = logistica;
            Corporativo = corporativo;
            Cimento = cimento;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicao.Valor();
        }

        public DivisaoColaboradorSegmentoExibicao(Guid id,
            int ano,
            double siderurgia,
            double mineracao,
            double logistica,
            double corporativo,
            double cimento,
            double total,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Ano = ano;
            Siderurgia = siderurgia;
            Mineracao = mineracao;
            Logistica = logistica;
            Corporativo = corporativo;
            Cimento = cimento;
            Total = total;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicao.Valor();
        }
        
        public int Ano { get; set; }
        public double Siderurgia { get; set; }
        public double Mineracao { get; set; }
        public double Logistica { get; set; }
        public double Corporativo { get; set; }
        public double Cimento { get; set; }
        public double Total { get; set; }

        [NotMapped]
        public DivisaoColaboradorSegmentoParametro Parametro { get; set; }
    }
}
