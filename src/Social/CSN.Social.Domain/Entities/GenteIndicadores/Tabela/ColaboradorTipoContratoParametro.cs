﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class ColaboradorTipoContratoParametro : Entity
    {
        public ColaboradorTipoContratoParametro() { }

        public ColaboradorTipoContratoParametro(string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoParametro.Valor();
            Slug = nome.ToSlug();
        }

        public ColaboradorTipoContratoParametro(Guid id, string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoParametro.Valor();
            Slug = nome.ToSlug();
        }

        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }
    }

}
