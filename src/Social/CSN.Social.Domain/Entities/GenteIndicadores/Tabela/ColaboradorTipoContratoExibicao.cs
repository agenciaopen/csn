﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class ColaboradorTipoContratoExibicao : Entity
    {
        public ColaboradorTipoContratoExibicao(){}

        public ColaboradorTipoContratoExibicao(TipoGenero genero, 
            double tempoDeterminado, 
            double tempoIndeterminado,
            double total,
            Idioma idioma,
            bool ativo = true)
        {
            Genero = genero;
            TempoDeterminado = tempoDeterminado;
            TempoIndeterminado = tempoIndeterminado;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoExibicao.Valor();
        }

        public ColaboradorTipoContratoExibicao(Guid id,
            TipoGenero genero,
            double tempoDeterminado,
            double tempoIndeterminado,
            double total,
            Idioma idioma,
            bool ativo = true)
        {
            Id = id;
            Genero = genero;
            TempoDeterminado = tempoDeterminado;
            TempoIndeterminado = tempoIndeterminado;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorTipoContratoExibicao.Valor();
        }

        public TipoGenero Genero { get; set; }
        public double TempoDeterminado { get; set; }
        public double TempoIndeterminado { get; set; }
        public double Total { get; set; }

        [NotMapped]
        public ColaboradorTipoContratoParametro Parametro { get; set; }
    }
}
