﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Core.Extensions;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class DivisaoColaboradorSegmentoParametro : Entity
    {
        public DivisaoColaboradorSegmentoParametro() { }

        public DivisaoColaboradorSegmentoParametro(string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametro.Valor();
            Slug = nome.ToSlug();
        }

        public DivisaoColaboradorSegmentoParametro(Guid id, string nome, int quantidadeAnos, Idioma idioma, bool ativo = true)
        {
            Id = id;
            Nome = nome;
            QuantidadeAnos = quantidadeAnos;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametro.Valor();
            Slug = nome.ToSlug();
        }

        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public string Slug { get; set; }

    }
}
