﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Domain.Entities.GenteIndicadores.Tabela
{
    public class ColaboradorProprioExibicao : Entity
    {
        public ColaboradorProprioExibicao(){}

        public ColaboradorProprioExibicao(int ano, double colaborProprio, double colaboradorTerceiro, double total, Idioma idioma, bool ativo = true)
        {
            Ano = ano;
            ColaborProprio = colaborProprio;
            ColaboradorTerceiro = colaboradorTerceiro;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioExibicao.Valor();
        }

        public ColaboradorProprioExibicao(Guid id, int ano, double colaborProprio, double colaboradorTerceiro, double total, Idioma idioma, bool ativo = true)
        {

            Id = id;
            Ano = ano;
            ColaborProprio = colaborProprio;
            ColaboradorTerceiro = colaboradorTerceiro;
            Idioma = idioma;
            Ativo = ativo;
            Referencia = ReferenciaTipo.GenteIndicadoresTabelaColaboradorProprioExibicao.Valor();
        }

        public int Ano { get; set; }
        public double ColaborProprio{ get; set; }
        public double ColaboradorTerceiro{ get; set; }
        public double Total { get; set; }

        [NotMapped]
        public ColaboradorProprioParametro Parametro { get; set; }
    }
}
