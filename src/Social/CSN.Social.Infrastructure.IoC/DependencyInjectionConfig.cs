﻿using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Services;
using CSN.Social.Domain.Entities.Diversidade.Grafico;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using CSN.Social.Domain.Interfaces;
using CSN.Social.Infrastructure.Data.Context;
using CSN.Social.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CSN.Social.Infrastructure.IoC
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection AddSocialContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SocialDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("CSN")));

            return services;
        }

        public static IServiceCollection AddSocialServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<SocialDbContext>();

            services.AddScoped<IRepositoryAsync<ColaboradorProprioParametro>, BaseRepositoryParametersAsync<ColaboradorProprioParametro>>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorProprioParametroService, GenteIndicadoresTabelaColaboradorProprioParametroService>();

            services.AddScoped<ICsnRepositoryAsync<ColaboradorProprioExibicao>, BaseRepositoryAsync<ColaboradorProprioExibicao>>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorProprioExibicaoRepository, GenteIndicadoresTabelaColaboradorProprioExibicaoRepository>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorProprioExibicaoService, GenteIndicadoresTabelaColaboradorProprioExibicaoService>();

            services.AddScoped<IRepositoryAsync<DivisaoColaboradorSegmentoParametro>, BaseRepositoryParametersAsync<DivisaoColaboradorSegmentoParametro>>();
            services.AddScoped<IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService, GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService>();

            services.AddScoped<ICsnRepositoryAsync<DivisaoColaboradorSegmentoExibicao>, BaseRepositoryAsync<DivisaoColaboradorSegmentoExibicao>>();
            services.AddScoped<IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoRepository, GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoRepository>();
            services.AddScoped<IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService, GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService>();

            services.AddScoped<IRepositoryAsync<ColaboradorTipoContratoParametro>, BaseRepositoryParametersAsync<ColaboradorTipoContratoParametro>>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorTipoContratoParametroService, GenteIndicadoresTabelaColaboradorTipoContratoParametroService>();

            services.AddScoped<ICsnRepositoryAsync<ColaboradorTipoContratoExibicao>, BaseRepositoryAsync<ColaboradorTipoContratoExibicao>>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository, GenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository>();
            services.AddScoped<IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService, GenteIndicadoresTabelaColaboradorTipoContratoExibicaoService>();

            services.AddScoped<IRepositoryAsync<NumeroTotalTreinamentoRealizadoParametro>, BaseRepositoryParametersAsync<NumeroTotalTreinamentoRealizadoParametro>>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService>();

            services.AddScoped<ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoExibicao>, BaseRepositoryAsync<NumeroTotalTreinamentoRealizadoExibicao>>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoRepository, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoRepository>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService>();

            services.AddScoped<IRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>, BaseRepositoryParametersAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService>();

            services.AddScoped<ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>, BaseRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRepository, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRepository>();
            services.AddScoped<IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService, GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService>();

            services.AddScoped<IRepositoryAsync<TaxaGravidadeParametro>, BaseRepositoryParametersAsync<TaxaGravidadeParametro>>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService, SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService>();

            services.AddScoped<ICsnRepositoryAsync<TaxaGravidadeExibicao>, BaseRepositoryAsync<TaxaGravidadeExibicao>>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoRepository, SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoRepository>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService, SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService>();

            services.AddScoped<IRepositoryAsync<TaxaFrequenciaParametro>, BaseRepositoryParametersAsync<TaxaFrequenciaParametro>>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService, SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService>();

            services.AddScoped<ICsnRepositoryAsync<TaxaFrequenciaExibicao>, BaseRepositoryAsync<TaxaFrequenciaExibicao>>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoRepository, SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoRepository>();
            services.AddScoped<ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService, SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService>();

            services.AddScoped<IRepositoryAsync<PercentualMulherGrupoParametro>, BaseRepositoryParametersAsync<PercentualMulherGrupoParametro>>();
            services.AddScoped<IDiversidadeGraficoPercentualMulherGrupoExibicaoRepository, DiversidadeGraficoPercentualMulherGrupoExibicaoRepository>();
            services.AddScoped<ICsnRepositoryAsync<PercentualMulherGrupoExibicao>, BaseRepositoryAsync<PercentualMulherGrupoExibicao>>();

            services.AddScoped<IDiversidadeGraficoPercentualMulherGrupoParametroService, DiversidadeGraficoPercentualMulherGrupoParametroService>();
            services.AddScoped<IDiversidadeGraficoPercentualMulherGrupoExibicaoService, DiversidadeGraficoPercentualMulherGrupoExibicaoService>();

            return services;
        }
    }
}