﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.Diversidade
{
    public class AdicionarGraficoPercentualMulherGrupoParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoPercentualMulherGrupoParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoPercentualMulherGrupoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoPercentualMulherGrupoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoPercentualMulherGrupoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoPercentualMulherGrupoParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoPercentualMulherGrupoParametroRequest
    {
        public string Slug { get; set; }
    }
}
