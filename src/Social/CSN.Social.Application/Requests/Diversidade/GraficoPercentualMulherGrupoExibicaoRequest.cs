﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.Diversidade
{
    public class AdicionarGraficoPercentualMulherGrupoExibicaoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoPercentualMulherGrupoExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoPercentualMulherGrupoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoPercentualMulherGrupoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
