﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteTreinamentos
{
    public class AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public string Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest
    {
        public string Slug { get; set; }
    }
}
