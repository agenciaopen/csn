﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteTreinamentos
{
    public class AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public int Ano { get; set; }
        public double QuantidadeHoras { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double QuantidadeHoras { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
