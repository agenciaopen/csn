﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteTreinamentos
{
    public class AdicionarTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public Guid Id { get; set; }
        public string Titulo { get; set; }
        public string Subtitulo { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public string Titulo { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaNumeroTotalTreinamentoRealizadoParametroRequest
    {
        public string Slug { get; set; }
    }

}
