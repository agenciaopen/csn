﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteTreinamentos
{
    public class AdicionarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public int Ano { get; set; }
        public double QuantidadeHoras { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double QuantidadeHoras { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}
