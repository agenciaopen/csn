﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaColaboradorTipoContratoParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaColaboradorTipoContratoParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaColaboradorTipoContratoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaColaboradorTipoContratoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaColaboradorTipoContratoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaColaboradorTipoContratoParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaColaboradorTipoContratoParametroRequest
    {
        public string Slug { get; set; }
    }

}
