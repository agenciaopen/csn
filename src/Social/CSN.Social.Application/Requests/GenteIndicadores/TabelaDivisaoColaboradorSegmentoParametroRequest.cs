﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaDivisaoColaboradorSegmentoParametroRequest
    {
        public string Slug { get; set; }
    }

}
