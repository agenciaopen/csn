﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaColaboradorTipoContratoExibicaoRequest
    {
        public TipoGenero Genero { get; set; }
        public double TempoDeterminado { get; set; }
        public double TempoIndeterminado { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaColaboradorTipoContratoExibicaoRequest
    {
        public Guid Id { get; set; }
        public TipoGenero Genero { get; set; }
        public double TempoDeterminado { get; set; }
        public double TempoIndeterminado { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaColaboradorTipoContratoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosTabelaColaboradorTipoContratoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
