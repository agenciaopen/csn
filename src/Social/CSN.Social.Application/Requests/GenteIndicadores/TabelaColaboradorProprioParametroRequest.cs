﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaColaboradorProprioParametroRequest
    {
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaColaboradorProprioParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaColaboradorProprioParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdTabelaColaboradorProprioParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugTabelaColaboradorProprioParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaTabelaColaboradorProprioParametroRequest
    {
        public string? Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteTabelaColaboradorProprioParametroRequest
    {
        public string Slug { get; set; }
    }

}
