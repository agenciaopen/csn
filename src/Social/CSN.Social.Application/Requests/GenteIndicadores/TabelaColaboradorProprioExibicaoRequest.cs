﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaColaboradorProprioExibicaoRequest
    {
        public int Ano { get; set; }
        public double ColaborProprio { get; set; }
        public double ColaboradorTerceiro { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaColaboradorProprioExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double ColaborProprio { get; set; }
        public double ColaboradorTerceiro { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaColaboradorProprioExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoTabelaColaboradorProprioExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosTabelaColaboradorProprioExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaColaboradorProprioExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }
}
