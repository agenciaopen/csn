﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.GenteIndicadores
{
    public class AdicionarTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public int Ano { get; set; }
        public double Siderurgia { get; set; }
        public double Mineracao { get; set; }
        public double Logistica { get; set; }
        public double Corporativo { get; set; }
        public double Cimento { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Siderurgia { get; set; }
        public double Mineracao { get; set; }
        public double Logistica { get; set; }
        public double Corporativo { get; set; }
        public double Cimento { get; set; }
        public double Total { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}
