﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.SaudeSegurancaDesempenho
{
    public class AdicionarGraficoTaxaFrequenciaParametroRequest
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoTaxaFrequenciaParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoTaxaFrequenciaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoTaxaFrequenciaParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoTaxaFrequenciaParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoTaxaFrequenciaParametroRequest
    {
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoTaxaFrequenciaParametroRequest
    {
        public string Slug { get; set; }
    }

}