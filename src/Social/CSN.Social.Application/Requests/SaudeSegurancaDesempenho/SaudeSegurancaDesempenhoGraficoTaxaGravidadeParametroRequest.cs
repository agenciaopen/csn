﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.SaudeSegurancaDesempenho
{
    public class AdicionarGraficoTaxaGravidadeParametroRequest
    {
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoTaxaGravidadeParametroRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public int QuantidadeAnos { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoTaxaGravidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorIdGraficoTaxaGravidadeParametroRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterPorSlugGraficoTaxaGravidadeParametroRequest
    {
        public string Slug { get; set; }
    }

    public class ObterListaGraficoTaxaGravidadeParametroRequest
    {
        public string Nome { get; set; }
        public bool? Ativo { get; set; }
    }

    public class ExisteGraficoTaxaGravidadeParametroRequest
    {
        public string Slug { get; set; }
    }

}
