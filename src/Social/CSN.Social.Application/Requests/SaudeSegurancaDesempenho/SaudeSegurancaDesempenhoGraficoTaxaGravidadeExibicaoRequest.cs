﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.SaudeSegurancaDesempenho
{
    public class AdicionarGraficoTaxaGravidadeExibicaoRequest
    {
        public int Ano { get; set; }
        public double Valor { get; set; }
        public double Reducao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoTaxaGravidadeExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double Valor { get; set; }
        public double Reducao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoTaxaGravidadeExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoTaxaGravidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoTaxaGravidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoTaxaGravidadeExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}
