﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Social.Application.Requests.SaudeSegurancaDesempenho
{
    public class AdicionarGraficoTaxaFrequenciaExibicaoRequest
    {
        public int Ano { get; set; }
        public double CAF { get; set; }
        public double SAF { get; set; }
        public double Reducao { get; set; }
        public Idioma Idioma { get; set; }
    }

    public class AtualizarGraficoTaxaFrequenciaExibicaoRequest
    {
        public Guid Id { get; set; }
        public int Ano { get; set; }
        public double CAF { get; set; }
        public double SAF { get; set; }
        public double Reducao { get; set; }
        public Idioma Idioma { get; set; }
        public bool Ativo { get; set; }
    }

    public class ExcluirGraficoTaxaFrequenciaExibicaoRequest
    {
        public Guid Id { get; set; }
    }

    public class ObterUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ObterDadosGraficoTaxaFrequenciaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

    public class ExisteUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest
    {
        public Idioma Idioma { get; set; }
    }

}