﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.Diversidade;
using CSN.Social.Domain.Entities.Diversidade.Grafico;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class DiversidadeGraficoPercentualMulherGrupoExibicaoService : IDiversidadeGraficoPercentualMulherGrupoExibicaoService
    {
        private readonly ICsnRepositoryAsync<PercentualMulherGrupoExibicao> _csnRepository;
        private readonly IDiversidadeGraficoPercentualMulherGrupoExibicaoRepository _repository;

        public DiversidadeGraficoPercentualMulherGrupoExibicaoService(ICsnRepositoryAsync<PercentualMulherGrupoExibicao> csnRepository, IDiversidadeGraficoPercentualMulherGrupoExibicaoRepository repository)
        {
            _csnRepository = csnRepository;
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            try
            {
                var entity = new PercentualMulherGrupoExibicao(request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            try
            {
                var entity = new PercentualMulherGrupoExibicao(request.Id, request.Ano, request.Valor, request.Idioma);
                var result = await _csnRepository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            try
            {
                var result = await _csnRepository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<PercentualMulherGrupoExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            var entity = await _csnRepository
                .ObterAsync(x => x.Ativo && x.Idioma == request.Idioma, o => o.Cadastro);

            return new ResponseResult<PercentualMulherGrupoExibicao>(true, entity);
        }

        public async Task<ResponseResult<IEnumerable<PercentualMulherGrupoExibicao>>> ObterDadosAsync(ObterDadosGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            var entity = await _repository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<PercentualMulherGrupoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest request)
        {
            return await _csnRepository
                .ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }

    }
}
