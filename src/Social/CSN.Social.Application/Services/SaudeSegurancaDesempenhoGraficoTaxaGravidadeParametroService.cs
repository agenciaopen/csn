﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Services
{
    public class SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService : ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService
    {
        private readonly IRepositoryAsync<TaxaGravidadeParametro> _repository;

        public SaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService(IRepositoryAsync<TaxaGravidadeParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaGravidadeParametroRequest request)
        {
            try
            {
                var entity = new TaxaGravidadeParametro(request.Nome, request.Sigla, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaGravidadeParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaGravidadeParametroRequest request)
        {
            try
            {
                var entity = new TaxaGravidadeParametro(request.Id, request.Nome, request.Sigla, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TaxaGravidadeParametro>> ObterPorIdAsync(ObterPorIdGraficoTaxaGravidadeParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<TaxaGravidadeParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<TaxaGravidadeParametro>> ObterPorSlugAsync(ObterPorSlugGraficoTaxaGravidadeParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<TaxaGravidadeParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<TaxaGravidadeParametro>>> ObterListaAsync(ObterListaGraficoTaxaGravidadeParametroRequest request)
        {
            var predicate = PredicateBuilder.True<TaxaGravidadeParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<TaxaGravidadeParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteGraficoTaxaGravidadeParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
