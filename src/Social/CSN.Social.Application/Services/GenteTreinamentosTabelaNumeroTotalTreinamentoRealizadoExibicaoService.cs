﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService : IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService
    {
        private readonly ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoExibicao> _repository;
        private readonly IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoRepository _exibicaoRepository;

        public GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService(ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoExibicao> repository, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoExibicao(request.Ano, request.QuantidadeHoras, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoExibicao(request.Id, request.Ano, request.QuantidadeHoras, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoExibicao>>> ObterDadosAsync(ObterDadosTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
