﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService : IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService
    {
        private readonly ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao> _repository;
        private readonly IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRepository _exibicaoRepository;

        public GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService(ICsnRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao> repository, IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao(request.Ano, request.QuantidadeHoras, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao(request.Id, request.Ano, request.QuantidadeHoras, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>>> ObterDadosAsync(ObterDadosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }

}
