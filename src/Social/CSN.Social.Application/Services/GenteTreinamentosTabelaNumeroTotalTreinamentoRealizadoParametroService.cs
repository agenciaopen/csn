﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Services
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService : IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService
    {
        private readonly IRepositoryAsync<NumeroTotalTreinamentoRealizadoParametro> _repository;

        public GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService(IRepositoryAsync<NumeroTotalTreinamentoRealizadoParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoParametro(request.Titulo, request.Subtitulo, request.Sigla, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoParametro(request.Id, request.Titulo, request.Subtitulo, request.Sigla, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>> ObterPorIdAsync(ObterPorIdTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<NumeroTotalTreinamentoRealizadoParametro>>> ObterListaAsync(ObterListaTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<NumeroTotalTreinamentoRealizadoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<NumeroTotalTreinamentoRealizadoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaNumeroTotalTreinamentoRealizadoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }

}
