﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService : ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService
    {
        private readonly ICsnRepositoryAsync<TaxaGravidadeExibicao> _repository;
        private readonly ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoRepository _exibicaoRepository;

        public SaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService(ICsnRepositoryAsync<TaxaGravidadeExibicao> repository, ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaGravidadeExibicaoRequest request)
        {
            try
            {
                var entity = new TaxaGravidadeExibicao(request.Ano, request.Valor, request.Reducao, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaGravidadeExibicaoRequest request)
        {
            try
            {
                var entity = new TaxaGravidadeExibicao(request.Id, request.Ano, request.Valor, request.Reducao, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaGravidadeExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TaxaGravidadeExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoTaxaGravidadeExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<TaxaGravidadeExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<TaxaGravidadeExibicao>>> ObterDadosAsync(ObterDadosGraficoTaxaGravidadeExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<TaxaGravidadeExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoTaxaGravidadeExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
