﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Services
{
    public class SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService : ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService
    {
        private readonly IRepositoryAsync<TaxaFrequenciaParametro> _repository;

        public SaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService(IRepositoryAsync<TaxaFrequenciaParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaFrequenciaParametroRequest request)
        {
            try
            {
                var entity = new TaxaFrequenciaParametro(request.Nome, request.Sigla, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaFrequenciaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaFrequenciaParametroRequest request)
        {
            try
            {
                var entity = new TaxaFrequenciaParametro(request.Id, request.Nome, request.Sigla, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TaxaFrequenciaParametro>> ObterPorIdAsync(ObterPorIdGraficoTaxaFrequenciaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<TaxaFrequenciaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<TaxaFrequenciaParametro>> ObterPorSlugAsync(ObterPorSlugGraficoTaxaFrequenciaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<TaxaFrequenciaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<TaxaFrequenciaParametro>>> ObterListaAsync(ObterListaGraficoTaxaFrequenciaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<TaxaFrequenciaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<TaxaFrequenciaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteGraficoTaxaFrequenciaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
