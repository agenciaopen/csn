﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService : ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService
    {
        private readonly ICsnRepositoryAsync<TaxaFrequenciaExibicao> _repository;
        private readonly ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoRepository _exibicaoRepository;

        public SaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService(ICsnRepositoryAsync<TaxaFrequenciaExibicao> repository, ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaFrequenciaExibicaoRequest request)
        {
            try
            {
                var entity = new TaxaFrequenciaExibicao(request.Ano, request.CAF, request.SAF, request.Reducao, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaFrequenciaExibicaoRequest request)
        {
            try
            {
                var entity = new TaxaFrequenciaExibicao(request.Id, request.Ano, request.CAF, request.SAF, request.Reducao, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaFrequenciaExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<TaxaFrequenciaExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<TaxaFrequenciaExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<TaxaFrequenciaExibicao>>> ObterDadosAsync(ObterDadosGraficoTaxaFrequenciaExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<TaxaFrequenciaExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
