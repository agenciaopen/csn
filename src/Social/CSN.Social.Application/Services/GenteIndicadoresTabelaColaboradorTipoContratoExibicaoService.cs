﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaColaboradorTipoContratoExibicaoService : IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService
    {
        private readonly ICsnRepositoryAsync<ColaboradorTipoContratoExibicao> _repository;
        private readonly IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository _exibicaoRepository;

        public GenteIndicadoresTabelaColaboradorTipoContratoExibicaoService(ICsnRepositoryAsync<ColaboradorTipoContratoExibicao> repository, IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            try
            {
                var entity = new ColaboradorTipoContratoExibicao(request.Genero, request.TempoDeterminado, request.TempoIndeterminado, request.Total, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            try
            {
                var entity = new ColaboradorTipoContratoExibicao(request.Id, request.Genero, request.TempoDeterminado, request.TempoIndeterminado, request.Total, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ColaboradorTipoContratoExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<ColaboradorTipoContratoExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<ColaboradorTipoContratoExibicao>>> ObterDadosAsync(ObterDadosTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<ColaboradorTipoContratoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
