﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaColaboradorProprioExibicaoService : IGenteIndicadoresTabelaColaboradorProprioExibicaoService
    {
        private readonly ICsnRepositoryAsync<ColaboradorProprioExibicao> _repository;
        private readonly IGenteIndicadoresTabelaColaboradorProprioExibicaoRepository _exibicaoRepository;

        public GenteIndicadoresTabelaColaboradorProprioExibicaoService(ICsnRepositoryAsync<ColaboradorProprioExibicao> repository, IGenteIndicadoresTabelaColaboradorProprioExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorProprioExibicaoRequest request)
        {
            try
            {
                var entity = new ColaboradorProprioExibicao(request.Ano, request.ColaborProprio,
                    request.ColaboradorTerceiro, request.Total, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorProprioExibicaoRequest request)
        {
            try
            {
                var entity = new ColaboradorProprioExibicao(request.Ano, request.ColaborProprio,
                    request.ColaboradorTerceiro, request.Total, request.Idioma);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorProprioExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ColaboradorProprioExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaColaboradorProprioExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o=>o.Cadastro);
            var result = new ResponseResult<ColaboradorProprioExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<ColaboradorProprioExibicao>>> ObterDadosAsync(ObterDadosTabelaColaboradorProprioExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<ColaboradorProprioExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaColaboradorProprioExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
