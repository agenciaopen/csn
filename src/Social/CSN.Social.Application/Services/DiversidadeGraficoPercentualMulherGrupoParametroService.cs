﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.Diversidade;
using CSN.Social.Domain.Entities.Diversidade.Grafico;

namespace CSN.Social.Application.Services
{
    public class DiversidadeGraficoPercentualMulherGrupoParametroService : IDiversidadeGraficoPercentualMulherGrupoParametroService
    {
        private readonly IRepositoryAsync<PercentualMulherGrupoParametro> _repository;

        public DiversidadeGraficoPercentualMulherGrupoParametroService(IRepositoryAsync<PercentualMulherGrupoParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherGrupoParametroRequest request)
        {
            try
            {
                var entity = new PercentualMulherGrupoParametro(request.Nome, request.QuantidadeAnos, request.Idioma, true);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherGrupoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherGrupoParametroRequest request)
        {
            try
            {
                var entity = new PercentualMulherGrupoParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<PercentualMulherGrupoParametro>> ObterPorIdAsync(ObterPorIdGraficoPercentualMulherGrupoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<PercentualMulherGrupoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<PercentualMulherGrupoParametro>> ObterPorSlugAsync(ObterPorSlugGraficoPercentualMulherGrupoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<PercentualMulherGrupoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<PercentualMulherGrupoParametro>>> ObterListaAsync(ObterListaGraficoPercentualMulherGrupoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<PercentualMulherGrupoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<PercentualMulherGrupoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteGraficoPercentualMulherGrupoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }

    }
}
