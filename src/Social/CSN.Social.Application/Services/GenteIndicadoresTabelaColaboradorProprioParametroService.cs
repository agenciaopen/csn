﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaColaboradorProprioParametroService : IGenteIndicadoresTabelaColaboradorProprioParametroService
    {
        private readonly IRepositoryAsync<ColaboradorProprioParametro> _repository;

        public GenteIndicadoresTabelaColaboradorProprioParametroService(IRepositoryAsync<ColaboradorProprioParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorProprioParametroRequest request)
        {
            try
            {
                var entity = new ColaboradorProprioParametro(request.Nome, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorProprioParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorProprioParametroRequest request)
        {
            try
            {
                var entity = new ColaboradorProprioParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ColaboradorProprioParametro>> ObterPorIdAsync(ObterPorIdTabelaColaboradorProprioParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<ColaboradorProprioParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<ColaboradorProprioParametro>> ObterPorSlugAsync(ObterPorSlugTabelaColaboradorProprioParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<ColaboradorProprioParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<ColaboradorProprioParametro>>> ObterListaAsync(ObterListaTabelaColaboradorProprioParametroRequest request)
        {
            var predicate = PredicateBuilder.True<ColaboradorProprioParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<ColaboradorProprioParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaColaboradorProprioParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
