﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Services
{
    public class GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService : IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService
    {
        private readonly IRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro> _repository;

        public GenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService(IRepositoryAsync<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro(request.Titulo, request.Subtitulo, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            try
            {
                var entity = new NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro(request.Id, request.Titulo, request.Subtitulo, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>> ObterPorIdAsync(ObterPorIdTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>> ObterListaAsync(ObterListaTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            var predicate = PredicateBuilder.True<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Titulo != null)
                predicate = predicate.And(x => x.Titulo.StartsWith(request.Titulo));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }
}
