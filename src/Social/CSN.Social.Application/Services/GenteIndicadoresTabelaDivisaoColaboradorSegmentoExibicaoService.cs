﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Interfaces;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService : IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService
    {
        private readonly ICsnRepositoryAsync<DivisaoColaboradorSegmentoExibicao> _repository;
        private readonly IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoRepository _exibicaoRepository;

        public GenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService(ICsnRepositoryAsync<DivisaoColaboradorSegmentoExibicao> repository, IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoRepository exibicaoRepository)
        {
            _repository = repository;
            _exibicaoRepository = exibicaoRepository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            try
            {
                var entity = new DivisaoColaboradorSegmentoExibicao(request.Ano, request.Siderurgia, request.Mineracao,
                    request.Logistica, request.Corporativo, request.Cimento, request.Total, request.Idioma);
                var result = await _repository.AdicionarAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            try
            {
                var entity = new DivisaoColaboradorSegmentoExibicao(request.Id, request.Ano, request.Siderurgia, request.Mineracao,
                    request.Logistica, request.Corporativo, request.Cimento, request.Total, request.Idioma, request.Ativo);
                var result = await _repository.AtualizarAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            try
            {
                var result = await _repository.ExcluirAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<DivisaoColaboradorSegmentoExibicao>> ObterUltimoAtivoAsync(ObterUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            var entity = await _repository
                .ObterAsync(x => x.Idioma == request.Idioma && x.Ativo, o => o.Cadastro);
            var result = new ResponseResult<DivisaoColaboradorSegmentoExibicao>(true, entity);
            return result;
        }

        public async Task<ResponseResult<IEnumerable<DivisaoColaboradorSegmentoExibicao>>> ObterDadosAsync(ObterDadosTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            var entity = await _exibicaoRepository.ObterDadosAsync(request.Idioma);
            return new ResponseResult<IEnumerable<DivisaoColaboradorSegmentoExibicao>>(true, entity);
        }

        public async Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest request)
        {
            return await _repository.ExisteAsync(x => x.Ativo && x.Idioma == request.Idioma);
        }
    }
}
