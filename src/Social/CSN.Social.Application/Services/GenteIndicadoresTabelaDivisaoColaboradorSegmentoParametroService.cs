﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService : IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService
    {
        private readonly IRepositoryAsync<DivisaoColaboradorSegmentoParametro> _repository;

        public GenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService(IRepositoryAsync<DivisaoColaboradorSegmentoParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            try
            {
                var entity = new DivisaoColaboradorSegmentoParametro(request.Nome, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            try
            {
                var entity = new DivisaoColaboradorSegmentoParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<DivisaoColaboradorSegmentoParametro>> ObterPorIdAsync(ObterPorIdTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<DivisaoColaboradorSegmentoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<DivisaoColaboradorSegmentoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<DivisaoColaboradorSegmentoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<DivisaoColaboradorSegmentoParametro>>> ObterListaAsync(ObterListaTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<DivisaoColaboradorSegmentoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<DivisaoColaboradorSegmentoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaDivisaoColaboradorSegmentoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }

    }
}
