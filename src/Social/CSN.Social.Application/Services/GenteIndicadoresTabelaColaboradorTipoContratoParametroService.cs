﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.Interfaces;
using CSN.Core.Helpers;
using CSN.Social.Application.Interfaces;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Services
{
    public class GenteIndicadoresTabelaColaboradorTipoContratoParametroService : IGenteIndicadoresTabelaColaboradorTipoContratoParametroService
    {
        private readonly IRepositoryAsync<ColaboradorTipoContratoParametro> _repository;

        public GenteIndicadoresTabelaColaboradorTipoContratoParametroService(IRepositoryAsync<ColaboradorTipoContratoParametro> repository)
        {
            _repository = repository;
        }

        public async Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorTipoContratoParametroRequest request)
        {
            try
            {
                var entity = new ColaboradorTipoContratoParametro(request.Nome, request.QuantidadeAnos, request.Idioma);
                var result = await _repository.AddAsync(entity);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorTipoContratoParametroRequest request)
        {
            try
            {
                var result = await _repository.DeleteAsync(request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorTipoContratoParametroRequest request)
        {
            try
            {
                var entity = new ColaboradorTipoContratoParametro(request.Id, request.Nome, request.QuantidadeAnos, request.Idioma, request.Ativo);
                var result = await _repository.UpdateAsync(entity, request.Id);
                return new ResponseResult(result);
            }
            catch (Exception e)
            {
                return new ResponseResult(e);
            }
        }

        public async Task<ResponseResult<ColaboradorTipoContratoParametro>> ObterPorIdAsync(ObterPorIdTabelaColaboradorTipoContratoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Id == request.Id);
            var result = new ResponseResult<ColaboradorTipoContratoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<ColaboradorTipoContratoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaColaboradorTipoContratoParametroRequest request)
        {
            var entity = await _repository
                .FindByAsync(x => x.Slug == request.Slug);
            var result = new ResponseResult<ColaboradorTipoContratoParametro>(true, entity);
            return result;
        }

        public async Task<ResponseResult<List<ColaboradorTipoContratoParametro>>> ObterListaAsync(ObterListaTabelaColaboradorTipoContratoParametroRequest request)
        {
            var predicate = PredicateBuilder.True<ColaboradorTipoContratoParametro>();

            if (request.Ativo != null)
                predicate = predicate.And(x => x.Ativo == request.Ativo);

            if (request.Nome != null)
                predicate = predicate.And(x => x.Nome.StartsWith(request.Nome));

            var entity = await _repository.FindAsync(predicate);

            var result = new ResponseResult<List<ColaboradorTipoContratoParametro>>(true, entity?.ToList());
            return result;
        }

        public async Task<bool> ExisteAsync(ExisteTabelaColaboradorTipoContratoParametroRequest request)
        {
            var entity = await _repository.HasExistAsync(x => x.Slug == request.Slug);
            return entity;
        }
    }

}
