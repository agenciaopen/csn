﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorTipoContratoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorTipoContratoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorTipoContratoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorTipoContratoParametroRequest request);
        Task<ResponseResult<ColaboradorTipoContratoParametro>> ObterPorIdAsync(ObterPorIdTabelaColaboradorTipoContratoParametroRequest request);
        Task<ResponseResult<ColaboradorTipoContratoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaColaboradorTipoContratoParametroRequest request);
        Task<ResponseResult<List<ColaboradorTipoContratoParametro>>> ObterListaAsync(ObterListaTabelaColaboradorTipoContratoParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaColaboradorTipoContratoParametroRequest request);
    }
}
