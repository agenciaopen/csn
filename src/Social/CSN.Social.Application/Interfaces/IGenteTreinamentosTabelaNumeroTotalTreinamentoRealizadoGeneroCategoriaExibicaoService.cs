﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoGeneroCategoriaExibicao>>> ObterDadosAsync(
            ObterDadosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaExibicaoRequest request);
    }
}
