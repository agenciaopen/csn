﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaDivisaoColaboradorSegmentoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<ResponseResult<DivisaoColaboradorSegmentoParametro>> ObterPorIdAsync(ObterPorIdTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<ResponseResult<DivisaoColaboradorSegmentoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<ResponseResult<List<DivisaoColaboradorSegmentoParametro>>> ObterListaAsync(ObterListaTabelaDivisaoColaboradorSegmentoParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaDivisaoColaboradorSegmentoParametroRequest request);
    }
}
