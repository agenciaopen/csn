﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>> ObterPorIdAsync(ObterPorIdTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoParametro>> ObterPorSlugAsync(ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<ResponseResult<List<NumeroTotalTreinamentoRealizadoParametro>>> ObterListaAsync(ObterListaTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaNumeroTotalTreinamentoRealizadoParametroRequest request);
    }
}
