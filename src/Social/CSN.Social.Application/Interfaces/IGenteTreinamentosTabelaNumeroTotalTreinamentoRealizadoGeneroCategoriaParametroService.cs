﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>> ObterPorIdAsync(ObterPorIdTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>> ObterPorSlugAsync(ObterPorSlugTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<ResponseResult<List<NumeroTotalTreinamentoRealizadoGeneroCategoriaParametro>>> ObterListaAsync(ObterListaTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaNumeroTotalTreinamentoRealizadoGeneroCategoriaParametroRequest request);
    }
}
