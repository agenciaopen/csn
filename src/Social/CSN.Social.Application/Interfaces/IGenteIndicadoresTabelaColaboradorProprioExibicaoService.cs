﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Core.Domain.ValueObjects;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorProprioExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorProprioExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorProprioExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorProprioExibicaoRequest request);
        Task<ResponseResult<ColaboradorProprioExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoTabelaColaboradorProprioExibicaoRequest request);
        Task<ResponseResult<IEnumerable<ColaboradorProprioExibicao>>> ObterDadosAsync(
            ObterDadosTabelaColaboradorProprioExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaColaboradorProprioExibicaoRequest request);
    }
}
