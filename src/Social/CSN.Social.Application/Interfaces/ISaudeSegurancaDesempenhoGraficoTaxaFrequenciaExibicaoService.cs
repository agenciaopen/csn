﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaFrequenciaExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaFrequenciaExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaFrequenciaExibicaoRequest request);
        Task<ResponseResult<TaxaFrequenciaExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest request);
        Task<ResponseResult<IEnumerable<TaxaFrequenciaExibicao>>> ObterDadosAsync(
            ObterDadosGraficoTaxaFrequenciaExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoTaxaFrequenciaExibicaoRequest request);
    }
}