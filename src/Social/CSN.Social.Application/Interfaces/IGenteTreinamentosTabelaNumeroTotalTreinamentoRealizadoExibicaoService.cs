﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteTreinamentos;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteTreinamentosTabelaNumeroTotalTreinamentoRealizadoExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
        Task<ResponseResult<NumeroTotalTreinamentoRealizadoExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<NumeroTotalTreinamentoRealizadoExibicao>>> ObterDadosAsync(
            ObterDadosTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaNumeroTotalTreinamentoRealizadoExibicaoRequest request);
    }
}
