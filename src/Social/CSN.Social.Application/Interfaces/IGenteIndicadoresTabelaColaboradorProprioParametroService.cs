﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorProprioParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorProprioParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorProprioParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorProprioParametroRequest request);
        Task<ResponseResult<ColaboradorProprioParametro>> ObterPorIdAsync(ObterPorIdTabelaColaboradorProprioParametroRequest request);
        Task<ResponseResult<ColaboradorProprioParametro>> ObterPorSlugAsync(ObterPorSlugTabelaColaboradorProprioParametroRequest request);
        Task<ResponseResult<List<ColaboradorProprioParametro>>> ObterListaAsync(ObterListaTabelaColaboradorProprioParametroRequest request);
        Task<bool> ExisteAsync(ExisteTabelaColaboradorProprioParametroRequest request);
    }
}
