﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaColaboradorTipoContratoExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaColaboradorTipoContratoExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaColaboradorTipoContratoExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaColaboradorTipoContratoExibicaoRequest request);
        Task<ResponseResult<ColaboradorTipoContratoExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<ColaboradorTipoContratoExibicao>>> ObterDadosAsync(
            ObterDadosTabelaColaboradorTipoContratoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaColaboradorTipoContratoExibicaoRequest request);
    }
}
