﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.Diversidade;
using CSN.Social.Domain.Entities.Diversidade.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface IDiversidadeGraficoPercentualMulherGrupoParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherGrupoParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherGrupoParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherGrupoParametroRequest request);
        Task<ResponseResult<PercentualMulherGrupoParametro>> ObterPorIdAsync(ObterPorIdGraficoPercentualMulherGrupoParametroRequest request);
        Task<ResponseResult<PercentualMulherGrupoParametro>> ObterPorSlugAsync(ObterPorSlugGraficoPercentualMulherGrupoParametroRequest request);
        Task<ResponseResult<List<PercentualMulherGrupoParametro>>> ObterListaAsync(ObterListaGraficoPercentualMulherGrupoParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoPercentualMulherGrupoParametroRequest request);
    }
}
