﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface ISaudeSegurancaDesempenhoGraficoTaxaFrequenciaParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaFrequenciaParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaFrequenciaParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaFrequenciaParametroRequest request);
        Task<ResponseResult<TaxaFrequenciaParametro>> ObterPorIdAsync(ObterPorIdGraficoTaxaFrequenciaParametroRequest request);
        Task<ResponseResult<TaxaFrequenciaParametro>> ObterPorSlugAsync(ObterPorSlugGraficoTaxaFrequenciaParametroRequest request);
        Task<ResponseResult<List<TaxaFrequenciaParametro>>> ObterListaAsync(ObterListaGraficoTaxaFrequenciaParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoTaxaFrequenciaParametroRequest request);
    }
}