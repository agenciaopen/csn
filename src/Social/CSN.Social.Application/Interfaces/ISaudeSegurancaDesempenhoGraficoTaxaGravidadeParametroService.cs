﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.GenteTreinamentos.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface ISaudeSegurancaDesempenhoGraficoTaxaGravidadeParametroService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaGravidadeParametroRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaGravidadeParametroRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaGravidadeParametroRequest request);
        Task<ResponseResult<TaxaGravidadeParametro>> ObterPorIdAsync(ObterPorIdGraficoTaxaGravidadeParametroRequest request);
        Task<ResponseResult<TaxaGravidadeParametro>> ObterPorSlugAsync(ObterPorSlugGraficoTaxaGravidadeParametroRequest request);
        Task<ResponseResult<List<TaxaGravidadeParametro>>> ObterListaAsync(ObterListaGraficoTaxaGravidadeParametroRequest request);
        Task<bool> ExisteAsync(ExisteGraficoTaxaGravidadeParametroRequest request);
    }
}
