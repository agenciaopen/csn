﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.GenteIndicadores;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;

namespace CSN.Social.Application.Interfaces
{
    public interface IGenteIndicadoresTabelaDivisaoColaboradorSegmentoExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
        Task<ResponseResult<DivisaoColaboradorSegmentoExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<DivisaoColaboradorSegmentoExibicao>>> ObterDadosAsync(
            ObterDadosTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoTabelaDivisaoColaboradorSegmentoExibicaoRequest request);
    }
}
