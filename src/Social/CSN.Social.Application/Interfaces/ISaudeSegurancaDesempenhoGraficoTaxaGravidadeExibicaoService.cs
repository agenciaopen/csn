﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.SaudeSegurancaDesempenho;
using CSN.Social.Domain.Entities.GenteIndicadores.Tabela;
using CSN.Social.Domain.Entities.SaudeSegurancaDesempenho.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface ISaudeSegurancaDesempenhoGraficoTaxaGravidadeExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoTaxaGravidadeExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoTaxaGravidadeExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoTaxaGravidadeExibicaoRequest request);
        Task<ResponseResult<TaxaGravidadeExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoTaxaGravidadeExibicaoRequest request);
        Task<ResponseResult<IEnumerable<TaxaGravidadeExibicao>>> ObterDadosAsync(
            ObterDadosGraficoTaxaGravidadeExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoTaxaGravidadeExibicaoRequest request);
    }
}
