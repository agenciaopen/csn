﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CSN.Core.Communications;
using CSN.Social.Application.Requests.Diversidade;
using CSN.Social.Domain.Entities.Diversidade.Grafico;

namespace CSN.Social.Application.Interfaces
{
    public interface IDiversidadeGraficoPercentualMulherGrupoExibicaoService
    {
        Task<ResponseResult> AdicionarAsync(AdicionarGraficoPercentualMulherGrupoExibicaoRequest request);
        Task<ResponseResult> AtualizarAsync(AtualizarGraficoPercentualMulherGrupoExibicaoRequest request);
        Task<ResponseResult> ExcluirAsync(ExcluirGraficoPercentualMulherGrupoExibicaoRequest request);
        Task<ResponseResult<PercentualMulherGrupoExibicao>> ObterUltimoAtivoAsync(
            ObterUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest request);
        Task<ResponseResult<IEnumerable<PercentualMulherGrupoExibicao>>> ObterDadosAsync(
            ObterDadosGraficoPercentualMulherGrupoExibicaoRequest request);
        Task<bool> ExisteUltimoAtivoAsync(ExisteUltimoAtivoGraficoPercentualMulherGrupoExibicaoRequest request);
    }
}
