﻿namespace CSN.Core.Extensions
{
    public class GenericComparer<T> : IEqualityComparer<T>
    {
        public Func<T, T, bool> EqualsMethod { get; }
        public Func<T, int> GetHashCodeMethod { get; }

        public GenericComparer(
            Func<T, T, bool> equalsMethod,
            Func<T, int> getHashCodeMethod)
        {
            this.EqualsMethod = equalsMethod;
            this.GetHashCodeMethod = getHashCodeMethod;
        }

        public static GenericComparer<T> Create(
            Func<T, T, bool> equalsMethod,
            Func<T, int> getHashCodeMethod)
            => new GenericComparer<T>(
                equalsMethod,
                getHashCodeMethod
            );

        public bool Equals(T x, T y) => EqualsMethod(x, y);

        public int GetHashCode(T obj) => GetHashCodeMethod(obj);
    }
}
