﻿namespace CSN.Core.Extensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<TSource> Distinct<TSource>(
                       this IEnumerable<TSource> source,
                       Func<TSource, TSource, bool> equalsMethod,
                       Func<TSource, int> getHashCodeMethod)
                           => source.Distinct(
                               GenericComparer<TSource>.Create(
                                   equalsMethod,
                                   getHashCodeMethod)
                                   );
    }
}
