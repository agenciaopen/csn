﻿using System.ComponentModel;

namespace CSN.Core.Extensions
{

    public static class EnumExtensions
    {
        public static string Description<T>(this T source)
        {
            var fi = source.GetType().GetField(source.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
                return attributes[0].Description;
            return source.ToString();
        }

        //public static string GetDescriptionFromEnumValue(this Enum value)
        //{
        //    var attribute = value.GetType()
        //        .GetField(value.ToString())
        //        .GetCustomAttributes(typeof(DescriptionAttribute), false)
        //        .SingleOrDefault() as DescriptionAttribute;
        //    return attribute == null ? value.ToString() : attribute.Description;
        //}
    }
}