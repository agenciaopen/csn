﻿using Newtonsoft.Json;

namespace CSN.Core.Extensions
{
    public static class JsonExtensions
    {
        public static string Serialize<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
