﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace CSN.Core.Extensions
{
    public static class StringExtensions
    {
        public static string ConvertToISO(this string value)
        {

            Encoding isoEncoding = Encoding.GetEncoding("ISO-8859-1");
            Encoding utfEncoding = Encoding.UTF8;

            // Converte os bytes 
            byte[] bytesIso = utfEncoding.GetBytes(value);

            //  Obtém os bytes da string UTF 
            byte[] bytesUtf = Encoding.Convert(utfEncoding, isoEncoding, bytesIso);

            // Obtém a string ISO a partir do array de bytes convertido
            string textoISO = utfEncoding.GetString(bytesUtf);

            return textoISO;
        }

        public static string FormatPhone(this string value, bool celular)
        {
            value = value.Trim().Replace("(", "").Replace(")", "").Replace("-", "");
            if (value.Length > 2 && value.Substring(0, 2) == "55")
            {
                value = value.Substring(2);
            }

            if (celular && value.Length > 2)
            {
                if (value.Length > 2 && value.Length < 11)
                {
                    string ddd = value.Substring(0, 2),
                        telefone = value.Substring(2);

                    value = ddd + "9" + telefone;
                }

                return string.Format("{0:(##)#####-####}", Convert.ToInt64(value));

            }
            else if (value.Length == 10)
            {
                return string.Format("{0:(##)####-####}", Convert.ToInt64(value));
            }
            else
            {
                return string.Empty;
            }
        }

        public static bool IsLandLine(this string value)
        {
            var reg = @"[1-9]{2}[2-8]{1}[0-9]{3}[0-9]{4}";
            var ehValido = Regex.IsMatch(value, reg);
            return ehValido;
        }

        public static bool IsMobile(this string value)
        {
            var reg = @"[1-9]{2}[9]{0,1}[6-9]{1}[0-9]{3}[0-9]{4}";
            var ehValido = Regex.IsMatch(value, reg);
            return ehValido;
        }

        public static string FormatMobile(this string num)
        {
            var ddd = num.Substring(0, 2);
            var parte1 = string.Empty;
            var parte2 = string.Empty;

            if (num.Length == 10)
            {
                parte1 = $"9{num.Substring(2, 4)}";
                parte2 = $"{num.Substring(6, 4)}";
            }

            if (num.Length == 11)
            {
                parte1 = $"{num.Substring(2, 5)}";
                parte2 = $"{num.Substring(7, 4)}";
            }

            return $"({ddd}){parte1}-{parte2}";
        }

        public static string FormatLandLine(this string num)
        {
            var ddd = num.Substring(0, 2);
            var parte1 = num.Substring(2, 4);
            var parte2 = num.Substring(6, 4);

            return $"({ddd}){parte1}-{parte2}";
        }

        public static string OnlyNumbers(this string input)
        {
            return new string(input.Where(char.IsDigit).ToArray());
        }

        /// <summary>
        /// Insere espaço ao final de cada palavra
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string CamelCaseSpacing(this string s)
        {
            // Sourced from https://stackoverflow.com/questions/4488969/split-a-string-by-capital-letters.
            var r = new Regex(@"
            (?<=[A-Z])(?=[A-Z][a-z]) |
             (?<=[^A-Z])(?=[A-Z]) |
             (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            return r.Replace(s, " ");
        }

        public static DateTime ToDateTime(this string value)
        {
            var result = Regex.IsMatch(value, "([0-9]{2})/([0-9]{2})/([0-9]{4})");

            if (!result)
                throw new Exception("Formato inválido.");

            var day = value.Substring(0, 2);
            var month = value.Substring(3, 2);
            var year = value.Substring(6, 4);

            return new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
        }

        public static DateTime ToDateTimeWithHour(this string value, string time)
        {
            var result = Regex.IsMatch(value, "([0-9]{2})/([0-9]{2})/([0-9]{4})");

            if (!result)
                throw new Exception("Formato inválido.");

            var day = value.Substring(0, 2);
            var month = value.Substring(3, 2);
            var year = value.Substring(6, 4);
            var hour = time.Substring(0, 2);
            var minute = time.Substring(3, 2);
            var second = time.Length > 5 ? time.Substring(6, 2) : "00";

            return new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day), Convert.ToInt32(hour), Convert.ToInt32(minute), Convert.ToInt32(second));
        }
        
        public static string ToTitleCase(this string title)
        {
            TextInfo textInfo = new CultureInfo("pt-BR", false).TextInfo;

            return textInfo.ToTitleCase(title.ToLower());
        }

        public static string HtmlEncode(this string value)
        {
            // call the normal HtmlEncode first
            char[] chars = HttpUtility.HtmlEncode(value).ToCharArray();
            StringBuilder encodedValue = new StringBuilder();
            foreach (char c in chars)
            {
                if ((int)c > 127) // above normal ASCII
                    encodedValue.Append("&#" + (int)c + ";");
                else
                    encodedValue.Append(c);
            }
            return encodedValue.ToString();
        }

        public static string ToUF(this string value)
        {
            var data = string.Empty;

            if (value != string.Empty || value != null)
            {
                switch (value.ToUpper())
                {
                    case "ACRE":
                        data = "AC";
                        break;
                    case "ALAGOAS":
                        data = "AL";
                        break;
                    case "AMAZONAS":
                        data = "AM";
                        break;
                    case "AMAPÁ":
                        data = "AP";
                        break;
                    case "BAHIA":
                        data = "BA";
                        break;
                    case "CEARÁ":
                        data = "CE";
                        break;
                    case "DISTRITO FEDERAL":
                        data = "DF";
                        break;
                    case "ESPÍRITO SANTO":
                        data = "ES";
                        break;
                    case "GOIÁS":
                        data = "GO";
                        break;
                    case "MARANHÃO":
                        data = "MA";
                        break;
                    case "MINAS GERAIS":
                        data = "MG";
                        break;
                    case "MATO GROSSO DO SUL":
                        data = "MS";
                        break;
                    case "MATO GROSSO":
                        data = "MT";
                        break;
                    case "PARÁ":
                        data = "PA";
                        break;
                    case "PARAÍBA":
                        data = "PB";
                        break;
                    case "PERNAMBUCO":
                        data = "PE";
                        break;
                    case "PIAUÍ":
                        data = "PI";
                        break;
                    case "PARANÁ":
                        data = "PR";
                        break;
                    case "RIO DE JANEIRO":
                        data = "RJ";
                        break;
                    case "RIO GRANDE DO NORTE":
                        data = "RN";
                        break;
                    case "RONDÔNIA":
                        data = "RO";
                        break;
                    case "RORAIMA":
                        data = "RR";
                        break;
                    case "RIO GRANDE DO SUL":
                        data = "RS";
                        break;
                    case "SANTA CATARINA":
                        data = "SC";
                        break;
                    case "SERGIPE":
                        data = "SE";
                        break;
                    case "SÃO PAULO":
                        data = "SP";
                        break;
                    case "TOCANTÍNS":
                        data = "TO";
                        break;
                    default:
                        data = "";
                        break;
                }
            }

            return data;
        }

        public static string FormatCnpj(this string cnpj)
        {
            return Convert.ToUInt64(cnpj).ToString(@"00\.000\.000\/0000\-00");
        }

        public static string FormatCpf(this string cpf)
        {
            return Convert.ToUInt64(cpf).ToString(@"000\.000\.000\-00");
        }

        public static string RemoveFormatCpfCnpj(this string codigo)
        {
            return codigo.Replace(".", string.Empty).Replace("-", string.Empty).Replace("/", string.Empty);
        }

        //public static string ToSlug(this string value)
        //{
        //    return value.ToLower().Replace(" ", "-");
        //}

        public static string RemoveSpecialCharacters(this string text, bool allowSpace = false)
        {
            string ret;

            if (allowSpace)
                ret = System.Text.RegularExpressions.Regex.Replace(text, @"[^0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄçÇ\s]+?", string.Empty);
            else
                ret = System.Text.RegularExpressions.Regex.Replace(text, @"[^0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄçÇ]+?", string.Empty);

            return ret;
        }

        public static string RemoveWhiteSpace(this string text)
        {
            return text.Trim();
        }
        
        public static string RemoveDiacritics(this string text)
        {
            if (string.IsNullOrEmpty(text))
                return text;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] > 255)
                    sb.Append(text[i]);
                else
                    sb.Append(s_Diacritics[text[i]]);
            }

            return sb.ToString();
        }

        private static readonly char[] s_Diacritics = GetDiacritics();
        private static char[] GetDiacritics()
        {
            char[] accents = new char[256];

            for (int i = 0; i < 256; i++)
                accents[i] = (char)i;

            accents[(byte)'á'] = accents[(byte)'à'] = accents[(byte)'ã'] = accents[(byte)'â'] = accents[(byte)'ä'] = 'a';
            accents[(byte)'Á'] = accents[(byte)'À'] = accents[(byte)'Ã'] = accents[(byte)'Â'] = accents[(byte)'Ä'] = 'A';

            accents[(byte)'é'] = accents[(byte)'è'] = accents[(byte)'ê'] = accents[(byte)'ë'] = 'e';
            accents[(byte)'É'] = accents[(byte)'È'] = accents[(byte)'Ê'] = accents[(byte)'Ë'] = 'E';

            accents[(byte)'í'] = accents[(byte)'ì'] = accents[(byte)'î'] = accents[(byte)'ï'] = 'i';
            accents[(byte)'Í'] = accents[(byte)'Ì'] = accents[(byte)'Î'] = accents[(byte)'Ï'] = 'I';

            accents[(byte)'ó'] = accents[(byte)'ò'] = accents[(byte)'ô'] = accents[(byte)'õ'] = accents[(byte)'ö'] = 'o';
            accents[(byte)'Ó'] = accents[(byte)'Ò'] = accents[(byte)'Ô'] = accents[(byte)'Õ'] = accents[(byte)'Ö'] = 'O';

            accents[(byte)'ú'] = accents[(byte)'ù'] = accents[(byte)'û'] = accents[(byte)'ü'] = 'u';
            accents[(byte)'Ú'] = accents[(byte)'Ù'] = accents[(byte)'Û'] = accents[(byte)'Ü'] = 'U';

            accents[(byte)'ç'] = 'c';
            accents[(byte)'Ç'] = 'C';

            accents[(byte)'ñ'] = 'n';
            accents[(byte)'Ñ'] = 'N';

            accents[(byte)'ÿ'] = accents[(byte)'ý'] = 'y';
            accents[(byte)'Ý'] = 'Y';

            return accents;
        }

        public static string ToSlug(this string phrase)
        {
            string str = phrase.RemoveAccent().ToLower();
            // invalid chars           
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            // convert multiple spaces into one space   
            str = Regex.Replace(str, @"\s+", " ").Trim();
            // cut and trim 
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-"); // hyphens   
            return str;
        }

        public static string RemoveAccent(this string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}