﻿namespace CSN.Core.Extensions
{
    public static class DateTimeExtensions
    {
        public static int TimeDifference(this DateTime source, DateTime target)
        {
            var diff = target - source;

            return diff.Hours;
        }
    }
}
