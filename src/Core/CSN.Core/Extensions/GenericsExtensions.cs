﻿using System.ComponentModel;
using System.Data;

namespace CSN.Core.Extensions
{
    public static class GenericsExtensions
    {
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            var props =
                TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            for (var i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            var values = new object[props.Count];
            foreach (var item in data)
            {
                for (var i = 0; i < values.Length; i++) values[i] = props[i].GetValue(item);
                table.Rows.Add(values);
            }

            return table;
        }
    }
}

