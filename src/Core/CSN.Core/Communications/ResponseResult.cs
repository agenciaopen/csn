﻿using System.Text.Json.Serialization;
using FluentValidation.Results;

namespace CSN.Core.Communications
{
    public class ResponseResult<T>
    {
        public bool Success { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? Message { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public T? Data { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<ResponseError>? Errors { get; set; }

        public ResponseResult()
        {

        }

        public ResponseResult(bool success, T? data)
        {
            Success = success;
            Data = data;
        }

        public ResponseResult(bool success)
        {
            Success = success;
        }

        public ResponseResult(ValidationResult validationResult)
        {
            var responseErrors = validationResult.Errors
                .Select(x => new ResponseError
                {
                    PropertyName = x.PropertyName,
                    ErrorMessage = x.ErrorMessage
                }).ToList();

            Errors = new List<ResponseError>();
            Errors?.AddRange(responseErrors);
            Success = false;
        }

        public ResponseResult(Exception exception)
        {
            Errors = new List<ResponseError>()
            {
                new ResponseError
                {
                    ErrorMessage = exception.Message
                }
            };

            Success = false;
        }
    }

    public class ResponseResult
    {
        public bool Success { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public string? Message { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public object? Id { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public List<ResponseError>? Errors { get; set; }

        public ResponseResult()
        {

        }

        public ResponseResult(bool success, object? id = null)
        {
            Success = success;
            Id = id;
        }

        public ResponseResult(ValidationResult validationResult)
        {
            var responseErrors = validationResult.Errors
                .Select(x => new ResponseError
                {
                    PropertyName = x.PropertyName,
                    ErrorMessage = x.ErrorMessage
                }).ToList();

            Errors = new List<ResponseError>();
            Errors?.AddRange(responseErrors);
            Success = false;
        }

        public ResponseResult(Exception exception)
        {
            Errors = new List<ResponseError>()
            {
                new ResponseError
                {
                    ErrorMessage = exception.Message
                }
            };

            Success = false;
        }
    }

    public class ResponseError
    {
        public string? PropertyName { get; set; }
        public string? ErrorMessage { get; set; }
    }
}
