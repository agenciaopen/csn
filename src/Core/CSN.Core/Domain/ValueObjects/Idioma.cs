﻿namespace CSN.Core.Domain.ValueObjects
{
    /// <summary>
    /// Enumerador que indica o idioma
    /// </summary>
    public enum Idioma
    {
        Portugues = 1,
        Ingles
    }
}
