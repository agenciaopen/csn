﻿namespace CSN.Core.Domain.ValueObjects
{
    public abstract class Entity
    {
        protected Entity()
        {
            Id = Guid.NewGuid();
            Cadastro = DateTime.Now;
        }

        public Guid Id { get; set; }
        
        /// <summary>
        /// Indica o idioma do registro.
        /// </summary>
        public Idioma Idioma { get; set; }

        /// <summary>
        /// Esse valor tem que ser o mesmo Guid para o registro em português e em inglês.
        /// </summary>
        public Guid Referencia { get; set; }

        /// <summary>
        /// Define se o registro está ativo.
        /// </summary>
        public bool Ativo { get; set; }

        /// <summary>
        /// Data do cadastro ou alteração do registro
        /// </summary>
        public DateTime Cadastro { get; set; }
    }
}
