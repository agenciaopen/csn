﻿using System.Linq.Expressions;
using CSN.Core.Domain.ValueObjects;

namespace CSN.Core.Domain.Interfaces
{
    public interface ICsnRepositoryAsync<TEntity> : IDisposable
        where TEntity : class
    {
        Task<bool> AdicionarAsync(TEntity t);
        Task<bool> AtualizarAsync(TEntity? t, object? key);
        Task<bool> ExcluirAsync(object id);
        Task<TEntity?> ObterAsync(Expression<Func<TEntity, bool>> predicate, Func<TEntity, DateTime> order);
        Task<bool> ExisteAsync(Expression<Func<TEntity, bool>> predicate);
    }

    public interface ICsnRepository<TEntity> : IDisposable
        where TEntity : class
    {
        Task<int> Adicionar(TEntity t);
        Task<int> Atualizar(TEntity t, object key);
        Task<int> Excluir(object id);
        Task<TEntity> ObterUltimoAtivo(Idioma idioma);
        Task<bool> ExisteUltimoAtivo(Idioma idioma);
    }

    public interface ICsnParametersRepositoryAsync<TEntity> : IDisposable
        where TEntity : class
    {
        Task<bool> AdicionarAsync(TEntity t);
        Task<bool> AtualizarAsync(TEntity? t, object? key);
        Task<bool> ExcluirAsync(object id);
        Task<TEntity?> ObterAsync(Expression<Func<TEntity, bool>> predicate, Func<TEntity, DateTime> order);
        Task<bool> ExisteAsync(Expression<Func<TEntity, bool>> predicate);
    }
}
