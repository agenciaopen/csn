﻿using System.Linq.Expressions;

namespace CSN.Core.Domain.Interfaces
{
    public interface IRepositoryAsync<TEntity> : IDisposable
        where TEntity : class
    {
        Task<bool> AddAsync(TEntity t);
        Task<int> CountAsync();
        Task<bool> HasExistAsync(Expression<Func<TEntity, bool>> match);
        Task<bool> DeleteAsync(Guid id);
        Task<ICollection<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match);
        Task<TEntity> FindByAsync(Expression<Func<TEntity, bool>> match);
        Task<ICollection<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<ICollection<TEntity>> GetAllAsync();
        Task<TEntity> GetAsync(int id);
        Task<int> SaveAsync();
        Task<bool> UpdateAsync(TEntity t, object key);
    }

    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        TEntity Add(TEntity t);
        int Count();
        void Delete(TEntity entity);
        TEntity Find(Expression<Func<TEntity, bool>> match);
        ICollection<TEntity> FindAll(Expression<Func<TEntity, bool>> match);
        IQueryable<TEntity> FindBy(Expression<Func<TEntity, bool>> predicate);
        TEntity Get(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAllIncluding(params Expression<Func<TEntity, object>>[] includeProperties);
        void Save();
        TEntity Update(TEntity t, object key);
    }
}
